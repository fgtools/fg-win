# 20140115 - Resume MANUAL fixes
# CMakeLists.txt generated 2014/01/15 16:21:36
# by make2cmake.pl from F:\FG\18\jpeg-9\makefile.vc

cmake_minimum_required (VERSION 2.8)

project (jpeg)

set( jpeg_VERSION_MAJOR 9 )
set( jpeg_VERSION_MINOR 0 )
set( jpeg_VERSION_POINT 0 )


# Allow developer to select is Dynamic or static library built
set( LIB_TYPE STATIC )  # set default static
option( BUILD_SHARED_LIB "Build Shared Library" OFF )

if (CMAKE_SIZEOF_VOID_P EQUAL 8)
    set(IS_64_BIT 1)
    message(STATUS "*** Compiling in 64-bit environmnet")
else ()
    set(IS_64_BIT 0)
    message(STATUS "*** Compiling in 32-bit environmnet")
endif ()

if(CMAKE_COMPILER_IS_GNUCXX)
    set( WARNING_FLAGS -Wall )
endif(CMAKE_COMPILER_IS_GNUCXX)

if (CMAKE_CXX_COMPILER_ID STREQUAL "Clang") 
   set( WARNING_FLAGS "-Wall -Wno-overloaded-virtual" )
endif() 

if (WIN32 AND MSVC)
    # turn off various warnings
    set(WARNING_FLAGS "${WARNING_FLAGS} /wd4996")
    # foreach(warning 4244 4251 4267 4275 4290 4786 4305)
    #     set(WARNING_FLAGS "${WARNING_FLAGS} /wd${warning}")
    # endforeach(warning)

    set( MSVC_FLAGS "-DNOMINMAX -D_USE_MATH_DEFINES -D_CRT_SECURE_NO_WARNINGS -D_SCL_SECURE_NO_WARNINGS -D__CRT_NONSTDC_NO_WARNINGS" )
    # if (${MSVC_VERSION} EQUAL 1600)
    #    set( MSVC_LD_FLAGS "/FORCE:MULTIPLE" )
    # endif (${MSVC_VERSION} EQUAL 1600)
    #set( NOMINMAX 1 )
    # to distinguish between debug and release lib
    set( CMAKE_DEBUG_POSTFIX "d" )
endif ()

set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${WARNING_FLAGS} ${MSVC_FLAGS} -D_REENTRANT" )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${WARNING_FLAGS} ${MSVC_FLAGS} -D_REENTRANT" )
set( CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${MSVC_LD_FLAGS}" )

add_definitions( -DHAVE_CONFIG_H )

if(BUILD_SHARED_LIB)
   set(LIB_TYPE SHARED)
   message(STATUS "*** Building DLL library ${LIB_TYPE}")
else(BUILD_SHARED_LIB)
   message(STATUS "*** Building static library ${LIB_TYPE}")
endif(BUILD_SHARED_LIB)

configure_file( ${CMAKE_SOURCE_DIR}/jconfig.vc ${CMAKE_SOURCE_DIR}/jconfig.h COPYONLY )

# Project: [libjpeg], type Static Library, with 55 sources, 46 C/C++, 9 Hdrs, 0 O.
set( libjpeg_SRCS
   jaricom.c
   jcapimin.c
   jcapistd.c
   jcarith.c
   jccoefct.c
   jccolor.c
   jcdctmgr.c
   jchuff.c
   jcinit.c
   jcmainct.c
   jcmarker.c
   jcmaster.c
   jcomapi.c
   jcparam.c
   jcprepct.c
   jcsample.c
   jctrans.c
   jdapimin.c
   jdapistd.c
   jdarith.c
   jdatadst.c
   jdatasrc.c
   jdcoefct.c
   jdcolor.c
   jddctmgr.c
   jdhuff.c
   jdinput.c
   jdmainct.c
   jdmarker.c
   jdmaster.c
   jdmerge.c
   jdpostct.c
   jdsample.c
   jdtrans.c
   jerror.c
   jfdctflt.c
   jfdctfst.c
   jfdctint.c
   jidctflt.c
   jidctfst.c
   jidctint.c
   jmemmgr.c
   jmemnobs.c
   jquant1.c
   jquant2.c
   jutils.c )
set( libjpeg_HDRS
   jconfig.h
   jdct.h
   jerror.h
   jinclude.h
   jmemsys.h
   jmorecfg.h
   jpegint.h
   jpeglib.h
   jversion.h )
list (APPEND inst_HDRS ${libjpeg_HDRS})
# Name of library 'jpeg' taken from FindJPEG.cmake modules
add_library( jpeg ${LIB_TYPE}
      ${libjpeg_SRCS}
      ${libjpeg_HDRS} )
list (APPEND add_LIBS jpeg )
list (APPEND inst_LIBS jpeg )

# Project: [wrjpgcom], type Application, with 3 sources, 1 C/C++, 2 Hdrs, 0 O.
set( wrjpgcom_SRCS
   wrjpgcom.c )
add_executable( wrjpgcom
      ${wrjpgcom_SRCS}
      )
if (WIN32)
    set_target_properties( wrjpgcom PROPERTIES DEBUG_POSTFIX d )
endif (WIN32)
target_link_libraries ( wrjpgcom ${add_LIBS} )
list (APPEND inst_BINS wrjpgcom)

# Project: [jpegtran], type Application, with 60 sources, 4 C/C++, 10 Hdrs, 46 O.
set( jpegtran_SRCS
   cdjpeg.c
   jpegtran.c
   rdswitch.c
   transupp.c )
set( jpegtran_HDRS
   cdjpeg.h
   transupp.h )
add_executable( jpegtran
      ${jpegtran_SRCS}
      ${jpegtran_HDRS} )
if (WIN32)
    set_target_properties( jpegtran PROPERTIES DEBUG_POSTFIX d )
endif (WIN32)
target_link_libraries ( jpegtran ${add_LIBS} )
list (APPEND inst_BINS jpegtran)

# Project: [cjpeg], type Application, with 62 sources, 8 C/C++, 8 Hdrs, 46 O.
set( cjpeg_SRCS
   cdjpeg.c
   cjpeg.c
   rdbmp.c
   rdgif.c
   rdppm.c
   rdrle.c
   rdswitch.c
   rdtarga.c )
set( cjpeg_HDRS
   cderror.h
   cdjpeg.h
   jconfig.h
   jerror.h
   jinclude.h
   jmorecfg.h
   jpeglib.h
   jversion.h )
add_executable( cjpeg
      ${cjpeg_SRCS}
      ${cjpeg_HDRS} )
if (WIN32)
    set_target_properties( cjpeg PROPERTIES DEBUG_POSTFIX d )
endif (WIN32)
target_link_libraries ( cjpeg ${add_LIBS} )
list (APPEND inst_BINS cjpeg)

# Project: [djpeg], type Application, with 62 sources, 8 C/C++, 8 Hdrs, 46 O.
set( djpeg_SRCS
   cdjpeg.c
   djpeg.c
   rdcolmap.c
   wrbmp.c
   wrgif.c
   wrppm.c
   wrrle.c
   wrtarga.c )
set( djpeg_HDRS
   cderror.h
   cdjpeg.h
   )
add_executable( djpeg
      ${djpeg_SRCS}
      ${djpeg_HDRS} )
if (WIN32)
    set_target_properties( djpeg PROPERTIES DEBUG_POSTFIX d )
endif (WIN32)
target_link_libraries ( djpeg ${add_LIBS} )
list (APPEND inst_BINS djpeg)

# Project: [rdjpgcom], type Application, with 3 sources, 1 C/C++, 2 Hdrs, 0 O.
set( rdjpgcom_SRCS
   rdjpgcom.c )
add_executable( rdjpgcom
      ${rdjpgcom_SRCS}
      )
if (WIN32)
    set_target_properties( rdjpgcom PROPERTIES DEBUG_POSTFIX d )
endif (WIN32)
target_link_libraries ( rdjpgcom ${add_LIBS} )
list (APPEND inst_BINS rdjpgcom)

# deal with INSTALL
install(TARGETS ${inst_LIBS} DESTINATION lib)
install(FILES ${inst_HDRS} DESTINATION include)
### install(TARGETS ${inst_BINS} DESTINATION bin)

# eof
