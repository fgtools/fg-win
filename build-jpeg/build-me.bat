@setlocal
@rem Adjust to X: drive build system
@rem Skip the artifact building
@set DOTINST=0
@set TMPDIR=X:
@set TMPRT=%TMPDIR%
@set TMPVER=1
@set TMPPRJ=jpeg
@set TMPSRC=%TMPRT%\jpeg-9
@set TMPBGN=%TIME%
@set TMPINS=%TMPRT%\3rdParty
@set TMPCM=%TMPSRC%\CmakeLists.txt
@set DOPAUSE=pause

@if NOT EXIST %TMPSRC%\nul goto NOSRC

@call chkmsvc %TMPPRJ% 

@if EXIST build-cmake.bat (
@call build-cmake
)

@if NOT EXIST %TMPCM% goto NOCM

@set TMPLOG=bldlog-1.txt
@set TMPOPTS=-DCMAKE_INSTALL_PREFIX=%TMPINS%

:RPT
@if "%~1x" == "x" goto GOTCMD
@if "%~1x" == "NOPAUSEx" (
@set DOPAUSE=echo No pause requested
) else (
@REM Assume a cmake option
@set TMPOPTS=%TMPOPTS% %1
)
@shift
@goto RPT
:GOTCMD

@echo Build %DATE% %TIME% > %TMPLOG%
@echo Build source %TMPSRC%... all output to build log %TMPLOG%
@echo Build source %TMPSRC%... all output to build log %TMPLOG% >> %TMPLOG%

cmake %TMPSRC% %TMPOPTS% >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR1

cmake --build . --config Debug >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR2

cmake --build . --config Release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR3
:DONEREL

@fa4 "***" %TMPLOG%
@call elapsed %TMPBGN%
@echo Appears a successful build... see %TMPLOG%

@echo Continue with install? Only Ctrl+c aborts...

@%DOPAUSE%

cmake --build . --config Debug  --target INSTALL >> %TMPLOG% 2>&1

cmake --build . --config Release  --target INSTALL >> %TMPLOG% 2>&1

@fa4 " -- " %TMPLOG%
@echo.
@call elapsed %TMPBGN%
@echo All done... see %TMPLOG%
@echo.
@goto END

:NOCM
@echo Error: Can NOT locate %TMPCM%
@goto ISERR

:ERR1
@echo cmake configuration or generations ERROR
@goto ISERR

:ERR2
@echo ERROR: Cmake build Debug FAILED!
@goto ISERR

:ERR3
@fa4 "mt.exe : general error c101008d:" %TMPLOG% >nul
@if ERRORLEVEL 1 goto ERR33
:ERR34
@echo ERROR: Cmake build Release FAILED!
@goto ISERR
:ERR33
@echo Try again due to this STUPID STUPID STUPID error
@echo Try again due to this STUPID STUPID STUPID error >>%TMPLOG%
cmake --build . --config Release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR34
@goto DONEREL

:NOSRC
@echo Oops, source %TMPSRC% does NOT EXIST...
@echo Maybe install with updjpeg.bat from zip
@goto ISERR

:ISERR
@echo See %TMPLOG% for details...
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
