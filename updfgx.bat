@setlocal
@set TMPDIR=fgx
@echo Update %TMPDIR%...
@set TMPREPO=git@github.com:fgx/fgx.git
@if "%~1x" == "x" goto GOTCMD
@set TMPDIR=%1
:GOTCMD

@if NOT EXIST %TMPDIR%\. goto CHECKOUT
@cd %TMPDIR%
@call git status
@cd ..
@echo.
@echo Found folder [%TMPDIR%]!
@echo This looks like an UPDATE only
@echo Will action 
@echo cd %TMPDIR%; git pull
@echo.
@echo *** CONTINUE? *** Only Ctrl+c to abort. All other keys continue...
@echo.
@pause

@cd %TMPDIR%
call git pull
@cd ..

@goto END

:CHECKOUT
@echo.
@echo Can not find folder [%TMPDIR%]!
@echo This looks like a fresh clone...
@echo Will action 
@echo git clone %TMPREPO% %TMPDIR%
@echo.
@echo *** CONTINUE? *** Only Ctrl+c to abort. All other keys continue...
@echo.
@pause

call git clone %TMPREPO% %TMPDIR%
@if NOT EXIST %TMPDIR%\. goto FAILED
@echo All done with [%TMPDIR%]
@goto END

:FAILED
@echo ERROR: Appears git clone FAILED...
@goto END

:END
