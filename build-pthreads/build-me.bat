@setlocal

@set TMPSRC=X:\pthreads
@set TMPOPTS=-DCMAKE_INSTALL_PREFIX:PATH=X:\3rdParty

@if NOT EXIST %TMPSRC%\nul goto NOSRC

@if EXIST build-cmake.bat (
call build-cmake
@if ERRORLEVEL 1 goto BCMERR
)

@if NOT EXIST %TMPSRC%\CMakeLists.txt goto NOCM

@set TMPLOG=bldlog-1.txt
@echo Being build %TIME% >%TMPLOG%

cmake %TMPSRC% %TMPOPTS% >>%TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR1

cmake --build . --config Debug >>%TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR2

cmake --build . --config Release >>%TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR3

@echo Appears a successful build...
@echo See %TMPLOG% for details...

@echo Continue with INSTALL? %TMPOPTS%
@pause

cmake --build . --config Debug --target INSTALL >>%TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR4

cmake --build . --config Release --target INSTALL >>%TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR5

@echo Appears a successful build and install
@echo See %TMPLOG% for details...

@goto END

:BCMERR
@echo Build cmake FAILED
@goto ISERR

:NOSRC
@echo Can NOT locate source %TMPSRC%
@goto ISERR

:NOCM
@echo Can NOT locate file %TMPSRC%\CMakeLists.txt
@goto ISERR

:ERR1
@echo cmake config, generation failed
@goto ISERR

:ERR2
@echo build debug failed
@goto ISERR

:ERR3
@echo build release failed
@goto ISERR

:ERR4
@echo install debug failed
@goto ISERR

:ERR5
@echo install release failed
@goto ISERR

:ISERR
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
