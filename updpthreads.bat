@setlocal
@REM from :
@REM enter ``anoncvs'' for the password
@REM cvs -d :pserver:anoncvs@sourceware.org:/cvs/pthreads-win32 login
@REM cvs -d :pserver:anoncvs@sourceware.org:/cvs/pthreads-win32 checkout pthreads

@set TMPDIR=pthreads
@if "%CVSROOT%." == "." goto SETROOT
:DOUP

@if NOT EXIST %TMPDIR%\. goto CHECKOUT
cd %TMPDIR%
cvs up -dP
cd ..
@goto END

:CHECKOUT
@echo Unable to locate folder %TMPDIR%
@echo This looks like a NEW checkout
@echo *** CONTINUE? *** Only Ctlr+C to abort. All other keys continue...
@pause

@echo enter anoncvs for the password...
cvs -d :pserver:anoncvs@sourceware.org:/cvs/pthreads-win32 login
@if ERRORLEVEL 1 goto NOLOGIN

cvs -d :pserver:anoncvs@sourceware.org:/cvs/pthreads-win32 checkout %TMPDIR%

@if NOT EXIST %TMPDIR%\. goto NOCHECKOUT

@echo Done... source is in %TMPDIR%

@goto END

:NOCHECKOUT
@echo ERROR: Appear checkout FAILED...
@goto END


:NOLOGIN
@echo Appear login FAILED
@goto END


:SETROOT
call setroot
@if "%CVSROOT%." == "." goto ERRROOT
@goto DOUP

:ERRROOT
@echo CVSROOT not set in the environment ... NO UPDATE DONE ...
@goto END

:end
