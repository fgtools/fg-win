@setlocal
@set TMPDIRS=build-fg build-jpeg build-openal build-osg build-plib build-png build-sg build-tiff build-zlib
@for %%i in (%TMPDIRS%) do @(call :DOCLEAN %%i)
@goto END

:DOCLEAN
@if "%~1x" == "x" goto :EOF
@if NOT EXIST %1\nul goto :EOF
@cd %1
@call cmake-clean
@cd ..
@goto :EOF

:END
