@setlocal
@set TMPEXE=src\Main\Release\metar.exe
@if NOT EXIST %TMPEXE% goto NOEXE
@set TMP3RD=X:\3rdParty.x64\bin
@set TMPZLIB=%TMP3rd%\zlib.dll
@if NOT EXIST %TMPZLIB% goto NOZLIB

@if "%~1x" == "x" goto NOICAO

@set PATH=%TMP3RD%;%PATH%

%TMPEXE% %1

@goto END

:NOICAO
@echo.
@echo Add the ICAO of the airport to fetch the metar from...
@echo.
@goto END


:NOEXE
@echo Can NOT locate %TMPEXE%! Has it been built?
@goto END

:NOZLIB
@echo Can NOT locate %TMPZLIB%! Needed to run %TMPEXE%
@goto END


:END
