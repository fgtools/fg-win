@setlocal
@set TMPDRV=X:
@set TMPPRJ=flightgear
@echo Build %TMPPRJ% project, in 64-bits
@set TMPLOG=bldlog-1.txt
@set BLDDIR=%CD%
@set TMPVERS=
@set TMPSRC=%TMPDRV%\%TMPPRJ%%TMPVERS%
@set SET_BAT=%ProgramFiles(x86)%\Microsoft Visual Studio 10.0\VC\vcvarsall.bat
@if NOT EXIST "%SET_BAT%" goto NOBAT
@set TMPOPTS=-G "Visual Studio 10 Win64"
@set DOPAUSE=pause

@REM To find OSG, and 3rdParty headers, libraries
@set TMP3RD=%TMPDRV%\3rdParty.x64
@set TMPOSG=%TMPDRV%\install\msvc100-64\OpenSceneGraph
@set TMPBOOST=%TMPDRV%\install\msvc100-64\boost
@set TMPSG=%TMPDRV%\install\msvc100-64\simgear

:RPT
@if "%~1x" == "x" goto DNCMD
@if "%~1x" == "NOPAUSEx" (
@set DOPAUSE=echo No pause requested
) else (
@echo Only one command NOPAUSE allowed
@goto ISERR
)
@shift
@goto RPT
:DNCMD

@call chkmsvc %TMPPRJ%

@echo Begin %DATE% %TIME%, output to %TMPLOG%
@echo Begin %DATE% %TIME% > %TMPLOG%

@REM ############################################
@REM NOTE: SPECIAL INSTALL LOCATION
@REM Adjust to suit your environment
@REM ##########################################
@if /I "%PROCESSOR_ARCHITECTURE%" EQU "AMD64" (
@set TMPINST=%TMPDRV%\install\msvc100-64\%TMPPRJ%
) ELSE (
 @if /I "%PROCESSOR_ARCHITECTURE%" EQU "x86_64" (
@set TMPINST=%TMPDRV%\install\msvc100-64\%TMPPRJ%
 ) ELSE (
@echo ERROR: Appears 64-bit is NOT available - *** FIX ME ***
@goto ISERR
 )
)

@echo Doing: 'call "%SET_BAT%" %PROCESSOR_ARCHITECTURE%'
@echo Doing: 'call "%SET_BAT%" %PROCESSOR_ARCHITECTURE%' >> %TMPLOG%
@call "%SET_BAT%" %PROCESSOR_ARCHITECTURE% >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR0

@REM call setupqt64
@cd %BLDDIR%

:DNARCH
@REM ===============================================================================
@REM This is BIG pieces of jiggey pokey - just hope it works
@if "%DXSDK_DIR%x" == "x" (
@echo Microsoft DirectX DXSDK_DIR NOT set in environment!
@goto ISERR
)
@if NOT EXIST X:\scripts\path2dos.bat (
@echo Can NOT locate 'X:\scripts\path2dos.bat'! *** FIX ME ***
@goto ISERR
)

@REM Can NOT get this path, which contains SPACES to work
@REM Also NOTE the 'sneaky' place where this dxguid.lib is 'hidden'
@REM See : http://wiki.secondlife.com/wiki/Common_compilation_problems#Cannot_open_input_file_.27dxguid.lib.27
@REM There is also an x86 folder
@if EXIST X:\scripts\temp\temppath.txt @del X:\scripts\temp\temppath.txt
@call X:\scripts\path2dos.bat "%DXSDK_DIR%Lib\x64"
@set /P TMPDX=<X:\scripts\temp\temppath.txt
@if NOT EXIST %TMPDX%\nul (
@echo Can NOT locate DirectX directory %TMPDX%!
@goto ISERR
)
@if NOT EXIST %TMPDX%\dxguid.lib (
@echo Can NOT locate DirectX library %TMPDX%\dxguid.lib!
@goto ISERR
)
@REM Thought these 'should' work...
@REM set LIB=%TMPDX%;%LIB%
@REM set LIBDIR=%TMPDX%;%LIBDIR%
@REM But eventually HAD to add this to tell the linker where to look!
@set LINK=/LIBPATH:%TMPDX%

@set TMPOPTS=%TMPOPTS% -DCMAKE_INSTALL_PREFIX=%TMPINST%
@set TMPOPTS=%TMPOPTS% -DCMAKE_PREFIX_PATH:PATH=%TMP3RD%;%TMPOSG%;%TMPSG%
@REM ###################################################################
@REM ##### These should be turned ON occasionally, if they now build ok
@REM Add this if fails to link fgpanel - needs freeglut
@set TMPOPTS=%TMPOPTS% -DWITH_FGPANEL:BOOL=OFF
@REM Disable fgjs
@REM set TMPOPTS=%TMPOPTS% -DENABLE_FGJS:BOOL=OFF
@REM Location of base fgdata directory
@set TMPOPTS=%TMPOPTS% -DFG_DATA_DIR=X:/fgdata

@if NOT EXIST %TMPSRC%\nul goto NOSRC
@if EXIST build-cmake.bat (
@call build-cmake >> %TMPLOG%
)
@if NOT EXIST %TMPSRC%\CMakeLists.txt goto NOCM
@if NOT EXIST %TMPBOOST%\nul goto NOBOOST

@echo. >> %TMPLOG%
@echo Deal with BOOST installed location >> %TMPLOG%
@set BOOST_INCLUDEDIR=%TMPBOOST%\include\boost-1_53
@echo Added ENV BOOST_INCLUDEDIR=%BOOST_INCLUDEDIR% >> %TMPLOG%
@set BOOST_ROOT=%TMPBOOST%
@echo Added ENV BOOST_ROOT=%BOOST_ROOT% >> %TMPLOG%
@set BOOST_LIBRARYDIR=%TMPBOOST%\lib
@echo Added ENV BOOST_LIBRARYDIR=%BOOST_LIBRARYDIR% >> %TMPLOG%
@REM ########################################################################
@REM ##### if really need to see what FindBost.cmake is doing, add this #####
@REM set TMPOPTS=%TMPOPTS% -DBoost_DEBUG=1
@REM echo Added -DBoost_DEBUG=1 to add boost search output >> %TMPLOG%
@REM ########################################################################
@echo. >> %TMPLOG%

@echo Doing: 'cmake %TMPSRC% %TMPOPTS%'
@echo Doing: 'cmake %TMPSRC% %TMPOPTS%' >> %TMPLOG%
@cmake %TMPSRC% %TMPOPTS% >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR1
@echo.
@echo Check %TMPLOG% to ensure 3rdparty items found - *** Only Ctrl+C aborts ***
@echo.
@%DOPAUSE%

@echo Doing: 'cmake --build . --config debug'
@echo Doing: 'cmake --build . --config debug' >> %TMPLOG%
@cmake --build . --config debug >> %TMPLOG%
@if ERRORLEVEL 1 goto ERR2

@echo Doing: 'cmake --build . --config RelWithDebInfo'
@echo Doing: 'cmake --build . --config RelWithDebInfo' >> %TMPLOG%
@cmake --build . --config RelWithDebInfo >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR6

@echo Doing: 'cmake --build . --config release'
@echo Doing: 'cmake --build . --config release' >> %TMPLOG%
@cmake --build . --config release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR3
:DNREL

@echo.
@echo Appears a successful build
@echo.
@echo Note install location %TMPINST%
@echo *** CONTINUE with install? *** Only Ctrl+C aborts ***
@echo.
@%DOPAUSE%

@echo Doing: 'cmake --build . --config debug --target INSTALL'
@echo Doing: 'cmake --build . --config debug --target INSTALL' >> %TMPLOG%
@cmake --build . --config debug --target INSTALL >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR4

@echo Doing: 'cmake --build . --config release --target INSTALL'
@echo Doing: 'cmake --build . --config release --target INSTALL' >> %TMPLOG%
@cmake --build . --config release --target INSTALL >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR5

@REM to 'see' what was installed, add this
@fa4 " -- " %TMPLOG%
@echo.
@echo Done build and install of %TMPPRJ%...
@echo.
@goto END

:NOSRC
@echo Error: Can NOT locate SOURCE %TMPSRC%! *** FIX ME ***
@goto ISERR

:NOCM
@echo Error: Can NOT locate FILE %TMPSRC%\CMakeLists.txt! *** FIX ME ***
@goto ISERR


:NOBAT
@echo Can NOT locate MSVC setup batch "%SET_BAT%"! *** FIX ME ***
@goto ISERR

:ERR0
@echo MSVC 10 setup error
@goto ISERR

:ERR1
@echo cmake config, generation error
@goto ISERR

:ERR2
@echo debug build error
@goto ISERR

:ERR3
@fa4 "mt.exe : general error c101008d:" %TMPLOG% >nul
@if ERRORLEVEL 1 goto ERR32
:ERR33
@echo release build error
@goto ISERR
:ERR32
@echo Stupid error... trying again...
@echo Doing: 'cmake --build . --config release'
@echo Doing: 'cmake --build . --config release' >> %TMPLOG%
@cmake --build . --config release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR33
@goto DNREL

:ERR4
@echo debug install error
@goto ISERR

:ERR5
@echo release install error
@goto ISERR

:ERR6
@echo RelWithDebInfo build error
@goto ISERR

:NOBOOST
@echo Error: Unable to locate boost %TMPBOOST%! *** FIX ME ***
@goto ISERR

:ISERR
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
