@setlocal
@set TMPEXE=Release\atlasg.exe

@if NOT EXIST %TMPEXE% goto ERR1

@set TMP3RD=C:\FG\18\3rdParty\bin
@REM set TMP3RD=C:\FG\17\3rdParty\bin
@REM set TMP3RD2=C:\FG\16\3rdParty\bin

@if NOT EXIST %TMP3RD%\nul goto ERR2
@REM if NOT EXIST %TMP3RD2%\nul goto ERR3

@set TMPCMD=
:RPT
@if "%~1x" == "x" goto GOTCMD
@set TMPCMD=%TMPCMD% %1
@shift
@goto RPT
:GOTCMD

@set PATH=%TMP3RD%;%PATH%
@echo Add %TMP3RD% to PATH...

%TMPEXE% %TMPCMD%

@goto END

:ERR1
@echo Can NOT find %TMPEXE%
@goto END

:ERR2
@echo Can NOT find folder %TMP3RD%
@goto END

:ERR3
@echo Can NOT find folder %TMP3RD2%
@goto END

:END
