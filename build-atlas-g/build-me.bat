@setlocal
@set TMPBGN=%TIME%

@set ADDUPD=0
@set ADDINST=1
@set ADDDBG=1
@set ADDCLEAN=0
@set ADDDEBUG=
@REM set ADDDEBUG=-v9 -d extra

@REM Get to the right FOLDER
@REM call hfg-current

@set TMPPROJ=atlas-g
@set TMPPRJ=atlas-g
@set TMPVERS=
@set TMPMSVC=msvc100

@set TMPBASE=X:
@set TMPSRC=%TMPBASE%\%TMPPROJ%%TMPVERS%
@set TMPGEN=Visual Studio 10
@set TMPDRV=X:
@if NOT EXIST %TMPDRV%\nul goto NOXDRV

@set TMPINST=%TMPDRV%\3rdParty
@set TMPOSG=%TMPDRV%\install\%TMPMSVC%\openscenegraph
@set TMPSG=%TMPDRV%\install\%TMPMSVC%\simgear
@set TMPBOOST=%TMPDRV%\boost_1_53_0
@REM Check these install location
@if NOT EXIST %TMPOSG%\nul goto NOOSG
@if NOT EXIST %TMPSG%\nul goto NOSG
@if NOT EXIST %TMPBOOST%\nul goto NOBOOST

@REM Setup for the BUILD
@set TMPJOB=%JOB_NAME%
@set TMPDST=%WORKSPACE%
@set TMPVER=%BUILD_NUMBER%

@set TMP3RD=%TMPDRV%\3rdParty
@set TMPIPATH=%TMP3RD%\include
@set TMPLPATH=%TMP3RD%\lib
@set TMPOPTS=
@REM set TMPOPTS=-DMSVC_3RDPARTY_ROOT=%TMPBASE%

@REM Set the VERSION LOG file
@set TMPLOG=bldlog-1.txt
@set TMPCZIP=%TMPDST%\%TMPPROJ%-cmake-%TMPVER%.zip

@echo Bgn %DATE% %TIME% to %TMPLOG%
@REM Restart LOG
@echo. > %TMPLOG%
@echo Bgn %DATE% %TIME% >> %TMPLOG%

@echo Begin in folder %CD% >> %TMPLOG%

@REM Check we are in the right place, and can FIND the SOURCE
@if NOT EXIST %TMPSRC%\nul goto NOSRC

@echo Building in folder %CD% >> %TMPLOG%

@REM Check for primary CMakeLists.txt file
@if NOT EXIST %TMPSRC%\CMakeLists.txt goto NOSRC2

@REM All looks ok to proceed with BUILD

@REM echo Establish MSVC + SDK environment >> %TMPLOG%
@REM call set-msvc-sdk >> %TMPLOG%
@REM echo NUM 2: ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%

@echo Set more ENVIRONMENT, if any >> %TMPLOG%
@set OSG_DIR=%TMPOSG%
@echo Set ENVIRONMENT OSG_DIR=%OSG_DIR% >> %TMPLOG%
@set BOOST_ROOT=%TMPBOOST%
@echo Set ENVIRONMENT BOOST_ROOT=%BOOST_ROOT% >> %TMPLOG%
@REM set BOOST_INCLUDEDIR=%TMPBOOST%
@REM echo Set ENVIRONMENT BOOST_INCLUDEDIR=%BOOST_INCLUDEDIR% >> %TMPLOG%
@set SIMGEAR_DIR=%TMPSG%
@echo Set ENVIRONMENT SIMGEAR_DIR=%SIMGEAR_DIR% >> %TMPLOG%

@echo Do cmake configure and generation >> %TMPLOG%
@set TMPOPTS=%TMPOPTS% -DCMAKE_PREFIX_PATH=%TMP3RD%;%TMPSG%;%TMPOSG%;%TMPBOOST%
@set TMPOPTS=%TMPOPTS% -DCMAKE_INSTALL_PREFIX=%TMPINST%

@if NOT EXIST no-cmake.txt goto DOCM
@echo.
@echo WARNING: presence of no-cmake.txt STOPS cmake config/gen!
@echo.
@goto DNCM
:DOCM

@echo Doing: cmake %TMPSRC% -G "%TMPGEN%" %TMPOPTS%
@echo Doing: cmake %TMPSRC% -G "%TMPGEN%" %TMPOPTS% >> %TMPLOG%
@cmake %TMPSRC% -G "%TMPGEN%" %TMPOPTS% >> %TMPLOG% 2>&1
@echo Done: cmake %TMPSRC% -G "%TMPGEN%" %TMPOPTS% >> %TMPLOG%
@if ERRORLEVEL 1 goto NOCM

:DNCM

@if NOT "%ADDDBG%x" == "1x" (
@echo NOTE: Debug build is DISABLED! >> %TMPLOG%
@goto DONE
)

@echo Do cmake Debug build >> %TMPLOG%

@echo Doing: 'cmake --build . --config Debug'
@echo Doing: 'cmake --build . --config Debug' >> %TMPLOG%
@cmake --build . --config Debug >> %TMPLOG%
@echo Done: 'cmake --build . --config Debug' ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@if ERRORLEVEL 1 goto NOBLD

@echo Install and generate the Debug ZIP in a temporary folder >> %TMPLOG%
@if EXIST build-zip.bat (
@echo Doing 'call build-zip Debug' to generate %TMPPROJ% zip >> %TMPLOG%
call build-zip Debug >> %TMPLOG%
@if ERRORLEVEL 1 goto ZIPERR
) else (
@echo Note build-zip.bat does NOT exist in folder %CD%, so NO Debug ZIP created! >> %TMPLOG%
)

:DONE

@echo Do cmake Release build >> %TMPLOG%

@echo Doing: 'cmake --build . --config Release'
@echo Doing: 'cmake --build . --config Release' >> %TMPLOG%
@cmake --build . --config Release >> %TMPLOG% 2>&1
@echo Done: 'cmake --build . --config Release' ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@if ERRORLEVEL 1 goto NOBLD

@echo Install and generate the Release ZIP in a temporary folder >> %TMPLOG%
@if EXIST build-zip.bat (
@echo Doing 'call build-zip Release' to generate %TMPPROJ% zip >> %TMPLOG%
call build-zip Release >> %TMPLOG%
@if ERRORLEVEL 1 goto ZIPERR
) else (
@echo Note build-zip.bat does NOT exist in folder %CD%, so NO Release ZIP created! >> %TMPLOG%
)

@if "%ADDINST%x" == "1x" (
@echo Doing: 'cmake --build . --config Debug --target INSTALL'
@echo Doing: 'cmake --build . --config Debug --target INSTALL' >> %TMPLOG%
@cmake --build . --config Debug --target INSTALL >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto NOINST

@echo Doing: 'cmake --build . --config Release --target INSTALL'
@echo Doing: 'cmake --build . --config Release --target INSTALL' >> %TMPLOG%
@cmake --build . --config Release --target INSTALL >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto NOINST
)

@echo.
@fa4 " -- " %TMPLOG%
@echo.
@echo Looks successful build and install >> %TMPLOG%

@if NOT "%ADDINST%x" == "1x" (
@echo NOTE: Install to %TMPINST% is DISABLED >> %TMPLOG%
)
@call elapsed %TMPBGN% >> %TMPLOG%
@echo End %DATE% %TIME% ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@call elapsed %TMPBGN%
@echo End %DATE% %TIME% Success
@echo.
@goto END

:NOXDRV
@echo No X: drive! Run setupx.bat first
@goto ISERR

:ZIPERR
@echo ERROR: build ZIP failed! returning ERRORLEVEL=%ERRORLEVEL%
@goto ISERR

:NOINST
@echo ERROR: CMake install error! ERRORLEVEL=%ERRORLEVEL%! >> %TMPLOG%
@goto ISERR

:NOBLD
@echo ERROR: CMake build error! ERRORLEVEL=%ERRORLEVEL%! >> %TMPLOG%
@goto ISERR

:NOCM
@echo ERROR: CMake configure and generation error! ERRORLEVEL=%ERRORLEVEL%! >> %TMPLOG%
@goto ISERR

:NOSRC
@echo ERROR: Could NOT locate SOURCE! %TMPSRC% >> %TMPLOG%
@goto ISERR

:NOSRC2
@echo ERROR: Could NOT locate SOURCE! %TMPSRC%\CMakeLists.txt >> %TMPLOG%
@goto ISERR

:NOOSG
@echo Error: Can NOT locate OSG %TMPOSG% directory! *** FIX ME ***
@goto ISERR

:NOSG
@echo Error: Can NOT locate SimGear %TMPSG% directory! *** FIX ME ***
@goto ISERR

:NOBOOST
@echo Error: Can NOT locate Boost %TMPBOOST% directory! *** FIX ME ***
@goto ISERR

:ISERR
@echo ERROREXIT! ERRORLEVEL=%ERRORLEVEL%! >> %TMPLOG%
@echo ERROREXIT! ERRORLEVEL=%ERRORLEVEL%!
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
