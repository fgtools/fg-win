# FG Win Project

### README.md - 20150327 - 20140813 - 20140524 - 20140312 - 20140115

Building FlightGear for WIN32. See README.x64.txt for WIN64 builds.

#### Short Form:

For the really impatient, all you need to do is - but you may want to at least check 
the 'Prerequisites:' below...

 1. Clone this repo - https://gitlab.com/fgtools/fg-win.git - which you may 
have already done if reading this...

 2. Change into the directory where you made this clone, and run setup.bat. This 
will establish this directory as the X: drive, and add X:\scripts to your PATH.

 3. Run: build-all.bat [NOPAUSE]. Without the NOPAUSE the build will stop at each install 
step. It will exit if there is an error. If you added NOPAUSE, GO AND HAVE COFFEE ;=))

 4. If not already done, clone fgdata. You can use updfgd.bat... This takes some time...
If you already have it somewhere, update it with 'git pull', and ADJUST run-fg.bat
accordingly.

 5. Run: run-fg.bat, and you should be FLYING ;=))

Note: The total disk usage is about 25 GB. Make sure you have the space.

Good luck...

#### Long Form:

An attempt to provide a complete windows build environment in a Windows machine. 
Here using WIndows 7, but should work with most recent windows versions. When 
completed I will try a test in Vista! But do not think this will work in earlier 
windows!?

In essence this flightgear build is based on the great flightgear wiki -
 http://wiki.flightgear.org/Building_using_CMake_-_Windows
In particular using the DIRECTORY STRUCTURE shown. This REPO clone forms the 
main ${MSVC_3RDPARTY_ROOT} folder. 

ALL THE BATCH FILES USED USE X: TO REPRESENT THIS ROOT FOLDER.

So you can clone this REPO into ANYWHERE, and use the windows 'subst' command 
to setup a virtual X: drive.

In my case I cloned this into F:\FG\18, so I use the 'subst' command -
 > subst X: F:\FG\18
This 'subst' need only be issued once, and thus could be automated during the boot.

There is a 'setup.bat' that can be used. It will set X: to what ever directory
it is run in.

20140206:  
Successfully completed the FGFS.exe build, so create a tag 1.0.0, and a 
branch 1.0.0 to mark this milestone before moving onto MORE components.

====================================================================
#### Prerequisites:

###### The following MUST be installed in the system.

 * Microsoft Visual C/C++ - http://www.visualstudio.com/en-us/downloads#d-2010-express
 
Here using 'Visual Studio 10', MSVC10, but should work with later versions.

 * Micrsoft DirectX SDK - http://www.microsoft.com/en-us/download/details.aspx?id=6812
 
For OpenAL-soft compile - here using 
-- Using DirectX SDK directory: ENV: DXSDK_DIR=C:\Program Files (x86)\Microsoft DirectX SDK (June 2010)\
This needs to be changed of installed elsewhere

 * CMake - http://www.cmake.org/
 
Here using CMake 2.8.7, but should work with later version.

 * git - http://msysgit.github.io/
 
Here using git version 1.7.9.msysgit.0, Feb 2012, but later version should be ok.

 * Unzip - http://www.winzip.com/downwz.htm - *** THIS IS IMPORTANT ***
 
In many cases the stable release zips of 3rdParty libraries come in ZIP file 
form, and are included in the REPO. So an UNZIP utility is NECESSARY!

Here using 7zip, through a batch file unzip8.bat - http://www.7-zip.org/download.html. 
Have also used Winzip command line utility... But perhaps could be any other 
unzipping utility, adjusting the x:\scripts\unzip8.bat accordingly.

Check the current X:\scripts\unzip8.bat, and adjust to your needs.

This batch file is called with -
 * @call unzip8 -d %TMPSRC%
The -d option tells winzip to use the folders, but most other unzip
utilities do this be default, so for say 7zip that unzip8.bat could be -
 * @7z x %2

Setting up this UNZIP8.bat is the *** MOST IMPORTANT *** first step. After that 
it should ALL BE EASY ;=))

Setup:

#### SETUP.BAT:

After cloning this repo, you should be ready to build, first all the 
3rdParty components, then moving onto SimGear, and eventually FlightGear.

The first thing is to make this 'root' directory the X: drive, and 
add X:\scripts into your PATH. There is a setup.bat to do this.

Where available the 3rdParty library source are in zip form, and in each 
case there is an upd<proj>.bat, to get the source, and a build-<proj>
folder containing a build-me.bat.

NOTE: Sometimes the BUILD ORDER IS IMPORTANT. Like for sure libraries png and zlib 
MUST be done BEFORE OpenSceneGraph (OSG), since these are needed to add the 
minimum plugin for simgear/flightgear use.

#### BUILD-ALL.BAT:

If you are feeling really BRAVE, just type in > build-all [NOPAUSE].
Without the 'NOPAUSE' option the batch will pause in lots of places,
usually before cloning a repo source, and before the install of the 
components.

##### NOTE: The batch file will EXIT on the first ERROR in the BUILD.

#### UPDFGD.BAT: *** CLONING FGDATA ***

The 'bigest' task is the cloning of 'fgdata'. At a minimum this 
takes 3 hours, and consumes about 17 GB of disk space.

And it has been reported that it quite frequently 'breaks' for 
some reason or the other.

So if you have not previously cloned 'fgdata', and kept this up-to-date
then you should start this soonest...

#### The source list in detail

In essence, each source is just -

 1. run the upd<project>.bat to unpack or clone the source.
 2. change into the appropriate build-<project> folder, and run the build-me.bat.
 
Sometimes the upd??.bat and folder names have been shortened, like say 'simgear' 
is 'updsg.bat, and 'build-sg' directory for the build-me.bat

 * Boost - http://www.boost.org/users/download/

Run updboost.bat. As mentioned above each of these 'update' batch file 
use an unzip8.bat which uses 7z, but could also use -
"C:\Program Files (x86)\WinZip\WZUNZIP.EXE"
the command line unzip form of WinZip...

No building of this source is initially needed.

 * ZLIB - http://zlib.net/

Run updzlib.bat. (see notes on unzipping). This will create the zlib-1.2.8 
source folder. Possibly later version of this library could be used.

Change to directory 'build-zlib', and run the build-zlib.bat. This should 
compile and link the zlib DLL and static libraries, and install them into 
the 3rdParty folder.

 * LibPng - http://www.libpng.org/pub/png/pngcode.html

Run updpng.bat. (see notes on unzipping). This will create a lpng1514
source folder. Perhaps a later source could be used.

Change to directory 'build-png', and run the build-me.bat. This should
compile and link the PNG DLL and static libraries, and install them into 
the 3rdParty folder.

 * jpeg - http://www.ijg.org/
 * jbig - SKIPPED
 * LibTIFF - http://libtiff.org/ 
 * crashrpt - https://code.google.com/p/crashrpt/

These are NOT strictly required for a FG build, so can be skipped!

 * jpeg - http://www.ijg.org/

Run updjpeg.bat. (see notes on unzipping). This will create a jpeg-9
source folder. Perhaps a later source could be used, if one exists, but note 
the CMakeLists.txt in the build-jpeg folder is based on this source, and may 
need adjustment for others.

Change to directory 'build-jpeg', and run the build-me.bat. This should
compile and link the jpeg static library, and install it with headers into 
the 3rdParty folder.

 * jbig - SKIPPED

 * LibTIFF - http://libtiff.org/ 

Site still shows v3.6.1 as latest! For a long time now, but here using 4.0.3 09/22/2012 

Run updtiff.bat. (see notes on unzipping). This will create the tiff-4.0.3 
source folder. Perhaps later, other sources could be used, but NOTE the 
CMakeLists.txt was built specifically for this version, and may need adjustments.

Change to directory 'build-tiff', and run the build-me.bat. This should
compile and link the libtiff static library, and install it, with headers, into 
the 3rdParty folder.

 * OpenSceneGraph - http://www.openscenegraph.org/index.php/download-section/stable-releases

Run updosg.bat. (see notes on unzipping). This will create the OpenSceneGraph-3.2.0 
source folder. Since this is a native cmake build project, later OSG source could 
probably be used.

Change to directory 'build-osg', and run the build-me.bat. This should
compile and link the osg shared (DLL) libraries, and install then, with headers, into 
install\msvc100\OpenScenegraph folder.

Had to work on the FindFreeType.cmake (local) module, which is copied to the osg source
during the build process. More about this problem in the added README.osg.txt, and in
the FindFreeType.cmake module itself.

 * OpenRTI - https://sourceforge.net/p/openrti/OpenRTI/ci/master/tree/ - *** OPTIONAL ***

This HLA1.3, IEEE-1516 and IEEE-1516E implementation is still in DEVELOPMENT.

 * OpenAL - http://kcat.strangesoft.net/openal.html

Run updopenal.bat. (this requires git). This will create the openal-soft 
source folder. Since this is a native cmake build project, later sources could 
probably be used.

Note: OpenAL build requires the Microsoft DirectX SDK installed.

Change to directory 'build-openal', and run the build-me.bat. This should
compile and link the openal shared (DLL) library, and install them, with headers, into 
the 3rdParty folder.

 * SimGear - http://simgear.sourceforge.net/ (old 2010 site)
             and http://wiki.flightgear.org/SimGear (also quite old)

But do NOT be fooled by the age of these pages. SimGear is an ESSENTIAL component to 
building FlightGear, and is VERY CURRENT. It version marches completely in step 
with FlightGear.

Run updsg.bat. This is the latest GIT source : https://sourceforge.net/p/flightgear/simgear/ci/next/tree/
This will create a 'simgear' source folder. This will be the latest 'next' branch.

In the simgear folder run > git branch -r to view the branches available. This is 
a native cmake build project, so any and all branches should work, but make sure 
when compiling the flightgear you use the SAME branch, and the SAME for the fgdata.

Change to directory 'build-sg', and run the build-me.bat. This should
compile and link the simgear static libraries, and install them, with headers, into 
the install/msvc100/simgear folder.

 * PLIB - http://plib.sourceforge.net/download.html

Although the original 1.8.5 source is a .tar.gz file, here have zipped it into a ZIP 
to use the same 'unzip' tool.

Run updplib.bat. (see notes on unzipping). This will create the plib-1.8.5 
source folder. This is NOT a native cmake build project, so using a hand crafted 
root CMakeLists.txt file, so this may need adjustments if later sources are used, if 
there is even one...

Change to directory 'build-plib', and run the build-me.bat. This should
compile and link the plib static libraries, and install then, with headers, into 
3rdParty, all the header to 3rdParty\include\plib...

 * FreeeGLUT - http://freeglut.sourceforge.net/

Run updfreeglut.bat. This is a 'recent' zip source. It will create a 
freeglut-3.0.0 source directory.

Changed directory to build-freeeglut, and run build-me.bat. The components 
will be installed in 3rdParty, with the headers in /include/GL

 * flightgear - http://flightgear.org - the main TARGET of this build.

Dependencies: Windows

 * REQUIRED: SimGear, Boost, ZLIB, OpenGL [1], OpenAL (openal-soft), OpenSceneGraph (min 3.2), PLIB
 * OPTIONAL: BZip2, FLTK (for fgadmin), RTI, git (for git HEAD revision message only), CrashRpt,
          SQLite3 (alt build-in source), GooglePerfTools (if ENABLE_PROFILE). 

Note, there are other dependencies, mostly optional, for Apple and Linux system.

Run updfg.bat. This is a git source. This will create the flightgear
source folder. This is a native cmake build project.

Change to directory 'build-fg', and run the build-me.bat. This should
compile and link the main flightgear exe, fgfs.exe, and some other 
uitlities, and install them into the install/msvc100/flightgear folder.

###### If the AIM was to just build flightgear, then you can STOP here. ;=))

However if you want to say go on to build say fgrun, and or terragear then lots 
more DEPENDENCIES are needed. Where needed the zip sources are already included in 
the zips folder. And build-<project> foldes have already been created, with appropriate 
build files, including a CMakeLists.txt, if needed.

In some cases the build-me.bat will pause at the 'install' stage, so you usually have to watch 
and wait for the build process to conclude. Eventually I hope to 'remove' all these 
pauses when you run > build-me NOPAUSE, but may have missed some...

The order is not generally important, but it is probably better if you stick to each as listed.

 * freetype2 - http://freetype.org/ - ft253.zip
 
(was freetype-2.5.2.tar.bz2 to freetype-2.5.2.zip - TODO: this can be removed)

Run updfreetype.bat. This is a zip source. This will create freetype-2.5.3 source directory

Change to directory build-ft, and run build-me.bat. This should build, and install the
'static' freetype libraries in 3rdParty/lib, and the include files in 3rdParty/include/freetype2.

Note this 'strange' install location, which is NOT found by the 'standard' cmake FindFreeType.cmake
module. So if you really need to find 'freetype' check out the local FindFreeType.cmake in the 
build-osg folder. This works fine, but could be tidied up more.

 * fltk - http://www.fltk.org/software.php (20120401) - version 1.3.0
 * libxml2 - http://git.gnome.org/browse/libxml2/ - libxml2-2.9.1.zip

 * GLEW - http://glew.sourceforge.net/ - version 1.10.0 - 20130722

Run updglew.bat. This is a ZIP source. This will create the glew-1.10.0 source directory

Change to directory build-glew, and run build-me.bat. This should build, and install the
'static' glew libraries into the 3rdParty folder.

 * atlas-g - https://gitlab.com/fgtools/atlas-g - 20140812

Run updatlas-g.bat. This will clone the above repo into atlas-g source folder

Change directory build-atlas-g, and run build-me.bat. This should build the Atlasg.exe and 
Mapg.exe.

#### Prerequisites:

  * REQURED:  SimGear, ZLIB, JPEG, PNG, PLIB, Boost, OpenGL, GLEW, GLUT(FreeGLUT)
  * OPTIONAL: CURL - only if GetMap option chosen.

#### MORE TODO

terragear - needs boost_system boost_thread, GDAL, libTIFF, CGAL, ..

All the 64-bit builds - see README.x64.txt

Geoff R. McLane  
email: reports _at_ geoffair _dot_ info  
20150626 - 20140813 - 20140524 - 20140312 - 20140208

[1] OpenGL is normally already installed in Windows

; eof
