# gmp.lib/dll build - 20140526
# Hand crafted CMakeLists.txt to build gmp-6.0.0 from source

cmake_minimum_required( VERSION 2.8 )

include (CheckFunctionExists)
include (CheckIncludeFile)
include (TestBigEndian)         # added 20140207 to add an 'endian' define for HOST_FILLORDER

project( gmp )

# to distinguish between debug and release lib
set( CMAKE_DEBUG_POSTFIX "d" )

if (WIN32)
    set (LIB_DEF OFF)
else (WIN32)
    set (LIB_DEF ON)
endif (WIN32)

    
set( LIB_NAME gmp ) # TODO check this
# Allow developer to select is Dynamic or static library built
set( LIB_TYPE STATIC )  # set default static
option( BUILD_SHARED_LIB "Build Shared Library"                          ${LIB_DEF} )
option( BUILD_CXX_LIB "Set ON to build the CXX library"                         OFF )

if(CMAKE_COMPILER_IS_GNUCXX)
    set( WARNING_FLAGS -Wall )
endif(CMAKE_COMPILER_IS_GNUCXX)

if (CMAKE_CXX_COMPILER_ID STREQUAL "Clang") 
   set( WARNING_FLAGS "-Wall -Wno-overloaded-virtual" )
endif() 

if(WIN32 AND MSVC)
    # turn off various warnings
    set(WARNING_FLAGS "${WARNING_FLAGS} /wd4996")
    # foreach(warning 4244 4251 4267 4275 4290 4786 4305)
    #     set(WARNING_FLAGS "${WARNING_FLAGS} /wd\${warning}")
    # endforeach(warning)
    # Got 18 disable warnings compile flags...
    # + C4146: unary minus operator applied to unsigned type, result still unsigned
    foreach(warning 4013 4018 4057 4100 4127 4131 4146 4206 4210 4244 4245 4251 4275 4305 4389 4701 4702 4706 4786 )
        set(WARNING_FLAGS "${WARNING_FLAGS} /wd${warning}")
    endforeach(warning)
    set( MSVC_FLAGS 
       "-DNOMINMAX -D_USE_MATH_DEFINES -D_CRT_SECURE_NO_WARNINGS -D_SCL_SECURE_NO_WARNINGS -D__CRT_NONSTDC_NO_WARNINGS
        -D_CRT_NONSTDC_NO_DEPRECATE -D_CRT_SECURE_NO_DEPRECATE -D_USE_MATH_DEFINES" )
    # if (${MSVC_VERSION} EQUAL 1600)
    #    set( MSVC_LD_FLAGS "/FORCE:MULTIPLE" )
    # endif (${MSVC_VERSION} EQUAL 1600)
    # set( NOMINMAX 1 )
    list(APPEND add_LIBS ws2_32.lib)
    
endif()

set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${WARNING_FLAGS} ${MSVC_FLAGS} -D_REENTRANT" )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${WARNING_FLAGS} ${MSVC_FLAGS} -D_REENTRANT" )
set( CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${MSVC_LD_FLAGS}" )

#define GMP_LIMB_BITS                      32
#define GMP_NAIL_BITS                      0
set(GMP_LIMB_BITS 32)
set(GMP_NAIL_BITS  0)
# TODO: add this definition?
add_definitions( -DHAVE_CONFIG_H )
# can NOT add this  -D__GMP_WITHIN_CONFIGURE

if(BUILD_SHARED_LIB)
   set(LIB_TYPE SHARED)
   message(STATUS "*** Building DLL library ${LIB_TYPE}")
   set(LIBGMP_DLL 1)
else(BUILD_SHARED_LIB)
   message(STATUS "*** Building static library ${LIB_TYPE}")
   set(LIBGMP_DLL 0)
endif(BUILD_SHARED_LIB)

# setup for config.h
# 31 check_include_file items
check_include_file(dlfcn.h HAVE_DLFCN_H)
check_include_file(fcntl.h HAVE_FCNTL_H)
check_include_file(float.h HAVE_FLOAT_H)
check_include_file(inttypes.h HAVE_INTTYPES_H)
check_include_file(invent.h HAVE_INVENT_H)
check_include_file(langinfo.h HAVE_LANGINFO_H)
check_include_file(locale.h HAVE_LOCALE_H)
check_include_file(machine/hal_sysinfo.h HAVE_MACHINE_HAL_SYSINFO_H)
check_include_file(memory.h HAVE_MEMORY_H)
check_include_file(nl_types.h HAVE_NL_TYPES_H)
check_include_file(sstream HAVE_SSTREAM)
check_include_file(stdint.h HAVE_STDINT_H)
check_include_file(stdlib.h HAVE_STDLIB_H)
check_include_file(strings.h HAVE_STRINGS_H)
check_include_file(string.h HAVE_STRING_H)
check_include_file(sys/attributes.h HAVE_SYS_ATTRIBUTES_H)
check_include_file(sys/iograph.h HAVE_SYS_IOGRAPH_H)
check_include_file(sys/mman.h HAVE_SYS_MMAN_H)
check_include_file(sys/param.h HAVE_SYS_PARAM_H)
check_include_file(sys/processor.h HAVE_SYS_PROCESSOR_H)
check_include_file(sys/pstat.h HAVE_SYS_PSTAT_H)
check_include_file(sys/resource.h HAVE_SYS_RESOURCE_H)
check_include_file(sys/stat.h HAVE_SYS_STAT_H)
check_include_file(sys/sysctl.h HAVE_SYS_SYSCTL_H)
check_include_file(sys/sysinfo.h HAVE_SYS_SYSINFO_H)
check_include_file(sys/syssgi.h HAVE_SYS_SYSSGI_H)
check_include_file(sys/systemcfg.h HAVE_SYS_SYSTEMCFG_H)
check_include_file(sys/times.h HAVE_SYS_TIMES_H)
check_include_file(sys/time.h HAVE_SYS_TIME_H)
check_include_file(sys/types.h HAVE_SYS_TYPES_H)
check_include_file(unistd.h HAVE_UNISTD_H)
# 41 check_function_exists items
check_function_exists(alarm HAVE_ALARM)
check_function_exists(attr_get HAVE_ATTR_GET)
check_function_exists(clock HAVE_CLOCK)
check_function_exists(clock_gettime HAVE_CLOCK_GETTIME)
check_function_exists(cputime HAVE_CPUTIME)
check_function_exists(fgetc HAVE_DECL_FGETC)
check_function_exists(fscanf HAVE_DECL_FSCANF)
check_function_exists(optarg HAVE_DECL_OPTARG)
check_function_exists(sys_errlist HAVE_DECL_SYS_ERRLIST)
check_function_exists(sys_nerr HAVE_DECL_SYS_NERR)
check_function_exists(ungetc HAVE_DECL_UNGETC)
check_function_exists(vfprintf HAVE_DECL_VFPRINTF)
check_function_exists(getpagesize HAVE_GETPAGESIZE)
check_function_exists(getrusage HAVE_GETRUSAGE)
check_function_exists(getsysinfo HAVE_GETSYSINFO)
check_function_exists(gettimeofday HAVE_GETTIMEOFDAY)
check_function_exists(localeconv HAVE_LOCALECONV)
check_function_exists(memset HAVE_MEMSET)
check_function_exists(mmap HAVE_MMAP)
check_function_exists(mprotect HAVE_MPROTECT)
check_function_exists(nl_langinfo HAVE_NL_LANGINFO)
check_function_exists(obstack_vprintf HAVE_OBSTACK_VPRINTF)
check_function_exists(popen HAVE_POPEN)
check_function_exists(processor_info HAVE_PROCESSOR_INFO)
check_function_exists(pstat_getprocessor HAVE_PSTAT_GETPROCESSOR)
check_function_exists(raise HAVE_RAISE)
check_function_exists(read_real_time HAVE_READ_REAL_TIME)
check_function_exists(sigaction HAVE_SIGACTION)
check_function_exists(sigaltstack HAVE_SIGALTSTACK)
check_function_exists(sigstack HAVE_SIGSTACK)
check_function_exists(strchr HAVE_STRCHR)
check_function_exists(strerror HAVE_STRERROR)
check_function_exists(strnlen HAVE_STRNLEN)
check_function_exists(strtol HAVE_STRTOL)
check_function_exists(strtoul HAVE_STRTOUL)
check_function_exists(sysconf HAVE_SYSCONF)
check_function_exists(sysctl HAVE_SYSCTL)
check_function_exists(sysctlbyname HAVE_SYSCTLBYNAME)
check_function_exists(syssgi HAVE_SYSSGI)
check_function_exists(times HAVE_TIMES)
check_function_exists(vsnprintf HAVE_VSNPRINTF)

# NOTES FROM UNIX-LOG.TXT
# created files
# config.status: creating demos/pexpr-config.h
# config.status: creating demos/calc/calc-config.h
# config.status: creating gmp.h
# config.status: creating config.h
configure_file( ${CMAKE_SOURCE_DIR}/gmp-h.in ${CMAKE_BINARY_DIR}/gmp.h @ONLY )
configure_file( ${CMAKE_BINARY_DIR}/config.in.cmake ${CMAKE_BINARY_DIR}/config.h @ONLY )
###configure_file( ${CMAKE_SOURCE_DIR}/config.in ${CMAKE_BINARY_DIR}/config.h @ONLY )
include_directories( ${CMAKE_BINARY_DIR} ${CMAKE_SOURCE_DIR} )

# processor type/name - to find the correct gmp-mparam.h
include_directories( mpn/x86 )

set(name gen-fib)
add_executable( ${name} gen-fib.c )
#add_custom_command(
#    PRE_BUILD
#    OUTPUT fib_table.h
#    COMMAND ${name} header 32 0 >fib_table.h
#    DEPENDS gen-fib.c
#)
set(name gen-fac)
add_executable( ${name} gen-fac.c )

set(name gen-bases)
add_executable( ${name} gen-bases.c )

set(name gen-jacobitab)
add_executable( ${name} gen-jacobitab.c )

set(name gen-psqr)
add_executable( ${name} gen-psqr.c )

set(name gen-trialdivtab)
add_executable( ${name} gen-trialdivtab.c )

# SUBDIRS = tests mpn mpz mpq mpf printf scanf rand cxx demos tune doc
#noinst_LTLIBRARIES = libmpf.la
# libmpf_la_SOURCES =
set(dir mpf)
set(name libmpf)
set(${name}_SRCS
    ${dir}/init.c
    ${dir}/init2.c
    ${dir}/inits.c
    ${dir}/set.c
    ${dir}/set_ui.c
    ${dir}/set_si.c
    ${dir}/set_str.c
    ${dir}/set_d.c
    ${dir}/set_z.c
    ${dir}/set_q.c
    ${dir}/iset.c
    ${dir}/iset_ui.c
    ${dir}/iset_si.c
    ${dir}/iset_str.c
    ${dir}/iset_d.c
    ${dir}/clear.c
    ${dir}/clears.c
    ${dir}/get_str.c
    ${dir}/dump.c
    ${dir}/size.c
    ${dir}/eq.c
    ${dir}/reldiff.c
    ${dir}/sqrt.c
    ${dir}/random2.c
    ${dir}/inp_str.c
    ${dir}/out_str.c
    ${dir}/add.c
    ${dir}/add_ui.c
    ${dir}/sub.c
    ${dir}/sub_ui.c
    ${dir}/ui_sub.c
    ${dir}/mul.c
    ${dir}/mul_ui.c
    ${dir}/div.c
    ${dir}/div_ui.c
    ${dir}/cmp.c
    ${dir}/cmp_d.c
    ${dir}/cmp_si.c
    ${dir}/cmp_ui.c
    ${dir}/mul_2exp.c
    ${dir}/div_2exp.c
    ${dir}/abs.c
    ${dir}/neg.c
    ${dir}/get_d.c
    ${dir}/get_d_2exp.c
    ${dir}/set_dfl_prec.c
    ${dir}/set_prc.c
    ${dir}/set_prc_raw.c
    ${dir}/get_dfl_prec.c
    ${dir}/get_prc.c
    ${dir}/ui_div.c
    ${dir}/sqrt_ui.c
    ${dir}/pow_ui.c
    ${dir}/urandomb.c
    ${dir}/swap.c
    ${dir}/get_si.c
    ${dir}/get_ui.c
    ${dir}/int_p.c
    ${dir}/ceilfloor.c
    ${dir}/trunc.c
    ${dir}/fits_sint.c
    ${dir}/fits_slong.c
    ${dir}/fits_sshort.c
    ${dir}/fits_uint.c
    ${dir}/fits_ulong.c
    ${dir}/fits_ushort.c
    ${dir}/fits_s.h
    ${dir}/fits_u.h
    )
add_library( ${name} ${LIB_TYPE} ${${name}_SRCS} )
set_target_properties( ${name} PROPERTIES COMPILE_FLAGS "-D__GMP_WITHIN_GMP" )
list(APPEND add_LIBS ${name})

set(dir mpn/generic)
set(name libmpn)
set(${name}_SRCS
    ${dir}/add.c
    ${dir}/addmul_1.c
    ${dir}/add_1.c
    ${dir}/add_err1_n.c
    ${dir}/add_err2_n.c
    ${dir}/add_err3_n.c
    ${dir}/add_n.c
    ${dir}/add_n_sub_n.c
    ${dir}/bdiv_dbm1c.c
    ${dir}/bdiv_q.c
    ${dir}/bdiv_qr.c
    ${dir}/bdiv_q_1.c
    ${dir}/binvert.c
    ${dir}/broot.c
    ${dir}/brootinv.c
    ${dir}/bsqrt.c
    ${dir}/bsqrtinv.c
    ${dir}/cmp.c
    ${dir}/cnd_add_n.c
    ${dir}/cnd_sub_n.c
    ${dir}/com.c
    ${dir}/comb_tables.c
    ${dir}/copyd.c
    ${dir}/copyi.c
    ${dir}/dcpi1_bdiv_q.c
    ${dir}/dcpi1_bdiv_qr.c
    ${dir}/dcpi1_divappr_q.c
    ${dir}/dcpi1_div_q.c
    ${dir}/dcpi1_div_qr.c
    ${dir}/diveby3.c
    ${dir}/divexact.c
    ${dir}/dive_1.c
    ${dir}/divis.c
    ${dir}/divrem.c
    ${dir}/divrem_1.c
    ${dir}/divrem_2.c
    ${dir}/div_q.c
    ${dir}/div_qr_1.c
    ${dir}/div_qr_1n_pi1.c
    ${dir}/div_qr_1n_pi2.c
    ${dir}/div_qr_1u_pi2.c
    ${dir}/div_qr_2.c
    ${dir}/div_qr_2n_pi1.c
    ${dir}/div_qr_2u_pi1.c
    ${dir}/dump.c
    ${dir}/fib2_ui.c
    ${dir}/gcd.c
    ${dir}/gcdext.c
    ${dir}/gcdext_1.c
    ${dir}/gcdext_lehmer.c
    ${dir}/gcd_1.c
    ${dir}/gcd_subdiv_step.c
    ${dir}/get_d.c
    ${dir}/get_str.c
    ${dir}/gmp-mparam.h
    ${dir}/hgcd.c
    ${dir}/hgcd2.c
    ${dir}/hgcd2_jacobi.c
    ${dir}/hgcd_appr.c
    ${dir}/hgcd_jacobi.c
    ${dir}/hgcd_matrix.c
    ${dir}/hgcd_reduce.c
    ${dir}/hgcd_step.c
    ${dir}/invert.c
    ${dir}/invertappr.c
    ${dir}/jacbase.c
    ${dir}/jacobi.c
    ${dir}/jacobi_2.c
    ${dir}/logops_n.c
    ${dir}/lshift.c
    ${dir}/lshiftc.c
    ${dir}/matrix22_mul.c
    ${dir}/matrix22_mul1_inverse_vector.c
    ${dir}/mode1o.c
    ${dir}/mod_1.c
    ${dir}/mod_1_1.c
    ${dir}/mod_1_2.c
    ${dir}/mod_1_3.c
    ${dir}/mod_1_4.c
    ${dir}/mod_34lsub1.c
    ${dir}/mul.c
    ${dir}/mullo_basecase.c
    ${dir}/mullo_n.c
    ${dir}/mulmid.c
    ${dir}/mulmid_basecase.c
    ${dir}/mulmid_n.c
    ${dir}/mulmod_bnm1.c
    ${dir}/mul_1.c
    ${dir}/mul_basecase.c
    ${dir}/mul_fft.c
    ${dir}/mul_n.c
    ${dir}/mu_bdiv_q.c
    ${dir}/mu_bdiv_qr.c
    ${dir}/mu_divappr_q.c
    ${dir}/mu_div_q.c
    ${dir}/mu_div_qr.c
    ${dir}/neg.c
    ${dir}/nussbaumer_mul.c
    ${dir}/perfpow.c
    ${dir}/perfsqr.c
    ${dir}/popham.c
    ${dir}/powlo.c
    ${dir}/powm.c
    ${dir}/pow_1.c
    ${dir}/pre_divrem_1.c
    ${dir}/pre_mod_1.c
    ${dir}/random.c
    ${dir}/random2.c
    ${dir}/redc_1.c
    ${dir}/redc_2.c
    ${dir}/redc_n.c
    ${dir}/remove.c
    ${dir}/rootrem.c
    ${dir}/rshift.c
    ${dir}/sbpi1_bdiv_q.c
    ${dir}/sbpi1_bdiv_qr.c
    ${dir}/sbpi1_divappr_q.c
    ${dir}/sbpi1_div_q.c
    ${dir}/sbpi1_div_qr.c
    ${dir}/scan0.c
    ${dir}/scan1.c
    ${dir}/sec_aors_1.c
    ${dir}/sec_div.c
    ${dir}/sec_invert.c
    ${dir}/sec_mul.c
    ${dir}/sec_pi1_div.c
    ${dir}/sec_powm.c
    ${dir}/sec_sqr.c
    ${dir}/sec_tabselect.c
    ${dir}/set_str.c
    ${dir}/sizeinbase.c
    ${dir}/sqr.c
    ${dir}/sqrmod_bnm1.c
    ${dir}/sqrtrem.c
    ${dir}/sqr_basecase.c
    ${dir}/sub.c
    ${dir}/submul_1.c
    ${dir}/sub_1.c
    ${dir}/sub_err1_n.c
    ${dir}/sub_err2_n.c
    ${dir}/sub_err3_n.c
    ${dir}/sub_n.c
    ${dir}/tdiv_qr.c
    ${dir}/toom22_mul.c
    ${dir}/toom2_sqr.c
    ${dir}/toom32_mul.c
    ${dir}/toom33_mul.c
    ${dir}/toom3_sqr.c
    ${dir}/toom42_mul.c
    ${dir}/toom42_mulmid.c
    ${dir}/toom43_mul.c
    ${dir}/toom44_mul.c
    ${dir}/toom4_sqr.c
    ${dir}/toom52_mul.c
    ${dir}/toom53_mul.c
    ${dir}/toom54_mul.c
    ${dir}/toom62_mul.c
    ${dir}/toom63_mul.c
    ${dir}/toom6h_mul.c
    ${dir}/toom6_sqr.c
    ${dir}/toom8h_mul.c
    ${dir}/toom8_sqr.c
    ${dir}/toom_couple_handling.c
    ${dir}/toom_eval_dgr3_pm1.c
    ${dir}/toom_eval_dgr3_pm2.c
    ${dir}/toom_eval_pm1.c
    ${dir}/toom_eval_pm2.c
    ${dir}/toom_eval_pm2exp.c
    ${dir}/toom_eval_pm2rexp.c
    ${dir}/toom_interpolate_12pts.c
    ${dir}/toom_interpolate_16pts.c
    ${dir}/toom_interpolate_5pts.c
    ${dir}/toom_interpolate_6pts.c
    ${dir}/toom_interpolate_7pts.c
    ${dir}/toom_interpolate_8pts.c
    ${dir}/trialdiv.c
    ${dir}/udiv_w_sdiv.c
    ${dir}/zero.c
    )
add_library( ${name} ${LIB_TYPE} ${${name}_SRCS} )
set_target_properties( ${name} PROPERTIES COMPILE_FLAGS "-D__GMP_WITHIN_GMP" )
list(APPEND add_LIBS ${name})

if (BUILD_CXX_LIB)
    # libcxx_la_SOURCES =
    set(dir cxx)
    set(name libcxx)
    set(${name}_SRCS
        ${dir}/isfuns.cc
        ${dir}/ismpf.cc
        ${dir}/ismpq.cc 
        ${dir}/ismpz.cc 
        ${dir}/ismpznw.cc
        ${dir}/limits.cc
        ${dir}/osdoprnti.cc
        ${dir}/osfuns.cc
        ${dir}/osmpf.cc
        ${dir}/osmpq.cc
        ${dir}/osmpz.cc
        )
    add_library( ${name} ${LIB_TYPE} ${${name}_SRCS} )
    set_target_properties( ${name} PROPERTIES COMPILE_FLAGS "-D__GMP_WITHIN_GMPXX" )
    list(APPEND add_LIBS ${name})
endif ()

# eof
