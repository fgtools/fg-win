@setlocal
@echo Building all boost 32-bit libraries...
@set LOGFIL=X:\build-boost\bldlog-1.txt
@set BLDLOG= ^>^> %LOGFIL% 2^>1
@set HAVELOG=1

@set TMPBLD=X:\build-boost
@set TMPINST=X:\install
@if NOT EXIST %TMPINST% (
@md %TMPINST%
@if ERRORLEVEL 1 goto NOIDIR
)
@set TMPINST=X:\install\msvc100
@if NOT EXIST %TMPINST% (
@md %TMPINST%
@if ERRORLEVEL 1 goto NOIDIR
)
@set TMPINST=X:\install\msvc100\boost

@set TMPSRC=boost_1_53_0
cd ..
@if NOT EXIST %TMPSRC%\nul (
    @if EXIST updboost.bat (
        call updboost
        @if NOT EXIST %TMPSRC%\nul (
            @echo FAILED to create %TMPSRC%
            @goto ISERR
        )
    )
)
@cd %TMPSRC%
@echo Got boost source in %CD%
@IF %HAVELOG% EQU 1 (
    @echo Got boost source in %CD% > %LOGFIL%
)

@set B2OPTS=install
@set B2OPTS=%B2OPTS% --toolset=msvc
@set B2OPTS=%B2OPTS% --build-type=complete
@set B2OPTS=%B2OPTS% --build-dir=%TMPBLD%
@set B2OPTS=%B2OPTS% --prefix="%TMPINST%"
@set B2OPTS=%B2OPTS% --without-mpi

@if NOT EXIST %TMPINST%\nul (
    @echo Building boost libraries
    @ECHO Doing: 'call .\bootstrap' %BLDLOG%
    @IF %HAVELOG% EQU 1 (
        @ECHO Doing: 'call .\bootstrap' to %LOGFIL%
    )
    call .\bootstrap %BLDLOG%
    @ECHO Doing: '.\b2 %B2OPTS%' %BLDLOG%
    @IF %HAVELOG% EQU 1 (
        @ECHO Doing: '.\b2 %B2OPTS%' to %LOGFIL%
    )
    .\b2 %B2OPTS% %BLDLOG%
) else (
    @echo Install directory %TMPINST% already exists
    @echo Assume boost build and install done
    @echo Rename, Delete, Move %TMPINST% to redo...
)
@goto END

:NOIDIR
@echo Failed to create %TMPINST%
@goto ISERR

:ISERR
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM EOF
