@setlocal
@set TMPDIR=simgear
@REM 20150325 - Update to use sf repo
@set TMPREPO=git://git.code.sf.net/p/flightgear/simgear
@REM set TMPREPO=git://gitorious.org/fg/simgear.git
@set DOPAUSE=pause
@if "%~1x" == "NOPAUSEx" @set DOPAUSE=echo NO PAUSE requested

@if EXIST %TMPDIR%\nul goto UPDATE

@echo Are you SURE continue to do CLONE of SG source?
@echo Will do: call git clone %TMPREPO% %TMPDIR%

@%DOPAUSE%

call git clone %TMPREPO% %TMPDIR%
@echo Done: call git clone %TMPREPO% %TMPDIR%
@if NOT EXIST %TMPDIR%\. goto COFAILED

@cd %TMPDIR%
@call git branch
@cd ..

@REM === seems not required ===
@REM echo Done new checkout of source... changing to 'next'???
@REM call git clone git://gitorious.org/fg/simgear.git %TMPDIR%
@REM call git branch -t -l next origin/next
@REM call git checkout next

@echo Done new clone...
@goto END

:UPDATE
@echo This is an UPDATE of simgear
@cd %TMPDIR%
@call git status
@echo *** CONTINUE? ***
@%DOPAUSE%

@echo In %CD% doing: 'call git pull origin next'
@call git pull
@echo Done: 'call git pull origin next'
@cd ..
@goto END

:COFAILED
@echo ERROR: %TMPDIR% not created - clone FAILED
@endlocal
@exit /b 1

:END
@endlocal

@REM eof
