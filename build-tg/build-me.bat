@setlocal
@set TMPBGN=%TIME%

@set ADDUPD=0
@set ADDINST=0
@set ADDTINST=0
@set ADDDBG=1
@set ADDCLEAN=0
@set ADDDEBUG=
@REM set ADDDEBUG=-v9 -d extra -ll

@REM Get to the right FOLDER
@REM call hfg-current

@set TMPPROJ=terragear
@set TMPPRJ=tg
@set TMPVERS=
@set TMPJENK=win32-%TMPPRJ%
@set TMPMSVC=msvc100
@set DOPAUSE=pause
@if "%~1x" == "NOPAUSEx" set DOPAUSE=echo No pause requested

@set TMPBASE=X:
@set TMPBLD=%TMPBASE%\build-%TMPPRJ%
@set TMPSRC=%TMPBASE%\%TMPPROJ%%TMPVERS%
@set TMPGEN=Visual Studio 10
@set TMPINST=%TMPBASE%\3rdParty
@set TMPIFIL=%TMPBASE%\3rdParty\installed.txt
@set TMPCZIP=%TMPPROJ%-cmake.zip

@REM Setup for the BUILD
@set TMPJOB=%JOB_NAME%
@set TMPDST=%WORKSPACE%
@set TMPVER=%BUILD_NUMBER%

@REM Not used
@set TMPIPATH=%TMPINST%\include
@set TMPLPATH=%TMPINST%\lib

@REM Set OPTIONS, if any
@set TMPOPTS=
@REM set TMPOPTS=-DCMAKE_DEBUG_POSTFIX=d
@REM set TMPOPTS=%TMPOPTS% -DOPENAL_LIB_DIR:PATH=%TMPINST%\lib
@REM set TMPOPTS=%TMPOPTS% -DOPENAL_INCLUDE_DIR:PATH=%TMPINST%\include
@set TMPOPTS=%TMPOPTS% -DCMAKE_INSTALL_PREFIX:PATH=%TMPINST%
@REM Until iconv/gettext are built
@REM set TMPOPTS=%TMPOPTS% -DENABLE_NLS:BOOL=OFF

@if "%TMPJOB%x" == "x" (
@set TMPJOB=%TMPJENK%
)

@REM if "%TMPDST%x" == "x" (
@REM set TMPDST=X:\artifacts\%TMPJOB%
@REM )

@REM if NOT EXIST %TMPDST%\nul (
@REM md %TMPDST%
@REM if ERRORLEVEL 1 goto ISERR
@REM )

@REM Set the VERSION LOG file
@set TMPLOG=bldlog-1.txt
@REM if "%TMPVER%x" == "x" goto GETVER
@REM set TMPLOG=%TMPDST%\bldlog-%TMPVER%.txt
@REM goto GOTVER
@REM :GETVER
@REM set TMPVER=0
@REM :RPT
@REM set /A TMPVER+=1
@REM set TMPLOG=%TMPDST%\bldlog-%TMPVER%.txt
@REM if EXIST %TMPLOG% goto RPT
@REM :GOTVER
@REM set TMPCZIP=%TMPDST%\%TMPPROJ%-cmake-%TMPVER%.zip

@echo Bgn %DATE% %TIME% to %TMPLOG%
@REM Restart LOG
@echo Bgn %DATE% %TIME% > %TMPLOG%
@REM if ERRORLEVEL 1 goto ISERR
@REM echo NUM 1: ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@set CURRDIR=%CD%
@echo Begin in folder %CURRDIR% >> %TMPLOG%

@REM get/update source
@if "%ADDUPD%x" == "1x" (
    @if EXIST ..\upd%TMPPRJ%.bat (
    call git status
    @echo Continue with the UPDATE? Only Ctrl+C aborts
    @%DOPAUSE%
    cd ..
    @call upd%TMPPRJ% NOPAUSE  >> %TMPLOG% 2>&1
    cd %CURRDIR%
    @echo Done %TMPPRJ% source update >> %TMPLOG%
    ) else (
    @echo Update has been set, but can NOT locate ..\upd%TMPPRJ%.bat file, so NO update possible!
    @echo Update has been set, but can NOT locate ..\upd%TMPPRJ%.bat file, so NO update possible! >>%TMPLOG%
    @goto ISERR
    )
)

@REM Check we are in the right place, and can FIND the SOURCE
@if NOT EXIST %TMPSRC%\nul goto NOSRC

@REM if NOT EXIST %TMPBLD%\nul (
@REM md %TMPBLD%
@REM )
@REM if NOT EXIST %TMPBLD%\nul goto NODIR
@REM cd %TMPBLD%
@REM if ERRORLEVEL 1 goto ISERR
@echo Building in folder %CD% >> %TMPLOG%

@REM echo Deal with the CMakeLists.txt files >> %TMPLOG%
@REM if EXIST build-cmake.bat (
@REM echo Copy new cmake list files >> %TMPLOG%
@REM call build-cmake >> %TMPLOG% 2>&1
@REM if ERRORLEVEL 1 goto ISERR
@REM ) else (
@REM echo build-cmake.bat NOT found >> %TMPLOG%
@REM )

@REM Check for primary CMakeLists.txt file
@if NOT EXIST %TMPSRC%\CMakeLists.txt goto NOSRC2

@REM All looks ok to proceed with BUILD
@REM echo Establish MSVC + SDK environment >> %TMPLOG%
@REM call set-msvc-sdk >> %TMPLOG%
@REM echo NUM 2: ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%

@echo Set more ENVIRONMENT, if any >> %TMPLOG%
@REM set OPENAL_LIB_DIR=%TMPINST%\lib
@REM echo Set ENVIRONMENT OPENAL_LIB_DIR=%OPENAL_LIB_DIR% >> %TMPLOG%
@REM set OPENAL_INCLUDE_DIR=%TMPINST%\include
@REM echo Set ENVIRONMENT OPENAL_INCLUDE_DIR=%OPENAL_INCLUDE_DIR% >> %TMPLOG%
@set BOOST_ROOT=%TMPBASE%\install\msvc100\boost
@echo Set BOOST_ROOT=%BOOST_ROOT% >> %TMPLOG%
@REM echo BOOST_LIBRARYDIR=%BOOST_ROOT%\stage\lib
@REM echo Set BOOST_LIBRARYDIR=%BOOST_LIBRARYDIR%

@echo Do cmake configure and generation >> %TMPLOG%

@echo Doing 'cmake %TMPSRC% %TMPOPTS%'  >> %TMPLOG%
cmake %TMPSRC% %TMPOPTS%  >> %TMPLOG% 2>&1
@echo Done 'cmake %TMPSRC% %TMPOPTS%'  ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@if ERRORLEVEL 1 goto NOCM

@if NOT "%ADDDBG%x" == "1x" (
@echo NOTE: Debug build is DISABLED! >> %TMPLOG%
@goto DONEDBG
)

@echo Do cmake Debug build >> %TMPLOG%

@echo Doing: 'cmake --build . --config Debug' >> %TMPLOG%
cmake --build . --config Debug >> %TMPLOG%
@echo Done: 'cmake --build . --config Debug' ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@if ERRORLEVEL 1 goto NOBLDDBG

@REM if "%TMPTINST%" == "0" (
@REM echo Due to FULL paths can NOT generate DBG ZIP artifacts >> %TMPLOG%
@REM goto DNTINSTDBG
@REM ) 
@REM echo Install and generate the Debug ZIP in a temporary folder >> %TMPLOG%
@REM if EXIST build-zip.bat (
@REM echo Doing 'call build-zip Debug' to generate %TMPPROJ% zip >> %TMPLOG%
@REM call build-zip Debug >> %TMPLOG%
@REM if ERRORLEVEL 1 goto ZIPERR
@REM ) else (
@REM echo Note build-zip.bat does NOT exist in folder %CD%, so NO Debug ZIP created! >> %TMPLOG%
@REM )
@REM :DNTINSTDBG

@if "%ADDINST%x" == "1x" (
@echo Doing: 'cmake -DBUILD_TYPE=Debug -P cmake_install.cmake' >> %TMPLOG%
cmake -DBUILD_TYPE=Debug -P cmake_install.cmake >> %TMPLOG%
@echo Done: 'cmake -DBUILD_TYPE=Debug -P cmake_install.cmake' ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@if ERRORLEVEL 1 goto NOINST

@echo. >> %TMPIFIL%
@echo = %TMPPROJ% Debug install %DATE% %TIME% >> %TMPIFIL%
@if EXIST install_manifest.txt (
type install_manifest.txt >> %TMPIFIL%
copy install_manifest.txt %TMPDST%\install-%TMPPROJ%-dbg.txt >nul
)
)
:DONEDBG

@echo Do cmake Release build >> %TMPLOG%

@echo Doing: 'cmake --build . --config Release' >> %TMPLOG%
cmake --build . --config Release >> %TMPLOG% 2>&1
@echo Done: 'cmake --build . --config Release' ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@if ERRORLEVEL 1 goto NOBLDREL
:DONEREL

@if "%TMPTINST%" == "0" (
@echo Due to FULL paths can NOT generate DBG ZIP artifacts >> %TMPLOG%
@goto DNTINSTREL
) 
@echo Install and generate the Release ZIP in a temporary folder >> %TMPLOG%
@if EXIST build-zip.bat (
@echo Doing 'call build-zip Release' to generate %TMPPROJ% zip >> %TMPLOG%
call build-zip Release >> %TMPLOG%
@if ERRORLEVEL 1 goto ZIPERR
) else (
@echo Note build-zip.bat does NOT exist in folder %CD%, so NO Release ZIP created! >> %TMPLOG%
)
:DNTINSTREL

@if "%ADDINST%x" == "1x" (
@echo Doing: 'cmake -DBUILD_TYPE=Release -P cmake_install.cmake' >> %TMPLOG%
cmake -DBUILD_TYPE=Release -P cmake_install.cmake >> %TMPLOG%  2>&1
@echo Done: 'cmake -DBUILD_TYPE=Release -P cmake_install.cmake' ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@if ERRORLEVEL 1 goto NOINST

@echo. >> %TMPIFIL%
@echo = %TMPPROJ% Release install %DATE% %TIME% >> %TMPIFIL%
@echo Installed to %TMPINST% >> %TMPIFIL%
@if EXIST install_manifest.txt (
type install_manifest.txt >> %TMPIFIL%
copy install_manifest.txt %TMPDST%\install-%TMPPROJ%-rel.txt >nul
)
)

@echo Looks successful >> %TMPLOG%

@REM echo keep the CMakeLists.txt, in '%TMPCZIP%' >> %TMPLOG%
@REM if EXIST %TMPCZIP% del %TMPCZIP% >nul
@REM echo Doing: 'call zip8 -a -p -r -o %TMPCZIP% %TMPSRC%\CMakeLists.txt' >> %TMPLOG%
@REM call zip8 -a -p -r -o %TMPCZIP% %TMPSRC%\CMakeLists.txt >nul 2>&1
@REM if NOT EXIST %TMPCZIP% (
@REM echo WARNING: Appears ZIP of %TMPCZIP% failed, but considered non-critical! >> %TMPLOG%
@REM )

@if NOT "%ADDINST%x" == "1x" (
@echo NOTE: Install to %TMPINST% is DISABLED >> %TMPLOG%
)
@call elapsed %TMPBGN% >> %TMPLOG%
@echo End %DATE% %TIME% ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@call elapsed %TMPBGN%
@echo End %DATE% %TIME% Success
@goto END

:ZIPERR
@echo ERROR: build ZIP failed! returning ERRORLEVEL=%ERRORLEVEL%
@goto ISERR

:NOINST
@echo ERROR: CMake install error! ERRORLEVEL=%ERRORLEVEL%! >> %TMPLOG%
@goto ISERR

:NOBLDDBG
@echo ERROR: Debug build error! ERRORLEVEL=%ERRORLEVEL%! >> %TMPLOG%
@goto ISERR


:NOBLDREL
fa4 "LINK : fatal error LNK1104:" %TMPLOG%
@if ERRORLEVEL 1 (
@echo Repeat release build due this STUPID STUPID STUPID error
cmake --build . --config Release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto NOBLDREL2
@goto DONEREL
)
fa4 "mt.exe : general error c101008d:" %TMPLOG%
@if ERRORLEVEL 1 (
@echo Repeat release build due this STUPID STUPID STUPID error
cmake --build . --config Release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto NOBLDREL2
@goto DONEREL
)
:NOBLDREL2
@echo ERROR: Release build error! ERRORLEVEL=%ERRORLEVEL%! >> %TMPLOG%
@goto ISERR

:NOCM
@echo ERROR: CMake configure and generation error! ERRORLEVEL=%ERRORLEVEL%! >> %TMPLOG%
@goto ISERR

:NOSRC
@echo ERROR: Could NOT locate SOURCE! %TMPSRC% >> %TMPLOG%
@goto ISERR

:NOSRC2
@echo ERROR: Could NOT locate SOURCE! %TMPSRC%\CMakeLists.txt >> %TMPLOG%
@goto ISERR

:NODIR
@echo ERROR: Could NOT create build directory %TMPBLD%! >> %TMPLOG%
@goto ISERR

:ISERR
@echo ERROREXIT! ERRORLEVEL=%ERRORLEVEL%! >> %TMPLOG%
@echo ERROREXIT! ERRORLEVEL=%ERRORLEVEL%!
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
