@setlocal
@REM 20140312 - adjusted to flightgear x: drive build
@set DOINSTALL=1
@set DOTMPINST=0

@set TMPDIR=X:
@set TMPRT=%TMPDIR%
@set TMPVER=1
@set TMPPRJ=fg
@set TMPPROJ=flightgear
@set TMPSRC=%TMPRT%\%TMPPROJ%
@set TMPBGN=%TIME%
@set TMPMSVC=msvc100
@set TMP3RD=%TMPRT%\3rdParty
@set TMPINS=%TMPRT%\install\%TMPMSVC%\%TMPPROJ%
@set TMPCM=%TMPSRC%\CMakeLists.txt
@set TMPOSG=%TMPRT%\install\%TMPMSVC%\OpenSceneGraph
@set TMPSG=%TMPRT%\install\%TMPMSVC%\simgear
@set DOPAUSE=pause

@set TMPLOG=bldlog-%TMPVER%.txt

@REM Deal with the BOOST version and location
@set Boost_ADDITIONAL_VERSIONS="1.53.0"
@REM set Boost_DIR=%TMPRT%\boost_1_53_0
@set BOOST_ROOT=%TMPRT%/boost_1_53_0

@REM ===============================================================================
@REM This is BIG pieces of jiggey pokey - just hope it works
@if "%DXSDK_DIR%x" == "x" (
@echo Microsoft DirectX DXSDK_DIR NOT set in environment!
@goto ISERR
)
@if NOT EXIST X:\scripts\path2dos.bat (
@echo Can NOT locate 'X:\scripts\path2dos.bat'! *** FIX ME ***
@goto ISERR
)

@REM Can NOT get this path, which contains SPACES to work
@REM Also NOTE the 'sneaky' place where this dxguid.lib is 'hidden'
@REM See : http://wiki.secondlife.com/wiki/Common_compilation_problems#Cannot_open_input_file_.27dxguid.lib.27
@REM There is also an x64 folder
@if EXIST X:\scripts\temp\temppath.txt @del X:\scripts\temp\temppath.txt
@call X:\scripts\path2dos.bat "%DXSDK_DIR%Lib\x86"
@set /P TMPDX=<X:\scripts\temp\temppath.txt
@if NOT EXIST %TMPDX%\nul (
@echo Can NOT locate DirectX directory %TMPDX%!
@goto ISERR
)
@if NOT EXIST %TMPDX%\dxguid.lib (
@echo Can NOT locate DirectX library %TMPDX%\dxguid.lib!
@goto ISERR
)
@REM Thought these 'should' work...
@REM set LIB=%TMPDX%;%LIB%
@REM set LIBDIR=%TMPDX%;%LIBDIR%
@REM But eventually HAD to add this to tell the linker where to look!
@set LINK=/LIBPATH:%TMPDX%
@REM See my joyinfo CMakeLists.txt, to see how this could be put in the CMakeLists.txt
@REM Would LOVE to hear about SOME improvments in the above - geoff
@REM ===============================================================================

@REM *************************************************************************************
@REM *** SHOULD NOT NEED TO ALTER ANYTHING BELOW HERE *** It uses all the above varaiables
@REM *************************************************************************************

@REM call chkmsvc %TMPPRJ% 
@REM if EXIST build-cmake.bat (
@REM call build-cmake
@REM )

@if NOT EXIST %TMPCM% goto NOCM

@set TMPOPTS=-DCMAKE_INSTALL_PREFIX=%TMPINS%
@set TMPOPTS=%TMPOPTS% -DCMAKE_PREFIX_PATH:PATH=%TMP3RD%;%TMPSG%;%TMPOSG%
@REM Add this if trouble finding boost
@REM set TMPOPTS=%TMPOPTS% -DBoost_DEBUG:BOOL=ON
@REM Add this if fails to link fgpanl
@set TMPOPTS=%TMPOPTS% -DWITH_FGPANEL:BOOL=OFF

:RPT
@if "%~1x" == "x" goto GOTCMD
@if "%~1x" == "NOPAUSEx" (
@set DOPAUSE=echo No pause requested...
) else (
@set TMPOPTS=%TMPOPTS% %1
)
@shift
@goto RPT
:GOTCMD

@echo Build %DATE% %TIME% > %TMPLOG%
@echo Build source %TMPSRC%... all output to build log %TMPLOG%
@echo Build source %TMPSRC%... all output to build log %TMPLOG% >> %TMPLOG%
@REM echo Set Boost_DIR=%Boost_DIR% >>%TMPLOG%
@echo Set BOOST_ROOT=%BOOST_ROOT% >>%TMPLOG%
@echo Set LIB abd LIBDIR=%LIB% >>%TMPLOG%
@echo set LINK=%LINK% >>%TMPLOG%

cmake %TMPSRC% %TMPOPTS% >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR1

cmake --build . --config Debug >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR2
:DONEDBG

cmake --build . --config RelWithDebInfo >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR5
:DONERELDBG

cmake --build . --config Release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR3
:DONEREL

@REM fa4 "***" %TMPLOG%
@call elapsed %TMPBGN%
@echo Appears a successful build... see %TMPLOG%

@if "%DOTMPINST%x" == "0x" (
@echo Skipping build of artifacts for now...
@goto DONETI
)

@if EXIST build-zip.bat (
@echo Building installation zips... moment...
@REM If paths given as ${CMAKE_INSTALL_PREFIX}
@call build-zip Debug
@call build-zip Release
@REM If full paths used in install
@REM call build-zipsf2 Debug
@REM call build-zipsf2 Release
@echo Done installation zips...
) else (
@echo No build-zip.bat found, so no artifacts created
)

:DONETI

@if "%DOINSTALL%x" == "0x" (
@echo Skipping install for now...
@goto END
)

@echo Continue with install? Only Ctrl+c aborts...
@%DOPAUSE%

cmake --build . --config Debug  --target INSTALL >> %TMPLOG% 2>&1
@if EXIST install_manifest.txt (
@copy install_manifest.txt install_manifest_dbg.txt >nul
@echo. >> %TMPINS%\installed.txt
@echo %TMPPRJ% Debug install %DATE% %TIME% >> %TMPINS%\installed.txt
@type install_manifest.txt >> %TMPINS%\installed.txt
)

cmake --build . --config Release  --target INSTALL >> %TMPLOG% 2>&1
@if EXIST install_manifest.txt (
@copy install_manifest.txt install_manifest_rel.txt >nul
@echo. >> %TMPINS%\installed.txt
@echo %TMPPRJ% Release install %DATE% %TIME% >> %TMPINS%\installed.txt
@type install_manifest.txt >> %TMPINS%\installed.txt
)

@echo.
fa4 " -- " %TMPLOG%
@echo.

@call elapsed %TMPBGN%
@echo All done... see %TMPLOG%

@goto END

:NOCM
@echo Error: Can NOT locate %TMPCM%
@goto ISERR

:ERR1
@echo cmake configuration or generations ERROR
@goto ISERR

:ERR2
@fa4 "LINK : fatal error LNK1104:" %TMPLOG% >nul
@if ERRORLEVEL 1 goto ERR22
:ERR24
@echo ERROR: Cmake build Debug FAILED!
@goto ISERR
:ERR22
@echo Try again due to this STUPID STUPID STUPID error
@echo Try again due to this STUPID STUPID STUPID error >>%TMPLOG%
cmake --build . --config Debug >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR24
@goto DONEDBG

:ERR3
@fa4 "mt.exe : general error c101008d:" %TMPLOG% >nul
@if ERRORLEVEL 1 goto ERR33
:ERR34
@echo ERROR: Cmake build Release FAILED!
@goto ISERR
:ERR33
@echo Try again due to this STUPID STUPID STUPID error
@echo Try again due to this STUPID STUPID STUPID error >>%TMPLOG%
cmake --build . --config Release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR34
@goto DONEREL

:ERR5
@fa4 "LINK : fatal error LNK1104:" %TMPLOG% >nul
@if ERRORLEVEL 1 goto ERR55
:ERR52
@echo ERROR: Cmake build RelWithDebInfo FAILED!
@goto ISERR
:ERR55
@echo Try again due to this STUPID STUPID STUPID error
@echo Try again due to this STUPID STUPID STUPID error >>%TMPLOG%
cmake --build . --config RelWithDebInfo >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR52
@goto DONERELDBG

:ISERR
@echo See %TMPLOG% for details...
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
