@setlocal
@REM need to set
@REM TMPPRJ=proj - short project name
@REM TMPVER=1    - optional version number

@if "%TMPPRJ%x" == "x" goto NOPRJ
@REM Adjust this as required
@set TMPJWS=F:\Projects\workspace
@if NOT EXIST %TMPJWS%\nul goto NOWS

@REM Ok, I want to keep REL and DBG separate, at least for NOW
@set TMPROOT=F:\FG\18\TEMPI
@set TMPTARG=install
@set TMPMSVC=msvc100

@REM DEFAULT to RELEASE
@set TMPCONF=Release
@set TMPCFG=rel
@set TMPZDIR=%TMPJWS%\win32-%TMPPRJ%
@if NOT EXIST %TMPZDIR%\nul (
md %TMPZDIR%
@if NOT EXIST %TMPZDIR%\nul (
@echo ERROR: Failed to create the diestination %TMPZDIR% for the zip file!
@goto ISERR
)
)

@if "%~1x" == "x" goto GOTCMD
@if "%~1x" == "Releasex" (
@set TMPCONF=Release
@set TMPCFG=rel
) else (
@if "%~1x" == "Debugx" (
@set TMPCONF=Debug
@set TMPCFG=dbg
) else (
@echo ERROR: Unknown command [%1] IGNORED - Using %TMPCONF% default
)
)
:GOTCMD
@set TMPZIP=%TMPPRJ%-%TMPMSVC%-%TMPCFG%
@if NOT "%TMPVER%x" == "x" (
@set TMPZIP=%TMPZIP%-%TMPVER%
)
@set TMPZIP=%TMPZIP%.zip

@echo Creating %TMPZIP%...
@if NOT EXIST %TMPROOT%\nul goto NODIR

@set TMPDST=%TMPROOT%\%TMPTARG%

@set TMPZFIL=%TMPZDIR%\%TMPZIP%

@if EXIST %TMPDST%\nul (
@echo Completely DELETING previous %TMPDST%
@echo Doing: 'xdelete -NO-confirmation-please=0 -dfrm %TMPDST%'
xdelete -NO-confirmation-please=0 -dfrm %TMPDST% >nul 2>&1
)

@if EXIST %TMPDST%\nul (
@echo ERROR: Directory %TMPDST% was NOT deleted!
@goto ISERR
)

@REM build up the full destination of the install
@set TMPDST=%TMPDST%\%TMPMSVC%
@md %TMPDST%
@if NOT EXIST %TMPDST%\nul (
@echo ERROR: Directory %TMPDST% was NOT created!
@goto ISERR
)
@set TMPDST=%TMPDST%\flightgear
@md %TMPDST%
@if NOT EXIST %TMPDST%\nul (
@echo ERROR: Directory %TMPDST% was NOT created!
@goto ISERR
)

@if EXIST %TMPZFIL% (
@call dirmin %TMPZFIL%
@echo Renaming above previous ZIP
@call ren2oldbak %TMPZFIL%
)

@echo Doing: 'call geninst %TMPDST% %TMPCONF% NOPAUSE'
call geninst %TMPDST% %TMPCONF% NOPAUSE
@echo Done: 'call geninst %TMPDST% %TMPCONF% NOPAUSE' ERRORLEVEL=%ERRORLEVEL%
@if ERRORLEVEL 1 (
@echo ERROR: geninst FAILED!
@goto ISERR
)

cd %TMPROOT%
@if ERRORLEVEL 1 (
@echo ERROR: unable to do 'cd %TMPROOT%'!
@goto ISERR
)

@echo Changed work directory to %CD%

@REM echo Am I in the right place?
@REM pause

@echo Doing: 'call zip8 -a -r -P -o %TMPZFIL% %TMPTARG%\*.*'
call zip8 -a -r -P -o %TMPZFIL% %TMPTARG%\*.* >nul 2>%1
@echo Done: 'call zip8 -a -r -P -o %TMPZFIL% %TMPTARG%\*.*'
@if ERRORLEVEL 1 (
@echo ERROR: Creation of ZIP FAILED!
@goto ISERR
)

@if NOT EXIST %TMPZFIL% (
@echo Creation of Zip to %TMPZFIL% FAILED!
@goto ISERR
)

@echo Appears zip creation was successful...
@call unzip8 -vb %TMPZFIL%
@call dirmin %TMPZFIL%

@goto END

:NOWS
@echo Error: Directory [%TMPJWS%] does NOT exist
@echo Error: Directory [%TMPJWS%] does NOT exist 1>&2
@goto ISERR

:NOPRJ
@echo ERROR: TMPPRJ not set in environment? Set to short project name
@echo ERROR: TMPPRJ not set in environment? Set to short project name 1>&2
@goto ISERR

:NODIR
@echo ERROR: Directory [%TMPROOT%] does NOT exist...
@echo ERROR: Directory [%TMPROOT%] does NOT exist... 1>&2
@goto ISERR

:ISERR
@REM echo pausing on the ERROR above...
@REM pause
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
