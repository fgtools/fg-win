@if NOT EXIST X:\scripts\nul (
@echo ERROR: Can NOT find X:\scripts directory!
@exit /b 1
)
@if NOT EXIST X:\scripts\unzip8.bat (
@echo ERROR: Can NOT find X:\scripts\unzip8.bat
@exit /b 1
)

@if "%USERNAME%x" == "userx" goto :DOSU
@echo This BATCH file is ONLY for a system with a USER name of 'user'!
@echo It would need modification for other systems... and shuld NOT be needed anyway...
@goto END

:DOSU
@echo WARNING: This is VERY SPECIFIC TO MY SYSTEM
@echo This allows me to isolate my PATH to make sure all items have been take care of...
@echo YOu should NOT run this in any other system!
@echo Use Ctrl+C to abort
@pause
@echo 2nd WARNING: This is specific to MY SYSTEM
@echo Use Ctrl+C to ABORT NOW
@pause
set  PATH=X:\scripts;C:\Program Files (x86)\AMD APP\bin\x86_64;C:\Program Files (x86)\ImageMagick-6.7.7-Q16;C:\Python27\;C:\Python27\Scripts;C:\Perl64\site\bin;C:\Perl64\bin;C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbem;C:\Windows\System32\WindowsPowerShell\v1.0\;C:\Program Files (x86)\ATI Technologies\ATI.ACE\Core-Static;C:\Program Files (x86)\Microsoft SQL Server\100\Tools\Binn\;C:\Program Files\Microsoft SQL Server\100\Tools\Binn\;C:\Program Files\Microsoft SQL Server\100\DTS\Binn\;C:\Program Files (x86)\Subversion\bin;C:\Program Files (x86)\Git\cmd;C:\Program Files\Microsoft Windows Performance Toolkit\;c:\swigwin-2.0.7;c:\Java\apache-ant-1.8.4\bin;C:\Program Files (x86)\Windows Kits\8.0\Windows Performance Toolkit\;C:\Program Files (x86)\Notepad++;C:\Program Files\Java\jdk1.7.0_25\bin;C:\Program Files (x86)\CMake 2.8\bin;C:\Qt\4.8.6\bin;C:\Program Files\Mercurial
@REM Adjusted 20140925
@REM PATH=X:\scripts;C:\Program Files (x86)\AMD APP\bin\x86_64;C:\Program Files (x86)\ImageMagick-6.7.7-Q16;C:\Python27\;C:\Python27\Scripts;C:\Perl64\site\bin;C:\Perl64\bin;C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbem;C:\Windows\System32\WindowsPowerShell\v1.0\;C:\Program Files (x86)\ATI Technologies\ATI.ACE\Core-Static;C:\Program Files (x86)\Microsoft SQL Server\100\Tools\Binn\;C:\Program Files\Microsoft SQL Server\100\Tools\Binn\;C:\Program Files\Microsoft SQL Server\100\DTS\Binn\;C:\Program Files (x86)\Subversion\bin;C:\Program Files (x86)\Git\cmd;C:\Program Files\Microsoft Windows Performance Toolkit\;c:\swigwin-2.0.7;c:\Java\apache-ant-1.8.4\bin;C:\Program Files (x86)\Windows Kits\8.0\Windows Performance Toolkit\;C:\Program Files (x86)\Notepad++;C:\Program Files (x86)\CGAL-4.1\auxiliary\gmp\lib;C:\QtSDK\Desktop\Qt\4.8.0\msvc2010\bin;C:\Program Files\Java\jdk1.7.0_25\bin;C:\Program Files (x86)\CMake 2.8\bin;C:\Program Files\Mercurial

:END
