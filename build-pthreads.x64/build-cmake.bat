@setlocal

@REM NOT POSSIBLE call amsrcs %TMPDEBUG% -r ptheads.inp
@REM NOT POSSIBLE call createdsp %TMPDEBUG% -@pthreads.inp
@set TMPFIL=CMakeLists.txt
@set TMPFIL2=..\build-pthreads\%TMPFIL%
@set TMPSRC=X:\pthreads
@echo TODO: Presently no way to GENERATE %TMPFIL% - is HAND crafted...

@if NOT EXIST %TMPSRC%\nul goto NOSRC
@if NOT EXIST %TMPFIL2% goto NOFIL

@if NOT EXIST %TMPSRC%\%TMPFIL% goto DOCOPY
@fc4 -v0 -q %TMPFIL2% %TMPSRC%\%TMPFIL%
@if ERRORLEVEL 1 goto DOCOPY
@call dirmin %TMPSRC%\%TMPFIL%
@echo Appears no change in %TMPFIL%... nothing done...
@goto DNCOPY
:DOCOPY
@call dirmin %TMPFIL2%
@if EXIST %TMPSRC%\%TMPFIL% @call dirmin %TMPSRC%\%TMPFIL%
@echo Copying %TMPFIL2% to %TMPSRC%
copy %TMPFIL2% %TMPSRC%
@if NOT EXIST %TMPSRC%\%TMPFIL% goto NOCOPY
@call dirmin %TMPSRC%\%TMPFIL%
@echo Done copy of %TMPFIL% to %TMPSRC%
:DNCOPY

@REM Add any other 'created' files

@goto END

:NOCOPY
@echo Copy of %TMPFIL% to %TMPSRC% FAILED!
@goto ISERR

:NOSRC
@echo ERROR: Can NOT locate source [%TMPSRC%]!
@goto ISERR

:NOFIL
@echo ERROR: Can NOT locate file [%TMPFIL2%]!
@goto ISERR

:ISERR
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
