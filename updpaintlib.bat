@setlocal
@echo from : http://sourceforge.net/p/paintlib/code/?source=navbar
@set TMPDIR=paintlib
@if EXIST %TMPDIR%\nul goto DOUPD
@echo A new CVS checkout to %TMPDIR%
@echo *** CONTINUE? *** Only Ctrl+C aborts... other keys continue...
@pause
@echo When prompted for a password for anonymous, simply press the Enter key.

cvs -d:pserver:anonymous@paintlib.cvs.sourceforge.net:/cvsroot/paintlib login 

cvs -z3 -d:pserver:anonymous@paintlib.cvs.sourceforge.net:/cvsroot/paintlib co -P %TMPDIR%

@goto END

:DOUPD

@cd %TMPDIR%

cvs up

@goto END

:END
