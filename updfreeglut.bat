@setlocal
@echo SOURCE: http://freeglut.sourceforge.net/
@set TMPDIR=freeglut-3.0.0
@set TMPZIP=zips\%TMPDIR%.zip
@if NOT EXIST %TMPZIP% goto ERR1
:DOUNZIP
@if EXIST %TMPDIR%\nul goto ERR2

call unzip8 -d %TMPZIP%

@goto END

:ERR1
@if EXIST %DOWNLOADS%\%TMPDIR%.zip (
copy %DOWNLOADS%\%TMPDIR%.zip %TMPZIP%
@if EXIST %TMPZIP% goto DOUNZIP
)
@echo Can NOT locate %TMPZIP%
@goto END

:ERR2
@echo Directory exist %TMPDIR%
@call dirmin %TMPZIP%
@echo Is source from above zip... download later for update...
@goto END


:END
