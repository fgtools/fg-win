@setlocal
@set TEMPD=fgx-fgx
@echo Update %TEMPD%...
@set TMPREPO=git@git.fgx.ch:fgx-devs/fgx-flightgear-launcher.git
@if "%~1x" == "x" goto GOTCMD
@set TEMPD=%1
:GOTCMD

@if NOT EXIST %TEMPD%\. goto CHECKOUT
@cd %TEMPD%
@call git status
@cd ..
@echo.
@echo Found folder [%TEMPD%]!
@echo This looks like an UPDATE only
@echo Will action 
@echo cd %TEMPD%; git pull
@echo.
@echo *** CONTINUE? *** Only Ctrl+c to abort. All other keys continue...
@echo.
@pause

@cd %TEMPD%
call git pull
@cd ..

@goto END

:CHECKOUT
@echo.
@echo Can not find folder [%TEMPD%]!
@echo This looks like a fresh clone...
@echo Will action 
@echo git clone %TMPREPO% %TEMPD%
@echo.
@echo *** CONTINUE? *** Only Ctrl+c to abort. All other keys continue...
@echo.
@pause

call git clone %TMPREPO% %TEMPD%
@if NOT EXIST %TEMPD%\. goto FAILED
@echo All done with [%TEMPD%]
@goto END

:FAILED
@echo ERROR: Appears git clone FAILED...
@goto END

:END
