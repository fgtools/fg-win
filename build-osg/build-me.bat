@setlocal
@REM 20140311 - adapted to flightgear X: drive build
@set DOPAUSE=pause
@if "%~1x" == "NOPAUSEx" (
@set DOPAUSE=echo NO pause requested
@shift
)

@set DOINSTALL=1

@set TMPDIR=X:
@set TMPRT=%TMPDIR%
@set TMPVER=1
@set TMPPRJ=osg
@set TMPSRC=%TMPRT%\OpenSceneGraph-3.2.0
@set TMPBGN=%TIME%
@set TMPMSVC=msvc100
@set TMP3RD=%TMPRT%\3rdParty
@set TMPINS=%TMPRT%\install\%TMPMSVC%\OpenSceneGraph
@set TMPCM=%TMPSRC%\CMakeLists.txt

@call setupqt32
@call chkmsvc %TMPPRJ% 


@set TMPLOG=bldlog-1.txt
@set TMPOPTS=-DCMAKE_INSTALL_PREFIX=%TMPINS%
@set TMPOPTS=%TMPOPTS% -DCMAKE_PREFIX_PATH:PATH=%TMP3RD%
:RPT
@if "%~1x" == "x" goto GOTCMD
@set TMPOPTS=%TMPOPTS% %1
@shift
@goto RPT
:GOTCMD

@echo Build %DATE% %TIME% > %TMPLOG%
@echo Build source %TMPSRC%... all output to build log %TMPLOG%
@echo Build source %TMPSRC%... all output to build log %TMPLOG% >> %TMPLOG%
@set LIB=%TMP3RD%\lib
@set LIBPATH=%TMP3RD%\lib
@echo Set ENV LIB and LIBPATH to %LIB% >> %TMPLOG%

@if EXIST build-cmake.bat (
@call build-cmake >> %TMPLOG%
@if ERRORLEVEL 1 goto BCMERR
)

@if NOT EXIST %TMPCM% goto NOCM

cmake %TMPSRC% %TMPOPTS% >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR1

@echo.
@echo Check the %TMPLOG% that all looks good...
@echo Continue with build? Only Ctrl+c aborts...
@echo.
@%DOPAUSE%
@echo.
cmake --build . --config Debug >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR2
:DONEDBG

cmake --build . --config Release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR3
:DONEREL

@REM fa4 "***" %TMPLOG%
@echo.
@call elapsed %TMPBGN%
@echo Appears a successful build... see %TMPLOG%

@if "%DOINSTALL%x" == "0x" (
@echo Skipping install for now...
@goto END
)

@echo Continue with install? Only Ctrl+c aborts...
@echo.
@%DOPAUSE%
@echo.

cmake --build . --config Debug  --target INSTALL >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR4

cmake --build . --config Release  --target INSTALL >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR5
@echo.
@fa4 " -- " %TMPLOG%
@echo.
@call elapsed %TMPBGN%
@echo All done... see %TMPLOG%
@echo.
@goto END

:BCMERR
@echo Error: Failed to update cmake component
@goto ISERR

:NOCM
@echo Error: Can NOT locate %TMPCM%
@goto ISERR

:ERR1
@echo cmake configuration or generations ERROR
@goto ISERR

:ERR2
@fa4 "LINK : fatal error LNK1104:" %TMPLOG% >nul
@if ERRORLEVEL 1 goto ERR22
:ERR24
@echo ERROR: Cmake build Debug FAILED!
@goto ISERR
:ERR22
@echo Try again due to this STUPID STUPID STUPID error
@echo Try again due to this STUPID STUPID STUPID error >>%TMPLOG%
cmake --build . --config Debug >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR24
@goto DONEDBG

:ERR3
@fa4 "mt.exe : general error c101008d:" %TMPLOG% >nul
@if ERRORLEVEL 1 goto ERR33
:ERR34
@echo ERROR: Cmake build Release FAILED!
@goto ISERR
:ERR33
@echo Try again due to this STUPID STUPID STUPID error
@echo Try again due to this STUPID STUPID STUPID error >>%TMPLOG%
cmake --build . --config Release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR34
@goto DONEREL


:ERR4
@echo Error in install of Debug
@goto ISERR

:ERR5
@echo Error in install of Debug
@goto ISERR

:ISERR
@echo See %TMPLOG% for details...
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
