File: README.osg.txt

=============================================================================================================
Seem the local CMakeModules/FindFreeType.cmake is almost an exact copy of the 
install cmake module, and it FAILS to 'find' freetype...

So have adjusted it to work here... and copy it to the OSG source if different


=============================================================================================================
Just out of interest, found this massive command from - http://forum.openscenegraph.org/viewtopic.php?t=11173 -
while researching PDB installation - all ONE line of course...

cmake.exe -DCMAKE_BUILD_TYPE=debug -DCMAKE_INSTALL_PREFIX="W:\3rd_party_install\win32\10.0\debug\OpenSceneGraph-3.1.1" 
-DCMAKE_USE_RELATIVE_PATHS=ON -DCMAKE_VERBOSE_MAKEFILE=OFF -DOSG_USE_AGGRESSIVE_WARNINGS=OFF 
-DTIFF_INCLUDE_DIR="W:\src_3rd_party\libtiff;W:\src_3rd_party\libtiff\win32" 
-DTIFF_LIBRARY="W:\intermediate\win32\10.0\debug\src_3rd_party\libtiff\tiff.lib" 
-DZLIB_INCLUDE_DIR="W:\src_3rd_party\libzlib" -DZLIB_LIBRARY="W:\intermediate\win32\10.0\debug\src_3rd_party\libzlib\zlib.lib" 
-DJPEG_INCLUDE_DIR="W:\src_3rd_party\libjpeg" -DJPEG_LIBRARY="W:\intermediate\win32\10.0\debug\src_3rd_party\libjpeg\jpeg.lib" 
-DPNG_PNG_INCLUDE_DIR="W:\src_3rd_party\libpng" -DPNG_LIBRARY="W:\intermediate\win32\10.0\debug\src_3rd_party\libpng\png.lib" 
-DFREETYPE_INCLUDE_DIRS="W:\src_3rd_party\libfreetype;W:\src_3rd_party\libfreetype\include" 
-DFREETYPE_LIBRARY="W:\intermediate\win32\10.0\debug\src_3rd_party\libfreetype\freetype.lib" 
-DGIFLIB_LIBRARY="" -DQT_INCLUDE_DIR="W:/3rd_party/qt/qt-4.8.2/include" -DQT_QTCORE_INCLUDE_DIR="W:/3rd_party/qt/qt-4.8.2/include/QtCore" 
-DQT_LIBRARY_DIR="W:/3rd_party/qt/qt-4.8.2/lib/10.0/win32" -DQT_RCC_EXECUTABLE="W:/3rd_party/qt/qt-4.8.2/bin/10.0/win32/rcc.exe" 
-DQT_MOC_EXECUTABLE="W:/3rd_party/qt/qt-4.8.2/bin/10.0/win32/moc.exe" -DQT_QMAKE_EXECUTABLE="W:/3rd_party/qt/qt-4.8.2/bin/10.0/win32/qmake.exe" 
-DQT_UIC_EXECUTABLE="W:/3rd_party/qt/qt-4.8.2/bin/10.0/win32/uic.exe" -DQT_QTMAIN_LIBRARY="W:/3rd_party/qt/qt-4.8.2/lib/10.0/win32/qtmain.lib" 
-DQT_QTCORE_LIBRARY_RELEASE="W:/3rd_party/qt/qt-4.8.2/lib/10.0/win32/QtCore4.lib" 
-DQT_QTCORE_LIBRARY_DEBUG="W:/3rd_party/qt/qt-4.8.2/lib/10.0/win32/QtCore4.lib" -DQT4_FOUND=TRUE 
-DCMAKE_MAKE_PROGRAM="W:\3rd_party\jom\win32\jom.exe" -G "NMake Makefiles" .

=============================================================================================================

# eof
