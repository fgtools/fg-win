# Try to find Dirent
# Once done this will define
#  DIRENT_FOUND - System has dirent
#  DIRENT_INCLUDE_DIRS - The dirent.h include directories
#  DIRENT_LIBRARIES - The static library needed to use dirent

message(STATUS "*** Looking for dirent...")
find_path(DIRENT_INCLUDE_DIR dirent.h
          HINTS ENV DIRENT_ROOT_DIR
          PATH_SUFFIXES include 
          )

find_library(DIRENT_LIBRARY NAMES dirent libdirent
             HINTS ENV DIRENT_ROOT_DIR
             )

set(DIRENT_LIBRARIES ${DIRENT_LIBRARY} )
set(DIRENT_INCLUDE_DIRS ${DIRENT_INCLUDE_DIR} )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set DIRENT_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(foo  DEFAULT_MSG
                                  DIRENT_LIBRARY DIRENT_INCLUDE_DIR)

mark_as_advanced(DIRENT_INCLUDE_DIR DIRENT_LIBRARY)

# eof
