@setlocal
@set TMPBGN=%TIME%
@set TMPRT=X:
@set TMPVER=1
@set TMPPRJ=glew
@set TMPMV=-1.11.0
@REM set TMPMV=-1.10.0
@REM set TMPMV=-1.9.0
@set TMPSRC=%TMPRT%\%TMPPRJ%%TMPMV%
@set TMPBGN=%TIME%
@set TMPINS=%TMPRT%\3rdParty
@set TMPLOG=bldlog-1.txt

@set TMPOPTS=-DCMAKE_INSTALL_PREFIX=%TMPINS%
:RPT
@if "%~1x" == "x" goto GOTCMD
@set TMPOPTS=%TMPOPTS% %1
@shift
@goto RPT
:GOTCMD

@echo Build %DATE% %TIME% > %TMPLOG%
@echo Build source %TMPSRC%... all output to build log %TMPLOG%
@echo Build source %TMPSRC%... all output to build log %TMPLOG% >> %TMPLOG%

@if EXIST build-cmake.bat (
@call build-cmake >> %TMPLOG%
)

@if NOT EXIST %TMPSRC%\CMakeLists.txt goto NOCM

cmake %TMPSRC% %TMPOPTS% >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR1

cmake --build . --config Debug >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR2

cmake --build . --config Release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR3

@REM fa4 "TODO" -c %TMPLOG%
@echo See %TMPLOG%... Appears OK...

@REM goto END

@REM echo Building installation zips... moment...
@REM call build-zips Debug
@REM call build-zips Release
@REM echo Done installation zips...

@call elapsed %TMPBGN%

@echo Continue with install to %TMPINS%? Only Ctrl+c aborts...

@pause

cmake --build . --config Debug  --target INSTALL >> %TMPLOG% 2>&1

cmake --build . --config Release  --target INSTALL >> %TMPLOG% 2>&1

@fa4 " -- " %TMPLOG%

@call elapsed %TMPBGN%
@echo All done... see %TMPLOG%
@echo NOTE: Use GLEW_STATIC define to use this library...

@goto END

:NOCM
@echo Error: can NOT locate %TMPSRC%\CMakeLists.txt! *** FIX ME ***
@goto ISERR

:ERR1
@echo cmake configuration or generations ERROR
@goto ISERR

:ERR2
@echo cmake build Debug ERROR
@fa4 "fatal" %TMPLOG%
@goto ISERR

:ERR3
@echo cmake build Release ERROR
@goto ISERR

:ISERR
@endlocal
@exit /b 1

:END1
@echo See %TMPLOG% for details...
:END
@endlocal
@exit /b 0

@REM eof
