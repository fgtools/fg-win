# root CMake file for glew project - CMakeLists.txt
# 20150326 - Update lib name for 64 bits
# Original generated 2012/10/28 13:30:35
# by vcproj05.pl from C:\FG\17\glew\build\vc10\glew.sln
cmake_minimum_required (VERSION 2.8.8)

project (glew)

# The version number.
set( GLEW_MAJOR 1 )
set( GLEW_MINOR 10 )
set( GLEW_MICRO 0 )

set( LIB_TYPE STATIC )  # set default static
set( LIB_NAME glew   )

option ( BUILD_SHARED_LIB "Set ON to build glew shared Library"             OFF )

if (CMAKE_SIZEOF_VOID_P EQUAL 8)
    set(IS_64_BIT 1)
    message(STATUS "*** Compiling in 64-bit environmnet")
    if (NOT NV_SYSTEM_PROCESSOR) 
        set(NV_SYSTEM_PROCESSOR "AMD64")
    endif ()
    if (MSVC)
        add_definitions( -DWIN64=1 )
    endif ()
else ()
    set(IS_64_BIT 0)
    message(STATUS "*** Compiling in 32-bit environmnet")
endif ()

# Add 2 include directories indicated
include_directories( "${PROJECT_BINARY_DIR}" include )

if(CMAKE_COMPILER_IS_GNUCXX)
    set( WARNING_FLAGS -Wall )
endif(CMAKE_COMPILER_IS_GNUCXX)

if (CMAKE_CXX_COMPILER_ID STREQUAL "Clang") 
   set( WARNING_FLAGS "-Wall -Wno-overloaded-virtual" )
endif() 

if(WIN32)
    if(MSVC)
        # turn off various warnings
        set(WARNING_FLAGS "${WARNING_FLAGS} /wd4996")
        # foreach(warning 4244 4251 4267 4275 4290 4786 4305)
        #     set(WARNING_FLAGS "${WARNING_FLAGS} /wd${warning}")
        # endforeach(warning)

        set( MSVC_FLAGS "-DNOMINMAX -D_USE_MATH_DEFINES -D_CRT_SECURE_NO_WARNINGS -D_SCL_SECURE_NO_WARNINGS -D__CRT_NONSTDC_NO_WARNINGS" )
        # if (${MSVC_VERSION} EQUAL 1600)
        #    set( MSVC_LD_FLAGS "/FORCE:MULTIPLE" )
        # endif (${MSVC_VERSION} EQUAL 1600)
    endif()
    set( CMAKE_DEBUG_POSTFIX "d" )
    set( NOMINMAX 1 )
    set( WIN_LIBS Opengl32.lib Glu32.lib )
    if (IS_64_BIT)
        if (BUILD_SHARED_LIB)
           set( LIB_NAME glew64 )
        else ()
           set( LIB_NAME glew64s )
        endif ()
    else ()
        if (BUILD_SHARED_LIB)
           set( LIB_NAME glew32 )
        else ()
           set( LIB_NAME glew32s )
        endif ()
    endif ()
    message(STATUS "*** Set GLEW library name to ${LIB_NAME}")
endif()

set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${WARNING_FLAGS} ${MSVC_FLAGS} -D_REENTRANT" )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${WARNING_FLAGS} ${MSVC_FLAGS} -D_REENTRANT" )
set( CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${MSVC_LD_FLAGS}" )

add_definitions( -DHAVE_CONFIG_H )

set ( EXTRA_LIBS ${EXTRA_LIBS} ${WIN_LIBS} )

set( glew_SRCS src/glew.c )
set( glew_HDRS include/GL/glew.h include/GL/wglew.h )

if(BUILD_SHARED_LIB)
   set(LIB_TYPE SHARED)
   message("*** Building DLL library ${LIB_TYPE}")
else(BUILD_SHARED_LIB)
   message("*** Building static library ${LIB_TYPE}")
   add_definitions( -DGLEW_STATIC )
endif(BUILD_SHARED_LIB)

add_library( ${LIB_NAME} ${LIB_TYPE} ${glew_SRCS} ${glew_SRCS} )

set( glewinfo_SRCS src/glewinfo.c )
add_executable( glewinfo ${glewinfo_SRCS} )
target_link_libraries ( glewinfo ${LIB_NAME} ${EXTRA_LIBS} )
set_target_properties( glewinfo PROPERTIES DEBUG_POSTFIX "d" )

set( visualinfo_SRCS src/visualinfo.c )
add_executable( visualinfo ${visualinfo_SRCS} )
target_link_libraries ( visualinfo ${LIB_NAME} ${EXTRA_LIBS} )
set_target_properties( visualinfo PROPERTIES DEBUG_POSTFIX "d" )

# INSTALL library
install ( TARGETS ${LIB_NAME}
  RUNTIME DESTINATION bin
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib )
# INSTALL headers
install ( FILES ${glew_HDRS} DESTINATION include/GL )

# eof
