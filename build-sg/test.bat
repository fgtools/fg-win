@setlocal
@set TMPSRC=..\simgear
@set TMP3RD=..\3RdParty
@set TMPINST=..\install\msvc100\simgear

@set TMPOPTS=-DCMAKE_INSTALL_PREFIX=%TMPINST%
@set TMPOPTS=%TMPOPTS% -DCMAKE_PREFIX_PATH:PATH=%TMP3RD%


cmake %TMPSRC% %TMPOPTS%

