@setlocal
@REM modified for flightgear X: drive build
@set DOINSTALL=1
@set DOTMPINST=0

@set TMPDIR=X:
@set TMPRT=%TMPDIR%
@set TMPVER=1
@set TMPPRJ=sg
@set TMPPROJ=simgear
@set TMPSRC=%TMPRT%\%TMPPROJ%
@set TMPBGN=%TIME%
@set TMPMSVC=msvc100
@set TMP3RD=%TMPRT%\3rdParty
@set TMPINS=%TMPRT%\install\%TMPMSVC%\%TMPPROJ%
@set TMPCM=%TMPSRC%\CMakeLists.txt
@set DOPAUSE=pause
@set TMPBOOST=X:\boost_1_53_0
@set TMPLOG=bldlog-%TMPVER%.txt


@REM call chkmsvc %TMPPRJ% 

@REM if EXIST build-cmake.bat (
@REM call build-cmake
@REM )

@if NOT EXIST %TMPCM% goto NOCM
@if NOT EXIST %TMPBOOST%\nul goto NOBOOST


@set TMPOPTS=-DCMAKE_INSTALL_PREFIX=%TMPINS%
@set TMPOPTS=%TMPOPTS% -DCMAKE_PREFIX_PATH:PATH=%TMP3RD%
:RPT
@if "%~1x" == "x" goto GOTCMD
@if "%~1x" == "NOPAUSEx" (
@set DOPAUSE=echo No pause requested...
) else (
@set TMPOPTS=%TMPOPTS% %1
)
@shift
@goto RPT
:GOTCMD

@echo Build %DATE% %TIME% > %TMPLOG%
@echo Build source %TMPSRC%... all output to build log %TMPLOG%
@echo Build source %TMPSRC%... all output to build log %TMPLOG% >> %TMPLOG%

@set BOOST_INCLUDEDIR=%TMPBOOST%
@echo Added ENV BOOST_INCLUDEDIR=%BOOST_INCLUDEDIR% >> %TMPLOG%

cmake %TMPSRC% %TMPOPTS% >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR1

cmake --build . --config Debug >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR2
:DONEDBG

cmake --build . --config Release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR3
:DONEREL

@REM fa4 "***" %TMPLOG%
@call elapsed %TMPBGN%
@echo Appears a successful build... see %TMPLOG%

@if "%DOTMPINST%x" == "0x" (
@echo Skipping build of artifacts for now...
@goto DONETI
)

@if EXIST build-zip.bat (
@echo Building installation zips... moment...
@REM If paths given as ${CMAKE_INSTALL_PREFIX}
@call build-zip Debug
@call build-zip Release
@REM If full paths used in install
@REM call build-zipsf2 Debug
@REM call build-zipsf2 Release
@echo Done installation zips...
)

:DONETI

@if "%DOINSTALL%x" == "0x" (
@echo Skipping install for now...
@goto END
)

@echo Continue with install? Only Ctrl+c aborts...
@%DOPAUSE%

cmake --build . --config Debug  --target INSTALL >> %TMPLOG% 2>&1
@if EXIST install_manifest.txt (
@copy install_manifest.txt install_manifest_dbg.txt >nul
@echo. >> %TMPINS%\installed.txt
@echo %TMPPRJ% Debug install %DATE% %TIME% >> %TMPINS%\installed.txt
@type install_manifest.txt >> %TMPINS%\installed.txt
)

cmake --build . --config Release  --target INSTALL >> %TMPLOG% 2>&1
@if EXIST install_manifest.txt (
@copy install_manifest.txt install_manifest_rel.txt >nul
@echo. >> %TMPINS%\installed.txt
@echo %TMPPRJ% Release install %DATE% %TIME% >> %TMPINS%\installed.txt
@type install_manifest.txt >> %TMPINS%\installed.txt
)

@echo.
@fa4 " -- " %TMPLOG%
@echo.

@call elapsed %TMPBGN%
@echo All done... see %TMPLOG%

@goto END

:NOCM
@echo Error: Can NOT locate %TMPCM%
@goto ISERR

:ERR1
@echo cmake configuration or generations ERROR
@goto ISERR

:ERR2
@fa4 "LINK : fatal error LNK1104:" %TMPLOG% >nul
@if ERRORLEVEL 1 goto ERR22
:ERR24
@echo ERROR: Cmake build Debug FAILED!
@goto ISERR
:ERR22
@echo Try again due to this STUPID STUPID STUPID error
@echo Try again due to this STUPID STUPID STUPID error >>%TMPLOG%
cmake --build . --config Debug >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR24
@goto DONEDBG

:ERR3
@fa4 "mt.exe : general error c101008d:" %TMPLOG% >nul
@if ERRORLEVEL 1 goto ERR33
:ERR34
@echo ERROR: Cmake build Release FAILED!
@goto ISERR
:ERR33
@echo Try again due to this STUPID STUPID STUPID error
@echo Try again due to this STUPID STUPID STUPID error >>%TMPLOG%
cmake --build . --config Release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR34
@goto DONEREL

:NOBOOST
@echo Error: Unable to locate boost %TMPBOOST%! *** FIX ME ***
@goto ISERR

:ISERR
@echo See %TMPLOG% for details...
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
