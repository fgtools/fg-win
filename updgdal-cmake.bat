@setlocal
@set TMPREPO=git@gitorious.org:cmake-gdal/gdal-svn.git
@set TMPDIR=gdal-cmake
@if EXIST %TMPDIR%\nul goto UPDATE

@echo Is a NEW clone into %TMPDIR%
@echo WIll do: 'call git clone %TMPREPO% %TMPDIR%'
@echo *** CONTINUE? *** Only Ctlr+C aborts...
@pause

call git clone %TMPREPO% %TMPDIR%


@goto END

:UPDATE
cd %TMPDIR%
call git status
@echo Proceed with update? Only Ctrl+C aborts...
@pause

call git pull

@goto END

:END
