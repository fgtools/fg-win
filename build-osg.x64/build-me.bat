@setlocal
@set TMPDRV=X:
@set TMPPRJ=OpenSceneGraph
@echo Build %TMPPRJ% project, in 64-bits
@set TMPLOG=bldlog-1.txt
@set BLDDIR=%CD%
@set TMPVERS=-3.2.0
@set TMPSRC=%TMPDRV%\%TMPPRJ%%TMPVERS%
@set SET_BAT=%ProgramFiles(x86)%\Microsoft Visual Studio 10.0\VC\vcvarsall.bat
@if NOT EXIST "%SET_BAT%" goto NOBAT
@set TMPOPTS=-G "Visual Studio 10 Win64"
@set DOPAUSE=pause
@set TMPPFP=%TMPDRV%\3rdParty.x64
@set TMPFREET=%TMPDRV%\3rdParty.x64

:RPT
@if "%~1x" == "x" goto DNCMD
@if "%~1x" == "NOPAUSEx" (
@set DOPAUSE=echo No pause requested
) else (
@echo Only one command NOPAUSE allowed
@goto ISERR
)
@shift
@goto RPT
:DNCMD

@call chkmsvc %TMPPRJ%

@echo Begin %DATE% %TIME%, output to %TMPLOG%
@echo Begin %DATE% %TIME% > %TMPLOG%

@if EXIST build-cmake.bat (
@call build-cmake >> %TMPLOG%
@if ERRORLEVEL 1 goto BCMERR
)

@REM ############################################
@REM NOTE: SPECIAL INSTALL LOCATION
@REM Adjust to suit your environment
@REM ##########################################
@if /I "%PROCESSOR_ARCHITECTURE%" EQU "AMD64" (
@set TMPINST=%TMPDRV%\install\msvc100-64\%TMPPRJ%
) ELSE (
 @if /I "%PROCESSOR_ARCHITECTURE%" EQU "x86_64" (
@set TMPINST=%TMPDRV%\install\msvc100-64\%TMPPRJ%
 ) ELSE (
@echo ERROR: Appears 64-bit build is NOT available
@goto ISERR
 )
)

@echo Doing: 'call "%SET_BAT%" %PROCESSOR_ARCHITECTURE%'
@echo Doing: 'call "%SET_BAT%" %PROCESSOR_ARCHITECTURE%' >> %TMPLOG%
@call "%SET_BAT%" %PROCESSOR_ARCHITECTURE% >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR0

@call setupqt64
@cd %BLDDIR%

:DNARCH
@REM set additional ENVIRONMENT
@set FREETYPE_DIR=%TMPFREET%
@echo Set ENV FREETYPE_DIR=%FREETYPE_DIR%
@echo Set ENV FREETYPE_DIR=%FREETYPE_DIR% >> %TMPLOG%

@set TMPOPTS=%TMPOPTS% -DCMAKE_INSTALL_PREFIX:PATH=%TMPINST%
@set TMPOPTS=%TMPOPTS% -DCMAKE_PREFIX_PATH:PATH=%TMPPFP%
@set CMAKE_PREFIX_PATH=%TMPPFP%
@set OSG_3RDPARTY_DIR=%TMPPFP%
@set TMPOPTS=%TMPOPTS% -DZLIB_ROOT:PATH=%TMPPFP%
@set TMPOPTS=%TMPOPTS% -DOSG_USE_QT:BOOL=NO
@set TMPOPTS=%TMPOPTS% -DUSE_3RDPARTY_BIN:BOOL=NO

@if NOT EXIST %TMPSRC%\nul goto NOSRC
@if NOT EXIST %TMPSRC%\CMakeLists.txt goto NOCM

@REM UGH: Sometimes seems need to ADJUST the *.vcxproj files MANUALLY, so need to skip re-writing them!!!
@if EXIST no-cmake.txt goto DOBUILD

@echo Doing: 'cmake %TMPSRC% %TMPOPTS%'
@echo Doing: 'cmake %TMPSRC% %TMPOPTS%' >> %TMPLOG%
@cmake %TMPSRC% %TMPOPTS% >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR1
@echo.
@echo *** CONTINUE with build? Check log %TMPLOG%. *** Only Ctrl+C aborts ***
@echo.
@%DOPAUSE%

:DOBUILD

@echo Doing: 'cmake --build . --config debug'
@echo Doing: 'cmake --build . --config debug' >> %TMPLOG%
@cmake --build . --config debug >> %TMPLOG%
@if ERRORLEVEL 1 goto ERR2

@echo Doing: 'cmake --build . --config release'
@echo Doing: 'cmake --build . --config release' >> %TMPLOG%
@cmake --build . --config release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR3
:DNREL

@echo Appears a successful build
@echo.
@echo Note install location %TMPINST%
@echo *** CONTINUE with install? *** Only Ctrl+C aborts
@%DOPAUSE%

@echo Doing: 'cmake --build . --config debug --target INSTALL'
@echo Doing: 'cmake --build . --config debug --target INSTALL' >> %TMPLOG%
@cmake --build . --config debug --target INSTALL >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR4

@echo Doing: 'cmake --build . --config release --target INSTALL'
@echo Doing: 'cmake --build . --config release --target INSTALL' >> %TMPLOG%
@cmake --build . --config release --target INSTALL >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR4

@fa4 " -- " %TMPLOG%

@echo Done build and install of %TMPPRJ%...

@goto END

:NOSRC
@echo Error: Can NOT locate SOURCE %TMPSRC%! *** FIX ME ***
@goto ISERR

:NOCM
@echo Error: Can NOT locate FILE %TMPSRC%\CMakeLists.txt! *** FIX ME ***
@goto ISERR


:NOBAT
@echo Can NOT locate MSVC setup batch "%SET_BAT%"! *** FIX ME ***
@goto ISERR

:ERR0
@echo MSVC 10 setup error
@goto ISERR

:ERR1
@echo cmake config, generation error
@echo cmake config, generation error >> %TMPLOG%
@goto ISERR

:ERR2
@echo debug build error
@echo debug build error >> %TMPLOG%
@goto ISERR

:ERR3
@fa4 "mt.exe : general error c101008d:" %TMPLOG% >nul
@if ERRORLEVEL 1 goto ERR32
:ERR33
@echo release build error
@echo release build error >> %TMPLOG%
@goto ISERR
:ERR32
@echo Stupid error... trying again...
@echo Doing: 'cmake --build . --config release'
@echo Doing: 'cmake --build . --config release' >> %TMPLOG%
@cmake --build . --config release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR33
@goto DNREL

:ERR4
@echo debug install error
@goto ISERR

:ERR5
@echo release install error
@goto ISERR

:BCMERR
@echo Error in build-cmake.bat source updating...
@goto ISERR

:ISERR
@echo Error exit! See %TMPLOG%
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
eof
