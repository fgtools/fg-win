README.osg.x64.txt - 20141130

Just some notes in the x64 build...

SOURCE: X:\OpenSceneGraph-3.2.0
=================================================================================
UGH: Seems cmake made a problem when adding optimized/debug libraries, and ended 
up adding ...;optimized.lib;X:/path/to/true/lib;debug.lib;X:/path/to/debug/lib;...
Had to MANUALLY delete these from the osg png vcxproj file.

Then to avoid re-creating these bad vcxproj files, in the build-me.bat added
@if EXIST no-cmake.txt goto DNCMAKE
skipping the regeneration

Maybe this has been FIXED in a later version of cmake. Presently using
cmake version 2.8.12.2
DWN: 25/03/2015  17:31        12,045,035 cmake-3.2.1-win32-x86.exe
But unforunatley is the SAME in this regard.

Now the build file pauses after the cmake generation is done to allow a 
fix of the particular osgdb_png.vcxproj file...
And also added a skip of the generation if a file no-cmake.txt is found.

Can ONLY hope this wrinkle goes AWAY over time. Maybe need to look at 
the osg CMakeLists.txt where the PNG library is found... Maybe a problem 
in there... It seems to ONLY affect the Release MinSizeRel RelWithDebInfo 
builds. The Debug build correctly only gets the X:\3rdParty.x64\lib\libpng15d.lib
added...

=================================================================================
*) Seems fails to find freetype, but X:\3rdParty.x64\lib\freetype.lib exists, AND
X:\3rdParty.x64\include\freetype2\ft2build.h also EXIST
BUT CMakeCache.txt shows

FREETYPE_INCLUDE_DIR_freetype2:PATH=FREETYPE_INCLUDE_DIR_freetype2-NOTFOUND
FREETYPE_INCLUDE_DIR_ft2build:PATH=FREETYPE_INCLUDE_DIR_ft2build-NOTFOUND
FREETYPE_LIBRARY:FILEPATH=FREETYPE_LIBRARY-NOTFOUND

WHY??? Ok, FIXED 'find' module
-- FREETYPE INCLUDE: ft2build X:/3rdParty.x64/include/freetype2 freetype2 X:/3rdParty.x64/include/freetype2/config
-- Found FREETYPE debug;X:/3rdParty.x64/lib/freetyped.lib;optimized;X:/3rdParty.x64/lib/freetype.lib X:/3rdParty.x64/include/freetype2;X:/3rdParty.x64/include/freetype2/config

Note, these are NOT FOUND, hopefully not important!!!
-- FIXED LibXml2 (LIBXML2_LIBRARIES LIBXML2_INCLUDE_DIR) 
-- Could NOT find CURL (missing:  CURL_LIBRARY CURL_INCLUDE_DIR) 
-- Could NOT find SDL (missing:  SDL_LIBRARY SDL_INCLUDE_DIR) 
-- Could NOT find PkgConfig (missing:  PKG_CONFIG_EXECUTABLE) 
-- Could NOT find TIFF (missing:  TIFF_LIBRARY TIFF_INCLUDE_DIR) 

Other (hopefully OPTIONAL) libraries not found!

-- checking for module 'gta'
--   package 'gta' not found
-- checking for module 'cairo'
--   package 'cairo' not found
-- checking for module 'poppler-glib'
--   package 'poppler-glib' not found
-- checking for module 'librsvg-2.0'
--   package 'librsvg-2.0' not found
-- checking for module 'gtk+-2.0'
--   package 'gtk+-2.0' not found
-- checking for module 'gtkglext-win32-1.0'
--   package 'gtkglext-win32-1.0' not found

=================================================================================


