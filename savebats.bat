@setlocal
@set TMPVER=01
@set TMPZIP=updbats-%TMPVER%.zip
@set TMPDIR=zips
@if NOT EXIST %TMPDIR%\nul goto NODST
@set TMPDST=zips\%TMPZIP%
@set TMPOPT=-a
@if NOT EXIST %TMPDST% goto GOTOPT
@set TMPOPT=-u
:GOTOPT
@set TMPOPT=%TMPOPT% -o

@echo Copy the bats to a zip... and copy to 'tmp'

@call zip8 %TMPOPT% %TMPDST% *.bat

@if NOT EXIST %TMPDST% goto ZFAILED

@cd zips
@if NOT EXIST %TMPZIP% goto NOZIP
@call copy2tmp %TMPZIP%

@goto END

:NOZIP
@echo Error: Can NOT locate the zip %TMPZIP% in %CD%!
@goto END

:ZFAILED
@echo Perhaps there is no zip8.bat in the PATH to do the zipping...
@echo Or some other serious problem...
@goto END

:NODST
@echo Can NOT locate directory %TMPDIR%! This must exist.
@echo Create it an run me again...
@goto END

:END
