#!/usr/bin/perl -w
# NAME: dirmin.pl
# AIM: Read a dir > file file, and trim to minimum...
# 20140311 - Adapted to flightgear X: drive build
# 13/12/2011 - Added -o <file> to output to a file.
# 15/10/2010 geoff mclane http://geoffair.net/mperl
use strict;
use warnings;
use File::Basename;  # split path ($name,$dir,$ext) = fileparse($file [, qr/\.[^.]*/] )
use Cwd;
my $perl_dir = 'X:\scripts';
unshift(@INC, $perl_dir);
require 'lib_utils.pl' or die "Unable to load 'lib_utils.pl'...\n";
# log file stuff
our ($LF);
my $pgmname = $0;
if ($pgmname =~ /(\\|\/)/) {
    my @tmpsp = split(/(\\|\/)/,$pgmname);
    $pgmname = $tmpsp[-1];
}
my $outfile = $perl_dir."\\temp\\temp.$pgmname.txt";
open_log($outfile);

# user variables
my $load_log = 0;
my $in_file = '';
my $out_file = '';

my $debug_on = 0;
my $def_file = 'def_file';

### program variables
my @warnings = ();
my $cwd = cwd();
my $os = $^O;

sub show_warnings($) {
    my ($val) = @_;
    if (@warnings) {
        prt( "\nGot ".scalar @warnings." WARNINGS...\n" );
        foreach my $itm (@warnings) {
           prt("$itm\n");
        }
        prt("\n");
    } else {
        #prt( "\nNo warnings issued.\n\n" );
    }
}

sub pgm_exit($$) {
    my ($val,$msg) = @_;
    if (length($msg)) {
        $msg .= "\n" if (!($msg =~ /\n$/));
        prt($msg);
    }
    show_warnings($val);
    close_log($outfile,$load_log);
    exit($val);
}


sub prtw($) {
   my ($tx) = shift;
   $tx =~ s/\n$//;
   prt("$tx\n");
   push(@warnings,$tx);
}

sub process_in_file($) {
    my ($inf) = @_;
    if (! open INF, "<$inf") {
        pgm_exit(1,"ERROR: Unable to open file [$inf]\n"); 
    }
    my @lines = <INF>;
    close INF;
    my $lncnt = scalar @lines;
    #prt("Processing $lncnt lines, from [$inf]...\n");
    my ($line,$tline,$lnn,$len,$dirof,$dir,$ndir,$msg);
    $dirof = 0;
    foreach $line (@lines) {
        chomp $line;
        if ($line =~ /^\s+Directory\s+of\s+(.+)$/) {
            $dir = $1;
            #prt("$dir\n");
            $dirof++;
        }
    }
    #exit(0);
    $ndir = '';
    $msg = '';
    foreach $line (@lines) {
        $lnn++;
        chomp $line;
        $tline = trim_all($line);
        $len = length($tline);
        next if ($len == 0);
        #next if (($line =~ /\s+<DIR>\s+/) && (($line =~ /\s+\.\s+/)||($line =~ /\s+\.\.\s+/) ));
        next if ($line =~ /\s+<DIR>\s+\.(\.*)\s*/); # remove dot and double dot
        if ($line =~ /^\s+/) {
            if ($line =~ /^\s+Directory\s+of\s+(.+)$/) {
                $dir = $1;
                next if ($dirof < 2);
                $ndir = $line;
                next;
            } else {
                next;
            }
        }
        if (length($ndir)) {
            prt("$ndir\n");
            $msg .= "$ndir\n";
            $ndir = '';
        }
        prt("$line\n");
        $msg .= "$line\n";
    }
    if (length($out_file) && length($msg) ) {
        write2file($msg,$out_file);
    }
}

#########################################
### MAIN ###
parse_args(@ARGV);
process_in_file($in_file);
pgm_exit(0,"");
########################################
sub give_help {
    prt("$pgmname: version 0.0.1 2010-09-11\n");
    prt("Usage: $pgmname [options] in-file\n");
    prt("Options:\n");
    prt(" --help (-h or -?) = This help, and exit 0.\n");
    prt(" --out <file> (-o) = Send output to file.\n");
}
sub need_arg {
    my ($arg,@av) = @_;
    pgm_exit(1,"ERROR: [$arg] must have following argument!\n") if (!@av);
}

sub parse_args {
    my (@av) = @_;
    my ($arg,$sarg);
    while (@av) {
        $arg = $av[0];
        if ($arg =~ /^-/) {
            $sarg = substr($arg,1);
            $sarg = substr($sarg,1) while ($sarg =~ /^-/);
            if (($sarg =~ /^h/i)||($sarg eq '?')) {
                give_help();
                pgm_exit(0,"Help exit(0)");
            } elsif ($sarg =~ /^o/) {
                need_arg(@av);
                shift @av;
                $sarg = $av[0];
                $out_file = $sarg;
            } else {
                pgm_exit(1,"ERROR: Invalid argument [$arg]! Try -?\n");
            }
        } else {
            $in_file = $arg;
            # prt("Set input to [$in_file]\n");
        }
        shift @av;
    }

    if ((length($in_file) ==  0) && $debug_on) {
        $in_file = $def_file;
    }
    if (length($in_file) ==  0) {
        pgm_exit(1,"ERROR: No input files found in command!\n");
    }
    if (! -f $in_file) {
        pgm_exit(1,"ERROR: Unable to find in file [$in_file]! Check name, location...\n");
    }
}

# eof - dirmin.pl
