@setlocal
@set TEMP1=%~1
@if "%TEMP1%x" == "x" goto DEFAULT
:RPT
@shift
@if "%~1x" == "x" goto DONE
@set TEMP1=%TEMP1% %1
@goto RPT

:DONE
@REM echo Set this command window with title %TEMP1% ...
@if NOT "%VCINSTALLDIR%x" == "x" (
@set TEMP1=%TEMP1% MSVC
)
@if /i "%Platform%x" == "x64x" (
@set TEMP1=%TEMP1% x64
)
@title %TEMP1%
@goto END

:DEFAULT
@echo Set default title %CD% ...
@call set-title

:END
@endlocal

