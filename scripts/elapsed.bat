@setlocal
@REM 20140311 - adjusted for flightgear X: drive build
@set TMPFIL=X:\scripts\elapsed.pl
@if NOT EXIST %TMPFIL% goto ERR1
@if "%1." == "." goto USE
@if "%2." == "." goto NOW
@perl %TMPFIL% %1 %2
@goto END

:NOW
@perl %TMPFIL% %1 %TIME%
@goto END

:USE
@echo Enter one, or two times, begin [end], in hh:mm:ss form ...
@goto end

:END
