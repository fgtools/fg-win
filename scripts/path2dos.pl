#!/usr/bin/perl
use strict;
use warnings;
# 20140312 - adapted for flightgear x: drive build

my $perl_dir = 'X:\scripts';
my $verbosity = 0;
my $out_file = $perl_dir.'\temp\temppath.txt';

sub VERB1() { return $verbosity >= 1; }
sub VERB2() { return $verbosity >= 2; }
sub VERB5() { return $verbosity >= 5; }
sub VERB9() { return $verbosity >= 9; }
sub prt($) { print shift; }

sub trim_leading($) {
    my ($ln) = shift;
	$ln = substr($ln,1) while ($ln =~ /^\s/); # remove all LEADING space
    return $ln;
}

sub trim_tailing($) {
    my ($ln) = shift;
	$ln = substr($ln,0, length($ln) - 1) while ($ln =~ /\s$/g); # remove all TRAILING space
    return $ln;
}

sub trim_ends($) {
    my ($ln) = shift;
    $ln = trim_tailing($ln); # remove all TRAINING space
	$ln = trim_leading($ln); # remove all LEADING space
    return $ln;
}

sub trim_all {
	my ($ln) = shift;
	$ln =~ s/\n/ /gm;	# replace CR (\n)
	$ln =~ s/\r/ /gm;	# replace LF (\r)
	$ln =~ s/\t/ /g;	# TAB(s) to a SPACE
    $ln = trim_ends($ln);
	$ln =~ s/\s{2}/ /g while ($ln =~ /\s{2}/);	# all double space to SINGLE
	return $ln;
}

sub mydie($) {
    my $txt = shift;
    prt($txt);
    exit(1);
}

sub write2file {
	my ($txt,$fil) = @_;
	open WOF, ">$fil" or mydie("ERROR: Unable to open $fil! $!\n");
	print WOF $txt;
	close WOF;
}


sub do_dir($) {
    my $tmp = shift;
    my (@arr,$path,$file,@arr2,$ln,$lnn,@arr3,$len,$dir,$name);
    my ($i,$max,$i2,$seek,$fl,$test,$j,$dos,$fnd);
    my (@lines);
    my $newpath = '';
    $tmp =~ s/\//\\/g;
    prt("Converting [$tmp]\n");
    @arr = split(/\\/,$tmp);
    $path = '';
    $max = scalar @arr;
    for ($i = 0; $i < $max; $i++) {
        $i2 = $i + 1;
        $file = $arr[$i];
        $path .= "\\" if (length($path));
        $path .= $file;
        $test = $path;
        if ($i2 < $max) {
            $seek = $arr[$i2];
            $fl = substr($seek,0,1);
            $test .= "\\$fl*"
        } else {
            $seek = 'LAST';
            last;
        }
        $newpath = $file if ($file =~ /^\w+:$/);
        if (open (DIR, "dir /X \"$test\"|")) {
            @arr2 = <DIR>;
            close DIR;
            $lnn = 0;
            # 11/03/2014  12:03               310 GITIGN~1     .gitignore
            # 11/03/2014  19:32    <DIR>          BU9B98~1     build-curl
            # 28/01/2014  15:29    <DIR>                       build-fg
            # 09/02/2014  12:03    <DIR>          BU568F~1     build-fgrun
            $len = scalar @arr2;
            prt("dir of [$test], seeking [$seek] in $len lines\n") if (VERB5());
            $fnd = 0;
            @lines = (); # clear lines
            foreach $ln (@arr2) {
                $lnn++;
                chomp $ln;
                $ln = trim_all($ln);
                $len = length($ln);
                next if ($len == 0);
                next if ($ln =~ /^Volume/);
                next if ($ln =~ /^Directory/);
                next if ($ln =~ /^\d+\s+File\(s\)/);
                next if ($ln =~ /^\d+\s+Dir\(s\)/);
                @arr3 = split(/\s+/,$ln);
                $len = scalar @arr3;
                next if ($len < 4);
                $dir = $arr3[2];    # is '<DIR>'
                $dos = $arr3[3];
                $name = $dos;
                if ($len > 4) {
                    $name = $arr3[4];
                    for ($j = 5; $j < $len; $j++) {
                        $name .= ' '.$arr3[$j]
                    }
                }
                next if ($name eq '.');
                next if ($name eq '..');
                if (lc($seek) eq lc($name)) {
                    $newpath .= "\\" if (length($newpath));
                    $newpath .= $dos;
                    prt("$lnn: Found [$dos] newpath [$newpath]\n") if (VERB1());
                    $fnd = 1;
                    last;
                } else {
                    prt("$lnn: [$dos] [$name] [$ln]\n") if (VERB9());
                    push(@lines,$ln);
                }
            }
            if (!$fnd) {
                $len = scalar @lines;
                prt("Did NOT find [$seek] in $len lines...\n");
                prt(join("\n",@lines)."\n");
                prt("*** FIX ME if you can ***\n");
            }
        } else {
            prt( "dir $path FAILED! ... $! ...\n" );
        }
    }
    prt("New PATH [$newpath]\n");
    write2file($newpath,$out_file);
    prt("Written to outfile $out_file\n");
}

if (@ARGV) {
    foreach my $dir (@ARGV) {
        do_dir($dir);
    }
    exit(0);
} else {
    print "No command! Give path to convert...\n";
    exit(1);
}