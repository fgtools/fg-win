@setlocal
@REM Remove the -wh... notice
@REM 2012-03-01 - Adjust for new WIN7 machine
@set TEMPWZ="C:\Program Files (x86)\WinZip\WZUNZIP.EXE"
@if NOT EXIST %TEMPWZ% goto Err1
@set TEMPSC=

:CYCLE
@if "%~1."=="." goto GOTCMD
@set TEMPSC=%TEMPSC% %1
@shift
@goto CYCLE

:GOTCMD

%TEMPWZ% %TEMPSC%

@set EXSTAT=%ERRORLEVEL%
@if NOT "%EXSTAT%." == "0." goto Err2

@goto End

:Err1
@echo Can NOT locate file %TEMPWZ% ...
@echo Check the README.txt about unzipping
@echo Use Ctrl+C to abort
@pause
@goto Err1

:Err2
@echo *** NOTE WELL *** GOT ERRORLEVEL=%EXSTAT% ...
@pause
@goto End

:End
@endlocal
