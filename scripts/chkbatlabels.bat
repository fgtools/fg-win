@setlocal
@REM Modified to FG x: drive build
@set TMPPERL=X:\scripts\chkbatlabel.pl
@if NOT EXIST %TMPPERL% goto ERR1
@set TMPCMD=
:RPT
@if "%~1x" == "x" goto GOTCMD
@set TMPCMD=%TMPCMD% %1
@shift
@goto RPT
:GOTCMD

perl %TMPPERL% %TMPCMD%

@goto END

:ERR1
@echo ERROR: Can NOT locate [%TMPPERL%]! Check name, location and FIX ME!
@endlocal
@exit /b 1


:END
@endlocal
@exit /b 0

@REM eof
