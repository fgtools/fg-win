@setlocal
@if NOT EXIST build-me.bat goto ERR1
@set TMPBM=
:RPT
@if "%~1x" == "x" goto GOTCMD
@set TMPBM=%TMPBM% %1
@shift
@goto RPT
:GOTCMD
@call build-me %TMPBM%
@goto END
:ERR1
@echo.
@echo Error: Can NOT locate build-me.bat
@echo.
:END
