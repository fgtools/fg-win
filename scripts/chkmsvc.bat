@setlocal
@REM Add a test for find-window to see if MSVC is running
@find-window -? >nul
@set TMPERR=%ERRORLEVEL%
@if "%TMPERR%x" == "2x" goto CHKMSVC
@echo Can NOT locate the 'find-window' tool, so unable to check if MSVC is running!
@echo If it is, it is recommended that you close it before continuing...
@echo *** CONTINUE? *** Only Ctrl+c will abort - all other keys continue...
@pause
@goto CONT

:CHKMSVC
@if "%~1x" == "x" goto NOPRJ
@find-window "%1 - Microsoft" -w
@if ERRORLEVEL 1 goto CHECK
@echo A window '%1 - Microsoft' not found... assume no MSVC... continuing... exit 0
@goto  CONT

:CHECK
@echo.
@echo *** WARNING: Appears '%1 - Microsoft' window exists ***
@echo This suggests MSVC is running on ***THIS*** project.
@echo.
@echo If it is, it is RECOMMENDED that you close it before continuing...
@echo *** CONTINUE? *** Only Ctrl+c will abort - all other keys continue...
@echo.
@pause
@goto CONT

:CONT
@exit /b 0


:NOTEST
@echo Have NOT found the app 'find-window' in your system
@echo So NOT able to 'test' for a MSVC window...
@endlocal
@exit /b 1

:NOPRJ
@echo This batch was called with no command...
@echo So no 'window' test can be performed... exit 0
@endlocal
@exit /b 0


@REM eof
