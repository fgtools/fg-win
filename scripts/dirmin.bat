@setlocal
@REM 2014-03-11 - adapted for flightgear x: drive build
@REM 2012-02-29 - more minor updates...
@REM 2011-12-13 - Some minor updates...
@if "%1x" == "x" goto HELP
@set TEMPP=X:\scripts\dirmin.pl
@if NOT EXIST %TEMPP% goto ERR1
@set TEMPOUT=%TEMP%\templist.txt
@if "%~1x" == "-?x" goto HELP
@if "%~1x" == "/?x" goto HELP
@if /i "%~1x" == "-hx" goto HELP
@if /i "%~1x" == "--helpx" goto HELP
@REM Else ASSUME a file name
@set TEMPF=%1
@shift
@set TEMPCMD=
@set TEMPPERL=
:RPT
@if "%1~x" == "-px" goto PERLCMD
@if "%1~x" == "--perlx" goto PERLCMD
@if "%1~x" == "x" goto DNCMD
@if "%1x" == "x" goto DNCMD
@if "%1x" == "-px" goto PERLCMD
@REM echo Adding [%1]
@set TEMPCMD=%TEMPCMD% %1
@shift
@goto RPT

:PERLCMD
@REM Toss this param
@REM echo Begin perl commands...
@shift
:RPT2
@if "%1~x" == "x" goto DNCMD
@if "%1x" == "x" goto DNCMD
@set TEMPPERL=%TEMPPERL% %1
@echo TEMPPERL=%TEMPPERL%
@shift
@goto RPT2

:DNCMD

@set TEMPDIR=no
@if EXIST %TEMPF% goto GOTFILE
@set TEMPDIR=yes
@if EXIST %TEMPF%\. goto GOTFILE
@echo.
@echo WARNING: Can NOT locate %TEMPF%, as directory or file...
@echo.
:GOTFILE

@dir %TEMPF% %TEMPCMD% >%TEMPOUT%

@if EXIST %TEMPP% (
@perl %TEMPP% %TEMPPERL% %TEMPOUT% 
) else (
@type %TEMPOUT%
)

@goto END

:NODIR
@goto END

:ERR1
@echo ERROR: Can NOT locate perl script [%TEMPP%]! Check name, location, and FIX ME!
@goto END


:HELP
@echo.
@echo Give name of directory, or file to list...
@echo --help -h -? - Show this HELP
@echo --perl -p      Switch to commands passed to perl script
@echo All commands before this will be part of 'dir ...' command
@echo which is output to file %TEMPOUT%
@echo.
@goto END

:END
