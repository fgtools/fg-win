@setlocal
@set TEMPWZ=X:\scripts\7z.exe
@if NOT EXIST %TEMPWZ% goto Err1
@set TEMPSC=x

:CYCLE
@if "%~1."=="." goto GOTCMD
@if "%~1x"=="-dx" goto SKIP
@set TEMPSC=%TEMPSC% %1
@shift
@goto CYCLE
:SKIP
@shift
@goto CYCLE
:GOTCMD

%TEMPWZ% %TEMPSC%

@set EXSTAT=%ERRORLEVEL%
@if NOT "%EXSTAT%." == "0." goto Err2

@goto End

:Err1
@echo Can NOT locate file %TEMPWZ% ...
@echo Check the README.txt about unzipping
@echo Use Ctrl+C to abort
@pause
@goto Err1

:Err2
@echo *** NOTE WELL *** GOT ERRORLEVEL=%EXSTAT% ...
@pause
@goto End

:End
@endlocal
