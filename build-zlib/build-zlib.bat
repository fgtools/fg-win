@setlocal
@set DOPAUSE=pause
@if "%~1x" == "NOPAUSEx" (
@set DOPAUSE=echo No pause requested
@shift
)

@set TMPDRV=X:
@if NOT EXIST %TMPDRV%\nul goto NOXD
@set TMPRT=%TMPDRV%
@set TMPVER=1
@set TMPPRJ=zlib
@set TMPSRC=%TMPRT%\zlib-1.2.8
@set TMPBGN=%TIME%
@set TMPINS=%TMPRT%\3rdParty
@call chkmsvc %TMPPRJ% 
@set TMPLOG=bldlog-1.txt
@set TMPOPTS=-DCMAKE_INSTALL_PREFIX=%TMPINS%
@set TMPPDB=X:\build-zlib\zlibstatic.dir\Debug\vc100.pdb

:RPT
@if "%~1x" == "x" goto GOTCMD
@set TMPOPTS=%TMPOPTS% %1
@shift
@goto RPT
:GOTCMD

@echo Build %DATE% %TIME% > %TMPLOG%
@echo Build source %TMPSRC%... all output to build log %TMPLOG%
@echo Build source %TMPSRC%... all output to build log %TMPLOG% >> %TMPLOG%

cmake %TMPSRC% %TMPOPTS% >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR1

cmake --build . --config Debug >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR2

cmake --build . --config Release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR3
:DONEREL

@fa4 "***" %TMPLOG%
@call elapsed %TMPBGN%
@echo Appears a successful build... see %TMPLOG%

@goto DNTINST
@REM TODO: These temporary build-zipsx need attention
@echo Building installation zips... moment...
@call build-zipsx Debug
@call build-zipsx Release
@echo Done installation zips...
:DNTINST

@echo Continue with install? Only Ctrl+c aborts...

@%DOPAUSE%

cmake --build . --config Debug  --target INSTALL >> %TMPLOG% 2>&1
@REM if EXIST install_manifest.txt (
@REM copy install_manifest.txt install_manifest_dbg.txt >nul
@REM echo. >> %TMPINS%\installed.txt
@REM echo %TMPPRJ% Debug install %DATE% %TIME% >> %TMPINS%\installed.txt
@REM type install_manifest.txt >> %TMPINS%\installed.txt
@REM )

cmake --build . --config Release  --target INSTALL >> %TMPLOG% 2>&1
@REM if EXIST install_manifest.txt (
@REM copy install_manifest.txt install_manifest_rel.txt >nul
@REM echo. >> %TMPINS%\installed.txt
@REM echo %TMPPRJ% Release install %DATE% %TIME% >> %TMPINS%\installed.txt
@REM type install_manifest.txt >> %TMPINS%\installed.txt
@REM )

@echo.
@fa4 " -- " %TMPLOG%
@echo.

@if NOT EXIST  %TMPPDB% (
@echo NOTE: Can NOT locate %TMPPDB%! May get LINK warnings!
@echo NOTE: Can NOT locate %TMPPDB%! May get LINK warnings! >> %TMPLOG%
)

@REM TO do automate copy of Debug\*.pdb to 3rdParty
@call elapsed %TMPBGN%
@echo All done... see %TMPLOG%

@goto END

:ERR1
@echo cmake configuration or generations ERROR
@goto ISERR

:ERR2
@echo ERROR: Cmake build Debug FAILED!
@goto ISERR

:ERR3
@fa4 "mt.exe : general error c101008d:" %TMPLOG% >nul
@if ERRORLEVEL 1 goto ERR33
:ERR34
@echo ERROR: Cmake build Release FAILED!
@goto ISERR
:ERR33
@echo Try again due to this STUPID STUPID STUPID error
@echo Try again due to this STUPID STUPID STUPID error >>%TMPLOG%
cmake --build . --config Release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR34
@goto DONEREL


:ISERR
@echo See %TMPLOG% for details...
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
