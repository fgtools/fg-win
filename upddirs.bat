@setlocal
@set TMPTMP=
@set TMPDIR=zips
@set TMPDIR2=TEMPI

@REM set TMPDIR3=Projects
@REM NO, need this in ROOT

@if EXIST %TMPDIR%\nul goto GOT1
@echo Creating zips directory to hold zips of 3rdPArty sources
@md %TMPDIR%
@if NOT EXIST %TMPDIR%\nul goto FAILED1
@set TMPTMP=%TMPTMP% %TMPDIR%
:GOT1

@if EXIST %TMPDIR2%\nul goto GOT2
@echo Creating %TMPDIR2% directory to do temorary install for creating artifact zips
@md %TMPDIR2%
@if NOT EXIST %TMPDIR2%\nul goto FAILED2
@set TMPTMP=%TMPTMP% %TMPDIR2%
:GOT2

@if "%TMPTMP%x" == "x" (
@echo Appears all directories exist...
) else (
@echo Created %TMPTMP% directories...
)

@goto END

:FAILED1
@echo ERROR: Failed to create directory %TMPDIR%
@goto END

:FAILED2
@echo ERROR: Failed to create directory %TMPDIR2%
@goto END


:END


