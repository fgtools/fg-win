@setlocal
@set TMPFIL=..\build-jpeg\build-cmake.bat
@if EXIST %TMPFIL% goto DOUPD
@echo Warning: Can NOT locate %TMPFIL%! *** CHECK ME ***
@echo This file is needed to update the CMakeLists.txt, plus others...
@goto END

:DOUPD
cd ..\build-jpeg
call build-cmake
@cd ..\build-jpeg.x64

:END
