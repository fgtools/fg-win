@setlocal
@echo Check MSVC W64 setup looks ok...
@echo Env VCINSTALLDIR=%VCINSTALLDIR% Platform=%Platform%
@if "%VCINSTALLDIR%x" == "x" goto NOMSVC
@if /i "%Platform%x" == "x64x" goto DNPLAT
@echo Platform=%Platform% is NOT x64
@goto ISERR
:DNPLAT
@nmake /? >nul 2>&1
@if ERRORLEVEL 1 goto NONMK

@echo Appears all ok...

@goto END

:NOMSVC
@echo Error: Can NOT locate MSVC in the environment...
@echo MIssing VCINSTALLDIR
@goto ISERR

:NONMK
@echo Error: Test run of 'nmake' failed...
@goto ISERR

:ISERR
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof

