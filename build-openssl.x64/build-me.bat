@setlocal
@echo Special build of OpenSSL... See INSTALL.W64 for details...
@set TMPSRC=X:\openssl-1.0.2
@if NOT EXIST %TMPSRC%\nul goto NOSRC
@if NOT EXIST %TMPSRC%\ms\nul goto NOSRC
@if EXIST %TMPSRC%\out32dll\libeay32.dll goto NOWAY
@if EXIST %TMPSRC%\tmp32dll\*.obj goto NOWAY
@if EXIST %TMPSRC%\tmp32dll\cryptlib.h goto NOWAY

@set TMPINST=X:\3rdParty.x64
@set TMPFIL=%TMPINST%\ssl\openssl.cnf

@call msvcchk
@if ERRORLEVEL 1 goto NOMSVC

@if EXIST %TMPFIL% (
@echo Appears OpenSSL already INSTALLED
@echo Delete %TMPFIL% to re-do...
@goto END
)

CD %TMPSRC%
@if ERRORLEVEL 1 goto CDERR
@if NOT EXIST ms\nul goto NOSRC

@if EXIST donecfg.x64.txt goto DOMAKE

perl Configure VC-WIN64A no-asm --prefix=%TMPINST%
@if ERRORLEVEL 1 goto NOPERL

call ms\do_win64a

touch donecfg.x64.txt
@if ERRORLEVEL 1 goto TCHERR

:DOMAKE
@echo Delete 'donecfg.x64.txt' to repeat configure step...

@if EXIST out32dll\libeay32.dll goto DOTEST

nmake -f ms\ntdll.mak
@if ERRORLEVEL 1 goto ERR1

:DOTEST

nmake -f ms\ntdll.mak test
@if ERRORLEVEL 1 goto ERR2

@echo If 'passed all tests' do INSTALL? Only Ctrl+C aborts
@pause

nmake -f ms\ntdll.mak install

@goto END

:NONMK
nmake /?
@echo Appears an error running 'nmake /?'
@goto ISERR

:TCHERR
@echo Appears 'touch' failed! Is this app in your path...
@goto ISERR

:CDERR
@echo Appears 'CD %TMPSRC%' failed! ????
@goto ISERR

:NOPERL
@echo Appears the perl configure step FAILED!
@goto ISERR

:ERR1
@echo Appears a compile, link error..
@goto ISERR

:ERR2
@echo Appears 'test' failed..
@goto ISERR

:NOSRC
@echo ERROR: Can NOT locate SOURCE %TMPSRC%
@goto ISERR

:NOMSVC
@echo.
@echo ERROR: Not in MSVC command prompt...
@echo No VCINSTALLDIR in environment...
@goto ISERR

:NOWAY
@echo.
@echo ERROR: Found previous built products! These MUST be cleaned...
@echo Change directory to %TMPSRC% and
@echo run 'nmake -f ms\ntdll.mak vclean'
@echo The directories %TMPSRC%\out32dll and tmp32dll MUST be EMPTY
@echo.
@goto ISERR


:ISERR
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
