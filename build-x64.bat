@setlocal
@REM Check all build-me.bat exist - but these should be part of the repo so MUST exist
@set TMPPLIB=build-plib.x64\build-me.bat
@if NOT EXIST %TMPPLIB% goto NOPLIB2
@set TMPZLIB=build-zlib.x64\build-me.bat
@if NOT EXIST %TMPPLIB% goto NOZLIB2
@set TMPLPNG=build-png.x64\build-me.bat
@if NOT EXIST %TMPLPNG% goto NOLPNG2
@set TMPGLUT=build-freeglut.x64\build-me.bat
@if NOT EXIST %TMPGLUT% goto NOGLUT2
@set TMPPTHREAD=build-pthreads.x64\build-me.bat
@if NOT EXIST %TMPPTHREAD% goto NOTHREAD2

@REM Check all sources exist
@set TMPPLIB=plib-1.8.5
@if NOT EXIST %TMPPLIB%\nul goto NOPLIB
@set TMPZLIB=zlib-1.2.8
@if NOT EXIST %TMPPLIB%\nul goto NOZLIB
@set TMPLPNG=lpng1514
@if NOT EXIST %TMPLPNG%\nul goto NOLPNG
@set TMPGLUT=freeglut-3.0.0
@if NOT EXIST %TMPGLUT%\nul goto NOGLUT
@set TMPPTHREAD=pthreads
@if NOT EXIST %TMPPTHREAD%\nul goto NOTHREAD

@REM All looks ok to do the builds
@set TMPLIB=PLIB
@set TMPDIR=build-plib.x64
@call :DO_ONE
@set TMPLIB=ZLIB
@set TMPDIR=build-zlib.x64
@call :DO_ONE
@set TMPLIB=LPNG
@set TMPDIR=build-png.x64
@call :DO_ONE
@set TMPLIB=freeglut
@set TMPDIR=build-freeglut.x64
@call :DO_ONE
@set TMPLIB=pthreads
@set TMPDIR=build-pthreads.x64
@call :DO_ONE

@goto :EOF

:DO_ONE
@echo Checking %TMPLIB%
@if EXIST %TMPDIR%\build-me.bat (
@cd %TMPDIR%
@call build-me NOPAUSE
@if ERRORLEVEL 1 (
@echo Build of %TMPLIB% x64 FAILED!
@pause
)
@cd ..
) else (
@echo NOTE: %TMPDIR% does NOT exist, so no x64 build of %TMPLIB%
@pause
)
@goto :EOF

:NOLIB
@echo Can NOT locate %TMPPLIB%! *** FIX ME ***
@echo Perhaps run updplib.bat
@goto END
:NOZLIB
@echo Can NOT locate %TMPPLIB%! *** FIX ME ***
@echo Perhaps run updplib.bat
@goto END
:NOLPNG
@echo Can NOT locate %TMPLPNG%! *** FIX ME ***
@echo Perhaps run updpng.bat
@goto END
:NOGLUT
@echo Can NOT locate %TMPGLUT%! *** FIX ME ***
@echo Perhaps run updfreeglut.bat
@goto END
:NOTHREAD
@echo Can NOT locate %TMPPTHREAD%! *** FIX ME ***
@echo Perhaps run updpthreads.bat
@goto END

:NOLIB2
@echo Can NOT locate %TMPPLIB%! *** FIX ME ***
@goto BADREPO
:NOZLIB2
@echo Can NOT locate %TMPPLIB%! *** FIX ME ***
@goto BADREPO
:NOLPNG2
@echo Can NOT locate %TMPLPNG%! *** FIX ME ***
@goto BADREPO
:NOGLUT2
@echo Can NOT locate %TMPGLUT%! *** FIX ME ***
@goto BADREPO
:NOTHREAD2
@echo Can NOT locate %TMPPTHREAD%! *** FIX ME ***
@goto BADREPO

:BADREPO
@echo This file SHOULD be part of the REPO, so do not know why!
@echo See other 64-bit build-me.bat for clues as to what it contains
@goto END

:END
@REM eof
