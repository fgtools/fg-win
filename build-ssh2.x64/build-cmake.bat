@setlocal
@set TMPFIL=CMakeLists.txt
@set TMPSRC=..\build-ssh2\%TMPFIL%
@set TMPDIR=X:\libssh2-1.5.0
@set TMPDST=%TMPDIR%\%TMPFIL%
@if NOT EXIST %TMPSRC% goto NOSRC
@if NOT EXIST %TMPDIR%\nul goto NODIR

@call :DOFILCOPY

@goto END

@REM ####################################################
@REM Potentially update a file to the source
@REM ####################################################
:DOFILCOPY
@call dirmin %TMPSRC%
@if NOT EXIST %TMPDST% goto DOCOPY1
@call dirmin %TMPDST%
@fc4 /q /v0 %TMPSRC% %TMPDST% >nul
@if ERRORLEVEL 2 goto NOFC4
@if ERRORLEVEL 1 goto DOCOPY1
@echo Appear EXACTLY the SAME... nothing done...
@goto DNCOPY1
:NOFC4
@echo No fc4 utility found in PATH so doing copy...
:DOCOPY1
@echo Copy %TMPSRC% to %TMPDST%
copy %TMPSRC% %TMPDST%
:DNCOPY1
@goto :EOF
@REM ####################################################


:NOSRC
@echo Error: Can NOT find source %TMPSRC% *** FIX ME ***
@goto ISERR

:NODIR
@echo Error: Can NOT find directory %TMPDIR% *** FIX ME ***
@goto ISERR

:ISERR
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
