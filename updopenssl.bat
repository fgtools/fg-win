@setlocal

@echo SOURCE: http://www.openssl.org/source/
@REM set TMPDIR=openssl-1.0.1f - 20141212 changed to
@set TMPDIR=openssl-1.0.2

@REM =============================================
@set TMPZIP=zips\%TMPDIR%.zip
@if NOT EXIST %TMPZIP% goto ERR1
:DOUNZIP
@if EXIST %TMPDIR%\nul goto ERR2

call unzip8 -d %TMPZIP%

@goto END

:ERR1
@REM if EXIST %DOWNLOADS%\openssl\src\%TMPDIR%.zip (
@REM copy %DOWNLOADS%\openssl\src\%TMPDIR%.zip %TMPZIP%
@REM if EXIST %TMPZIP% goto DOUNZIP
@REM )
@if EXIST %DOWNLOADS%\%TMPDIR%.zip (
copy %DOWNLOADS%\%TMPDIR%.zip %TMPZIP%
@if EXIST %TMPZIP% goto DOUNZIP
) else (
@echo Can NOT locate %DOWNLOADS%\%TMPDIR%.zip, nor
)
@echo Can NOT locate %TMPZIP%
@goto END

:ERR2
@echo Directory exist %TMPDIR%
@call dirmin %TMPZIP%
@echo Is source from above zip... download later for update...
@goto END


:END
