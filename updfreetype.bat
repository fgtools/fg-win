@setlocal
@set TMPDIR=freetype-2.6.2
@set TMPZIP=ft262.zip
@set TMPSRC=zips\%TMPZIP%
@REM set TMPDIR=freetype-2.5.3
@REM set TMPZIP=ft253.zip
@REM set TMPDIR=freetype-2.5.2
@REM set TMPZIP=zips\%TMPDIR%.zip
@if NOT EXIST %TMPSRC% goto ERR1
:DOUNZIP
@if EXIST %TMPDIR%\nul goto ERR2

call unzip8 -d %TMPSRC%

@goto END

:ERR1
@if EXIST %DOWNLOADS%\%TMPZIP% (
copy %DOWNLOADS%\%TMPZIP% %TMPSRC%
@if EXIST %TMPSRC% goto DOUNZIP
)
@echo Can NOT locate %TMPZIP%
@goto END

:ERR2
@echo Directory exist %TMPDIR%
@call dirmin %TMPSRC%
@echo Is source from above zip... download later for update...
@echo http://download.savannah.gnu.org/releases/freetype/ or
@echo http://sourceforge.net/projects/freetype/
@goto END

:END
