@setlocal
@REM ######################################################
@REM If no gitlab.com account may need to switch repo to
@REM https://gitlab.com/fgtools/atlas-g.git
@REM #######################################################
@REM 20150327 - Update to SF repo
@set REPO=git@gitlab.com:fgtools/atlas-g.git
@REM set REPO=git@gitorious.org:fgtools/atlas-g.git
@set TMPDIR=atlas-g

@if NOT EXIST %TMPDIR%\nul goto DOCLONE

@cd %TMPDIR%
@call git status
@echo Continue with 'git pull'?
@pause

call git pull
@cd ..
@goto END

:DOCLONE
@echo Folder %TMPDIR% does NOT exist...
@echo Will do: 'call git clone %REPO% %TMPDIR%'
@echo *** CONTINUE? ***

@pause

@echo Doing: 'call git clone %REPO% %TMPDIR%'
call git clone %REPO% %TMPDIR%

@goto END

:END
@endlocal

