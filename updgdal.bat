@setlocal
@REM from : http://trac.osgeo.org/gdal/wiki/DownloadSource
@REM

@set TMPZIP=gdal191.zip
@set TMPSRC=zips\%TMPZIP%
@set TMPDIR=gdal-1.9.1
@set TMPDWN=%DOWNLOADS%\%TMPZIP%
@REM set TMPDIR=gdal-1.9.2

@if EXIST %TMPDIR%\nul (
@echo No update - this is a ZIP source %TMPSRC%
@if EXIST %TMPSRC% @call dirmin %TMPSRC%
) else (
@if NOT EXIST %TMPSRC% goto ERR1
:DOUNZIP
call unzip8 -d %TMPSRC%
@if NOT EXIST %TMPDIR%\nul goto ERR2
@echo Have unzipped source into %TMPDIR%
)
@endlocal
@exit /b 0

:ERR1
@if EXIST %TMPDWN% (
copy %TMPDWN% %TMPSRC%
@if EXIST %TMPSRC% goto DOUNZIP
@echo ERROR: Zip source %TMPDWN% copy to %TMPSRC% FAILED!
)
@echo ERROR: Zip source %TMPDWN% is NOT available!
@echo ERROR: Zip source %TMPSRC% is NOT available!
@goto ISERR

:ERR2
@echo ERROR: Unzip source %TMPSRC% FAILED to create %TMPDIR%!
@goto ISERR

:ISERR
@endlocal
@exit /b 1
