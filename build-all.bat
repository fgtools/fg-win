@setlocal
@set NOPAUSE=
@if "%~1x" == "NOPAUSEx" (
@set NOPAUSE=NOPAUSE
)
echo The whole thing...

@REM goto NEXT

call updboost %NOPAUSE%

@if ERRORLEVEL 1 goto ISERR
call updzlib %NOPAUSE%
cd build-zlib
call build-zlib %NOPAUSE%
cd ..

call updpng.bat %NOPAUSE%
@if ERRORLEVEL 1 goto ISERR
cd build-png
call build-me %NOPAUSE%
@if ERRORLEVEL 1 goto ISERR
cd ..

@REM Add some more components to the OSG build
call updfreetype %NOPAUSE%
@if ERRORLEVEL 1 goto ISERR
cd build-freetype
call build-me %NOPAUSE%
@if ERRORLEVEL 1 goto ISERR
cd ..

call updlibxml2
@if ERRORLEVEL 1 goto ISERR
cd build-xml2
call build-me %NOPAUSE%
@if ERRORLEVEL 1 goto ISERR
cd ..

call updosg %NOPAUSE%
@if ERRORLEVEL 1 goto ISERR
cd build-osg
call build-me %NOPAUSE%
@if ERRORLEVEL 1 goto ISERR
cd ..

call updopenal %NOPAUSE%
@if ERRORLEVEL 1 goto ISERR
cd build-openal 
call build-me %NOPAUSE%
@if ERRORLEVEL 1 goto ISERR
cd ..

call updsg %NOPAUSE%
@if ERRORLEVEL 1 goto ISERR
cd build-sg
call build-me %NOPAUSE%
@if ERRORLEVEL 1 goto ISERR
cd ..

call updplib %NOPAUSE%
@if ERRORLEVEL 1 goto ISERR
cd build-plib 
call build-me %NOPAUSE%
@if ERRORLEVEL 1 goto ISERR
cd ..

call updfreeglut %NOPAUSE%
@if ERRORLEVEL 1 goto ISERR
cd build-freeglut
call build-me %NOPAUSE%
@if ERRORLEVEL 1 goto ISERR
cd ..

call updfg %NOPAUSE%
@if ERRORLEVEL 1 goto ISERR
cd build-fg
call build-me %NOPAUSE%
@if ERRORLEVEL 1 goto ISERR
cd ..

@echo.
@echo Congrats! Appears a SUCCESSFUL build...
@echo.
@if EXIST fgdata\version goto FNDFGD
@echo.
@echo Once you have cloned fgdata you are ready to FLY...
@echo You can use updfgd.bat for this...
@echo.
@echo If cloned elsewhere, updae it and adjust run-fg.bat to point to YOUR fgdata,
@echo and up, up and away...
@echo.
@goto END

:FNDFGD
@echo.
@echo Found fgdata\version - doing an update...
@echo.
@call updfgd %NOPAUSE%
@if ERRORLEVEL 1 goto UPDERR
@echo.
@echo Appears you are ready to FLY...
@echo.
@echo Try run-fg.bat
@echo.
@goto END


:UPDERR
@echo Appears an error in updating fgdata..
@goto ISERR

:ISERR
@echo.
@endlocal
@echo *** GOT AN ERROR *** FIX ME ***
exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
