@setlocal

@set TMPPRJ=szip
@set TMPSRC=..\szip-2.1
@set TMPINST=X:\3rdParty
@if NOT EXIST %TMPSRC%\nul goto NOSRC
@if NOT EXIST %TMPSRC%\CMakeLists.txt goto NOCM

@set TMPLOG=bldlog-1.txt
@set DOPAUSE=ask

@call chkmsvc %TMPPRJ%

@echo Bgn build %TIME%, output to %TMPLOG%
@echo Bgn build %TIME% >%TMPLOG%
@set TMPOPTS=
@set TMPOPTS=%TMPOPTS% -DCMAKE_INSTALL_PREFIX=%TMPINST%
@REM Try for SHARED (DLL) library
@REM set TMPOPTS=%TMPOPTS% -DBUILD_SHARED_LIBS=ON

@if EXIST build-cmake.bat (
@call build-cmake >> %TMPLOG%
@if ERRORLEVEL 1 goto ERR0
)

@echo Doing: 'cmake %TMPSRC% %TMPOPTS%'
@echo Doing: 'cmake %TMPSRC% %TMPOPTS%' >>%TMPLOG%
@cmake %TMPSRC% %TMPOPTS% >>%TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR1

@echo Doing 'cmake --build . --config Debug'
@echo Doing 'cmake --build . --config Debug' >>%TMPLOG%
@cmake --build . --config Debug >>%TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR2

@echo Doing 'cmake --build . --config Release'
@echo Doing 'cmake --build . --config Release' >>%TMPLOG%
@cmake --build . --config Release >>%TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR3
:DNRELBLD

@echo Appears successful... see %TMPLOG% for details...
@echo.
@echo Continue with install to %TMPINST%?
@echo.
@%DOPAUSE% Only 'y' continues...
@if ERRORLEVEL 2 goto NOASK
@if ERRORLEVEL 1 goto DOINST
@echo.
@echo No - skipping install at this time...
@echo.
@goto END

:NOASK
@echo.
@echo Warning: Utility ask not found in PATH! SKIPPING INSTALL
@echo.
@goto END

:DOINST

@echo Doing 'cmake --build . --config Debug --target INSTALL'
@echo Doing 'cmake --build . --config Debug --target INSTALL' >>%TMPLOG%
@cmake --build . --config Debug  --target INSTALL>>%TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR4

@echo Doing 'cmake --build . --config Release --target INSTALL'
@echo Doing 'cmake --build . --config Release --target INSTALL' >>%TMPLOG%
@cmake --build . --config Release --target INSTALL >>%TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR5

@REM Show waht is installed
@echo.
@fa4 " -- " %TMPLOG%
@echo.

@echo Appears successful install ... see %TMPLOG% for details...
@echo.
@goto END

:NOSRC
@echo Error: Can NOT locate folder %TMPSRC%! *** FIX ME ***
@goto ISERR

:NOCM
@echo Error: Can NOT locate folder %TMPSRC%\CMakeLists.txt! *** FIX ME ***
@goto ISERR

:ERR0
@echo Error: cmake update error
@goto ISERR

:ERR1
@echo Error: cmake configuration, generation error
@goto ISERR

:ERR2
@echo Error: build Debug error
@goto ISERR

:ERR3
@fa4 "mt.exe : general error c101008d:" %TMPLOG%
@if ERRORLEVEL 1 goto ERR32
@echo Appears "mt.exe : general error c101008d:" NOT FOUND
@goto ERR33
:ERR32
@echo Try again to overcome this STUPID error
@echo Try again to overcome this STUPID error >>%TMPLOG%
nmake -f Makefile >>%TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR33
@goto DNRELBLD
:ERR33
@echo Error: build Release error
@goto ISERR

:ERR4
@echo Error: install Debug error
@goto ISERR

:ERR5
@echo Error: install Release error
@goto ISERR


:ISERR
@echo see %TMPLOG% for details...
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
