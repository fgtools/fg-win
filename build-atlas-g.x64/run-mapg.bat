@setlocal
@REM set TMPEXE=Release\mapg.exe
@set TMPEXE=Debug\mapgd.exe
@if NOT EXIST %TMPEXE% goto ERR1
@set TMP3RD=X:\3rdParty.x64\bin
@if NOT EXIST %TMP3RD%\nul goto ERR2
@set TMPROOT=X:\fgdata
@if NOT EXIST %TMPROOT%\nul goto ERR3
@set TMPPAL=X:\atlas-g\src\data\Palettes\default.ap
@if NOT EXIST %TMPPAL% goto ERR4

@set TMPOUT=temp-base
@echo.
@echo This runs %TMPEXE% to generate just the small set of tiles given in the 'base' data.
@echo The output is to the %TMPOUT% directory, which will be create if NOT existing...
@echo The palette file used it %TMPPAL%
@echo First RUN will be using --test to show what will be created...

@set TMPCMD=--fg-root=%TMPROOT% --png --atlas=%TMPOUT% --palette=%TMPPAL%
:RPT
@if "%~1x" == "x" goto GOTCMD
@set TMPCMD=%TMPCMD% %1
@shift
@goto RPT
:GOTCMD

@echo Will run: '%TMPEXE% %TMPCMD%'
@echo *** CONTINUE? *** Only Ctrl+C aborts... all other keys continue...
@pause

@REM goto END

@if NOT EXIST %TMPOUT%\nul (
@md %TMPOUT%
)
@if NOT EXIST %TMPOUT%\nul goto ERR5


@set PATH=%TMP3RD%;%PATH%
@echo Added %TMP3RD% to the PATH...

%TMPEXE% %TMPCMD% --test

@echo.
@echo Continue without test?
@echo Only Ctrl+C aborts... all other keys continue...
@pause

%TMPEXE% %TMPCMD%

@goto END

:ERR1
@echo Can NOT find the EXE %TMPEXE%! *** FIX ME ***
@goto ISERR

:ERR2
@echo Can NOT find folder %TMP3RD%! *** FIX ME ***
@goto ISERR

:ERR3
@echo Can NOT find folder %TMPROOT%! *** FIX ME ***
@goto ISERR

:ERR4
@echo Can NOT find palette file %TMPPAL%! *** FIX ME ***
@goto ISERR

:ERR5
@echo Failed to create folder %TMPOUT%! *** FIX ME ***
@goto ISERR

:ISERR
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
