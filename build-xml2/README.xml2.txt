README.xml2.txt - 20240208 - 20140207

After several tries at creating a CMakeLists.txt FAILED to do 
te right thing, turned to trying the given build systems...

In the folder win32 ran -
> cscript configure.js compiler=msvc prefix=F:\FG\18\3rdParty iconv=no

This got me a 'make' that worked...

Maybe I should try again the build the 'iconv' library...

But now with iconv=no, was able to build the libxml.dll, in the MSVC10 
command prompt, using 
F:\FG\18\libxml2-2.9.1\win32>nmake -f Makefile.msvc

This built -
 F:\FG\18\libxml2-2.9.1\win32\bin.msvc\libxml2.dll, and the 
 link equired libxml2.lib...
 And it built some 19 EXE files -
runsuite.exe,runtest.exe. testapi.exe. testAutomata.exe. testC14N.exe,
testHTML.exe. testlimits.exe, testModule.exe, testReader.exe, testrecurse.exe,
testRegexp.exe, testRelax.exe. testSAX.exe. testSchemas.exe. testThreadsWin32.exe,
testURI.exe, testXPath.exe. xmlcatalog.exe, xmllint.exe

So this is a good SUCCESS, but why did my cmake FAIL?

Some things noted -
This build defined /D "NOLIBTOOL"
It defines VERSION
/VERSION:$(LIBXML_MAJOR_VERSION).$(LIBXML_MINOR_VERSION)

It has a checktest: target, so try that, but can not find how to 
invoke that 'target'????????

=======================================================================
TODO: Solve WHAT features are enabled???

A comparison between original xmlwin32version.h and a copy from the linux build xmlunixversion.h
shows at the moment LOTS of features enabled in UNIX are NOT yet enabled in WIN32!
Also I put the switches in CMakeLists.txt, but they MUST be in the final xmlversion.h
so when the library is used by apps, all the correct items will be anabled.

In addition to this, there is the question of the 'exports'. If building libxml2 as a 
DLL then obviously the MSVC correct __declspec(export) __declspec(import) must be 
correctly switched. Likewise appropriate declaration if LIBXML_STATIC used/defined.

First trial: Bring the xmlwin32version.h much closer to xmlunixversion.h, and try 
a build. IT WORKED! YEAH!!!
Now running: F:\FG\18\build-xml2>release\xmllint --version
release\xmllint: using libxml version 20901-ZIP
   compiled with: Threads Tree Output Push Reader Patterns Writer SAXv1 FTP HTTP DTDValid HTML 
   Legacy C14N Catalog XPath XPointer XInclude ISO8859X Unicode Regexps Automata Expr Schemas 
   Schematron Modules Debug Zlib
which is now the SAME as linux, EXCEPT for iconv, since have not yet built that in windows.

In the source root F:\FG\18\libxml2-2.9.1 ran F:\FG\18\build-xml2\Release\runsuite, with results
## XML Schemas datatypes test suite from James Clark
Ran 1035 tests, 10 errors, 0 leaks
## Relax NG test suite from James Clark
Ran 253 tests, no errors
## Relax NG test suite for libxml2
Ran 183 tests, no errors
Failed to parse xstc/Tests/Metadata/NISTXMLSchemaDatatypes.testSet
Ran 0 tests (0 schemata), no errors
Failed to parse xstc/Tests/Metadata/SunXMLSchema1-0-20020116.testSet
Ran 0 tests (0 schemata), no errors
Failed to parse xstc/Tests/Metadata/MSXMLSchema1-0-20020116.testSet
Ran 0 tests (0 schemata), no errors
Total 1471 tests, 10 errors, 0 leaks
Can NOT find the *.testSet in the source... nor in the unix source???

In the source root F:\FG\18\libxml2-2.9.1 ran F:\FG\18\build-xml2\Release\runtests, with results
## XML regression tests
## XML regression tests on memory
## XML entity subst regression tests
## XML Namespaces regression tests
## Error cases regression tests
## Error cases stream regression tests
## Reader regression tests
## Reader entities substitution regression tests
## Reader on memory regression tests
## Walker regression tests
## SAX1 callbacks regression tests
## SAX2 callbacks regression tests
## XML push regression tests
## HTML regression tests
## Push HTML regression tests
## HTML SAX regression tests
## Valid documents regression tests
## Validity checking regression tests
## Streaming validity checking regression tests
## Streaming validity error checking regression tests
## General documents valid regression tests
## XInclude regression tests
## XInclude xmlReader regression tests
## XInclude regression tests stripping include nodes
## XInclude xmlReader regression tests stripping include nodes
## XPath expressions regression tests
## XPath document queries regression tests
## XPointer document queries regression tests
## xml:id regression tests
## URI parsing tests
## URI base composition tests
## Path URI conversion tests
## Schemas regression tests
## Relax-NG regression tests
## Relax-NG streaming regression tests
## Pattern regression tests
## C14N with comments regression tests
## C14N without comments regression tests
## C14N exclusive without comments regression tests
## C14N 1.1 without comments regression tests
## Catalog and Threads regression tests
Total 2876 tests, 10 errors, 0 leaks
File ./test/ebcdic_566012.xml generated an error
File ./test/ebcdic_566012.xml generated an error
File ./test/ebcdic_566012.xml generated an error
Result for ./test/ebcdic_566012.xml failed
File ./test/ebcdic_566012.xml generated an error
Result for ./test/ebcdic_566012.xml failed
File ./test/ebcdic_566012.xml generated an error
Result for ./test/ebcdic_566012.xml failed
File ./test/ebcdic_566012.xml generated an error
Failed to parse ./test/ebcdic_566012.xml
File ./test/ebcdic_566012.xml generated an error
Failed to parse ./test/ebcdic_566012.xml
File ./test/ebcdic_566012.xml generated an error
Failed to parse ./test/ebcdic_566012.xml
File ./test/ebcdic_566012.xml generated an error
Failed to parse ./test/ebcdic_566012.xml
File ./test/ebcdic_566012.xml generated an error
This looks good. Some errors but seem all in one file...

Ran F:\FG\18\build-xml2\Release>testapi, with results
Testing HTMLparser : 32 of 38 functions ...
Testing HTMLtree : 18 of 18 functions ...
Testing SAX2 : 38 of 38 functions ...
Testing c14n : 3 of 4 functions ...
Testing catalog : 27 of 36 functions ...
Testing chvalid : 9 of 9 functions ...
Testing debugXML : 25 of 28 functions ...
Testing dict : 10 of 13 functions ...
Testing encoding : 16 of 19 functions ...
Testing entities : 13 of 17 functions ...
Testing hash : 16 of 24 functions ...
Testing list : 19 of 26 functions ...
Testing nanoftp : 14 of 22 functions ...
Testing nanohttp : 13 of 17 functions ...
Testing parser : 61 of 70 functions ...
Testing parserInternals : 33 of 90 functions ...
Testing pattern : 10 of 15 functions ...
Testing relaxng : 14 of 24 functions ...
Testing schemasInternals : 0 of 2 functions ...
Testing schematron : 1 of 10 functions ...
Testing tree : 142 of 164 functions ...
Testing uri : 10 of 15 functions ...
Testing valid : 50 of 70 functions ...
Testing xinclude : 8 of 10 functions ...
Testing xmlIO : 40 of 50 functions ...
Testing xmlautomata : 3 of 19 functions ...
Testing xmlerror : 7 of 15 functions ...
Testing xmlmodule : 2 of 4 functions ...
Testing xmlreader : 76 of 86 functions ...
Testing xmlregexp : 16 of 30 functions ...
Testing xmlsave : 4 of 10 functions ...
Testing xmlschemas : 16 of 27 functions ...
Testing xmlschemastypes : 26 of 34 functions ...
Testing xmlstring : 26 of 30 functions ...
Testing xmlunicode : 166 of 166 functions ...
Testing xmlwriter : 52 of 80 functions ...
Testing xpath : 32 of 40 functions ...
Testing xpathInternals : 106 of 117 functions ...
Testing xpointer : 17 of 21 functions ...
Total: 1171 functions, 291418 tests, 0 errors
No 'errors' LOOK good.

Then built a batch file, run-tests.bat, to run xmllint.exe on ALL 
the XML files in the source. It again showed that ebcdic support 
is NOT built in, but seemed to parse all the others... output is 
FAR too large for here... Run the test yourself...

Of the 840 XML files in the source, 44 exited non 0. That is 796 passed 
with no problem.

I declare this libxml2.lib as SUCCESS, and now proceed to install it 
into my 3rdParty folder...

=======================================================================
TODO: Try the install target... DONE
Also added finding this library to my cmake-test project, and it was 
located fine.

PHEW, seem I got there. Always 'more' difficult when it is NOT a native 
cmake project, while I INSIST to do the building with cmake, thus must 
learn a lot about the project to ge it right.

Geoff. 
# eof
