@setlocal
@set TMPLOG=temptest.txt
@set TMPLIST=testxml.txt
@set TMPERR=temperr.txt

@set TMPEXE=Release\xmllint.exe
@if NOT EXIST %TMPLIST% goto ERR1
@if NOT EXIST %TMPEXE% goto ERR2

@set TMPCNT1=0
@set TMPCNT2=0
@set TMPCNTM=0
@set TMPCNTE=0

@for /F %%i in (%TMPLIST%) do @(call :DOCNT %%i)

@echo Found %TMPCNT1% files to process...
@if NOT "%TMPCNTM%" == "0" (
@echo Note %TMPCNTM% were missing..
)
@if "%MTPCNT1" == "0" (
@echo Appears no files to process!!!
@goto END
)
@echo All output will be to %TMPLOG%
@echo *** CONTINUE? ***
@pause

@REM goto END
@echo Begin %DATE% %TIME% >%TMPLOG%
@echo # error list >%TMPERR%

@for /F %%i in (%TMPLIST%) do @(call :DOCHK %%i)

@echo Done %TMPCNT2% of %TMPCNT1%. Errors %TMPCNTE%
@echo ============================ >>%TMPLOG%
@echo Done %TMPCNT2% of %TMPCNT1% >> %TMPLOG%
@echo END %DATE% %TIME% >>%TMPLOG%
@echo See %TMPLOG% for output results...
@if NOT "%TMPCNTE%" == "0" (
@echo Also see %TMPERR% file for those that exited not 0
)
@goto END

:DOCNT
@if "%~1x" == "x" goto :EOF
@if NOT EXIST %1 goto NOFILE
@set /A TMPCNT1+=1
@goto :EOF
:NOFILE
@echo can NOT locate file %1
@set /A TMPCNTM+=1
@pause
@goto :EOF


:DOCHK
@if "%~1x" == "x" goto :EOF
@if NOT EXIST %1 goto :EOF
@echo ============================ >>%TMPLOG%
@set /A TMPCNT2+=1
@echo File %TMPCNT2% of %TMPCNT1%: %1
@echo File %TMPCNT2% of %TMPCNT1%: %1 >>%TMPLOG%
%TMPEXE% %1 >>%TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ISERR
@echo Exit ERRORLEVEL=%ERRORLEVEL% >>%TMPLOG%
@goto :EOF
:ISERR
@echo Exit ERRORLEVEL=%ERRORLEVEL% >>%TMPLOG%
@echo %1 >>%TMPERR%
@set /A TMPCNTE+=1
@goto :EOF



:ERR1
@echo Error: Can NOT locate the file %TMPLIST%, containing 
@echo a list of XML files to test! *** FIX ME ***
@echo The list can be generated from the libxml2 root with
@echo dir F:\FG\18\libxml2-2.9.1\*.xml /s /b > testxml.txt
@goto END

:ERR2
@echo Error: Can NOT locate the 'test' EXE %TMPEXE%! Has it been built? *** FIX ME ***
@goto END


:END
