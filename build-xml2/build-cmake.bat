@setlocal

@set TMPSRC=X:\libxml2-2.9.1
@set TMPFIL=CMakeLists.txt
@set TMPFIL2=config.h.cmake
@set TMPSRC2=%TMPSRC%
@set TMPFIL3=xmlwin32version.h
@set TMPSRC3=%TMPSRC%\include\libxml
@set TMPFIL4=new-config.h
@set TMPDST4=X:\libxml2-2.9.1\win32\VC10

@if NOT EXIST %TMPSRC%\nul goto NOSRC
@if NOT EXIST %TMPSRC2%\nul goto NOSRC2
@if NOT EXIST %TMPSRC3%\nul goto NOSRC3
@if NOT EXIST %TMPFIL% goto NOFIL
@if NOT EXIST %TMPFIL2% goto NOFIL2
@if NOT EXIST %TMPFIL3% goto NOFIL3
@if NOT EXIST %TMPFIL4% goto NOFIL4
@if NOT EXIST %TMPDST4%\nul goto NODST4

@if NOT EXIST %TMPSRC%\%TMPFIL% goto DOCOPY
@fc4 -v0 -q %TMPFIL% %TMPSRC%\%TMPFIL%
@if ERRORLEVEL 1 goto DOCOPY
@call dirmin %TMPSRC%\%TMPFIL%
@echo Appears no change in %TMPFIL%... *** NOTHING DONE ***
@goto DNCOPY
:DOCOPY
@call dirmin %TMPFIL%
@if EXIST %TMPSRC%\%TMPFIL% @call dirmin %TMPSRC%\%TMPFIL%
@echo Copying %TMPFIL% to %TMPSRC%
copy %TMPFIL% %TMPSRC%
@if NOT EXIST %TMPSRC%\%TMPFIL% goto NOCOPY
@call dirmin %TMPSRC%\%TMPFIL%
@echo Done copy of %TMPFIL% to %TMPSRC%
:DNCOPY

@REM Add any other 'created' files
@REM ==============================================
@if NOT EXIST %TMPSRC2%\%TMPFIL2% goto DOCOPY2
@fc4 -v0 -q %TMPFIL2% %TMPSRC2%\%TMPFIL2%
@if ERRORLEVEL 1 goto DOCOPY2
@call dirmin %TMPSRC2%\%TMPFIL2%
@echo Appears no change in %TMPFIL2%... *** NOTHING DONE ***
@goto DNCOPY2
:DOCOPY2
@call dirmin %TMPFIL2%
@if EXIST %TMPSRC2%\%TMPFIL2% @call dirmin %TMPSRC2%\%TMPFIL2%
@echo Copying %TMPFIL2% to %TMPSRC2%
copy %TMPFIL2% %TMPSRC2%
@if NOT EXIST %TMPSRC2%\%TMPFIL2% goto NOCOPY2
@call dirmin %TMPSRC2%\%TMPFIL2%
@echo Done copy of %TMPFIL2% to %TMPSRC2%
:DNCOPY2

@REM ==============================================
@if NOT EXIST %TMPSRC3%\%TMPFIL3% goto DOCOPY3
@fc4 -v0 -q %TMPFIL3% %TMPSRC3%\%TMPFIL3%
@if ERRORLEVEL 1 goto DOCOPY3
@call dirmin %TMPSRC3%\%TMPFIL3%
@echo Appears no change in %TMPFIL3%... *** NOTHING DONE ***
@goto DNCOPY3
:DOCOPY3
@call dirmin %TMPFIL3%
@if EXIST %TMPSRC3%\%TMPFIL3% @call dirmin %TMPSRC3%\%TMPFIL3%
@echo Copying %TMPFIL3% to %TMPSRC3%
copy %TMPFIL3% %TMPSRC3%
@if NOT EXIST %TMPSRC3%\%TMPFIL3% goto NOCOPY3
@call dirmin %TMPSRC3%\%TMPFIL3%
@echo Done copy of %TMPFIL3% to %TMPSRC3%
:DNCOPY3

@if NOT EXIST %TMPDST4%\config.h goto DOCOPY4
@fc4 -v0 -q %TMPFIL4% %TMPDST4%\config.h
@if ERRORLEVEL 1 goto DOCOPY4
@call dirmin %TMPDST4%\config.h
@echo Appears no change in %TMPFIL4%... *** NOTHING DONE ***
@goto DNCOPY4
:DOCOPY4

@call dirmin %TMPFIL4%
@if EXIST %TMPDST4%\config.h @call dirmin %TMPDST4%\config.h
@echo Copying %TMPFIL4% to %TMPDST4%
copy %TMPFIL4% %TMPDST4%\config.h
@if NOT EXIST %TMPDST4%\config.h goto NOCOPY4
@call dirmin %TMPDST4%\config.h
@echo Done copy of %TMPFIL4% to %TMPDST4%\config.h
:DNCOPY4

@goto END

:NOCOPY
@echo Copy of %TMPFIL% to %TMPSRC% FAILED!
@goto ISERR

:NOCOPY2
@echo Copy of %TMPFIL2% to %TMPSRC2% FAILED!
@goto ISERR

:NOCOPY3
@echo Copy of %TMPFIL3% to %TMPSRC2% FAILED!
@goto ISERR

:NOCOPY4
@echo Copy of %TMPFIL4% to %TMPDST4% FAILED!
@goto ISERR

:NOSRC
@echo ERROR: Can NOT locate source [%TMPSRC%]!
@goto ISERR

:NOSRC2
@echo ERROR: Can NOT locate source [%TMPSRC2%]!
@goto ISERR

:NOSRC3
@echo ERROR: Can NOT locate source [%TMPSRC319:21 2019-09-28%]!
@goto ISERR

:NOFIL
@echo ERROR: Can NOT locate file [%TMPFIL%]!
@goto ISERR

:NOFIL2
@echo ERROR: Can NOT locate file [%TMPFIL2%]!
@goto ISERR

:NOFIL3
@echo ERROR: Can NOT locate file [%TMPFIL3%]!
@goto ISERR

:NOFIL4
@echo ERROR: Can NOT locate file [%TMPFIL4%]!
@goto ISERR

:NODST4
@echo ERROR: Can NOT locate file [%TMPDST4%]!
@goto ISERR
:ISERR
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
