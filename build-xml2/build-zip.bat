@setlocal
@set TMPPROJ=libxml2
@set TMPPRJ=xml2
@set TMPJWS=F:\Projects\workspace

@REM get to where we want to be
@REM call h%TMPPRJ%m NOTITLE
@REM if ERRORLEVEL 1 (
@REM echo h%TMPPRJ%m.bat failed!
@REM goto ISERR
@REM )

@REM Ok, I want to keep REL and DBG separate, at least for NOW
@set TMPROOT=F:\FG\18\TEMPI
@set TMPTARG=3rdParty
@set TMPMSVC=msvc100

@REM DEFAULT to RELEASE
@set TMPCONF=Release
@set TMPCFG=rel
@set TMPZDIR=%TMPJWS%\win32-%TMPPRJ%

@if "%~1x" == "x" goto GOTCMD
@if "%~1x" == "Releasex" (
@set TMPCONF=Release
@set TMPCFG=rel
) else (
@if "%~1x" == "Debugx" (
@set TMPCONF=Debug
@set TMPCFG=dbg
) else (
@echo ERROR: Unknown command [%1] IGNORED
)
)
:GOTCMD

@set TMPZIP=%TMPPROJ%-%TMPMSVC%-%TMPCFG%
@if NOT "%TMPVER%x" == "x" (
@set TMPZIP=%TMPZIP%-%TMPVER%
)
@set TMPZIP=%TMPZIP%.zip

@echo Creating %TMPZIP%...
@if NOT EXIST %TMPROOT%\nul goto NODIR

@set TMPDST=%TMPROOT%\%TMPTARG%

@set TMPZFIL=%TMPZDIR%\%TMPZIP%
@if NOT EXIST %TMPZDIR%\nul goto NODIR2

@if EXIST %TMPDST%\nul (
@echo Completely DELETING previous %TMPDST%
@echo Doing: 'xdelete -NO-confirmation-please=0 -dfrm %TMPDST%'
xdelete -NO-confirmation-please=0 -dfrm %TMPDST%
)

@if EXIST %TMPDST%\nul (
@echo ERROR: Directory %TMPDST% was NOT deleted!
@goto ISERR
)

@REM Now build backup the directory structure
@md %TMPDST%
@if NOT EXIST %TMPDST%\nul (
@echo ERROR: Unable to create directory %TMPDST%!
@goto ISERR
)

@if EXIST %TMPZFIL% (
@call dirmin %TMPZFIL%
@echo Deleting above previous ZIP
@del %TMPZFIL% >nul
)

@echo Doing 'call geninst %TMPDST% %TMPCONF% NOPAUSE'
call geninst %TMPDST% %TMPCONF% NOPAUSE
@if ERRORLEVEL 1 goto NOINST

cd %TMPROOT%
@echo Changed work directory to %CD%
@echo Doing: 'call zip8 -a -r -P -o %TMPZFIL% %TMPTARG%\*.*'
call zip8 -a -r -P -o %TMPZFIL% %TMPTARG%\*.* >nul 2>&1

@if NOT EXIST %TMPZFIL% (
@echo Zip to %TMPZFIL% FAILED!
@goto ISERR
)

@echo Appears zip creation was successful...
@call unzip8 -vb %TMPZFIL%
@call dirmin %TMPZFIL%

@goto END

:NOINST
@echo ERROR: 'call geninst %TMPDST% %TMPCONF% NOPAUSE' FAILED!
@goto ISERR

:NODIR
@echo ERROR: %TMPROOT% does NOT exist...
@goto ISERR

:NODIR2
@echo ERROR: Destination for ZIP, [%TMPZDIR%] does NOT exist...
@goto ISERR

:ISERR
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
