@setlocal
@REM Update to FG X: drive build
@set TMPDRV=X:
@set TMPROOT=%TMPDRV%
@set TMPEXE=install\msvc100\flightgear\bin\fgfs.exe
@set FG_ROOT=%TMPDRV%\fgdata
@set TMP3RD=%TMPROOT%\3rdParty\bin
@set TMPOSG=%TMPROOT%\install\msvc100\OpenSceneGraph\bin
@set TMPSVN=%TMPDRV%\fgsvnts
@set DOPAUSE=pause

@if NOT EXIST %TMPROOT%\nul goto ERR3
@if NOT EXIST %TMPEXE% goto ERR1
@if NOT EXIST %FG_ROOT%\nul goto ERR2
@if NOT EXIST %TMP3RD%\nul goto ERR4
@if NOT EXIST %TMPOSG%\nul goto ERR5
@REM Is this required? YES IT IS!!! Either create it, or disable terrasync
@if NOT EXIST %TMPSVN%\nul goto ERR6

@call dirmin %TMPEXE%
@echo Using the above executable...

@REM Maybe add  --console ???
@set TMPCMD="--fg-root=%FG_ROOT%" "--timeofday=noon" "--terrasync-dir=%TMPSVN%" --enable-terrasync --console
@REM set TMPCMD=%TMPCMD% "--callsign=GA007" "--fg-aircraft=D:\FG\fgaddon\Aircraft"

:RPT
@if "%~1x" == "x" goto GOTCMD
@if "%~1x" == "NOPAUSEx" (
@set DOPAUSE=echo No pause requested!
) else (
@set TMPCMD=%TMPCMD% %1
)
@shift
@goto RPT
:GOTCMD 

@set TMPTN=
@REM Use this to enable a TELNET connection to FG
@REM set TMPTN="--telnet=foo,bar,100,foo,5556,bar"

@set TMPSCENE=
@REM Use this to point to 'other' scenery downloads
@REM set TMPSCENE="--fg-scenery=D:\Scenery\terrascenery\data\Scenery"

@echo Will RUN: '%TMPEXE% %TMPSCENE% %TMPCMD% %TMPTN%'
@echo *** CONTINUE? *** Only Ctrl+C will abort...
@%DOPAUSE%
@set BGNTIM=%TIME%

@REM Fix the PATH to find the shared (DLL) libraires
@echo Adding %TMPOSG% and
@echo %TMP3RD% to PATH...

@set PATH=%TMPOSG%;%TMP3RD%;%PATH%

%TMPEXE% %TMPCMD% %TMPSCENE% %TMPTN%

@call elapsed %BGNTIM%

@goto END

:ERR1
@echo Error: Can NOT locate %TMPEXE%! *** FIX ME ***
@echo Has it been built, and installed?
@goto ISERR

:ERR2
@echo Error: Can NOT locate root data %FG_ROOT%! *** FIX ME ***
@goto ISERR

:ERR3
@echo Error: Can NOT locate root folder %TMPROOT%! *** FIX ME ***
@goto ISERR

:ERR4
@echo Error: Can NOT locate 3rdParty folder %TMP3RD%! *** FIX ME ***
@goto ISERR

:ERR5
@echo Error: Can NOT locate OpenSceneGraphs install %TMPOSG%! *** FIX ME ***
@goto ISERR

:ERR6
@echo Error: Can NOT locate terrasync download folder %TMPSVN%! *** FIX ME ***
@echo This is required! YES IT IS!!! Either create it, or disable terrasync...
@goto ISERR

:ISERR
@echo The above ERROR indicates this batch file MUST be amended to suit your particular environment...
@echo Make the appropriate fix, hopefully in the first 10 lines of so of this batch file, and try again...
@endlocal
@exit /b 1


:END
@endlocal
@exit /b 0

@REM eof
