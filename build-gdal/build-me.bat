@setlocal
@set TMPBGN=%TIME%

@set ADDUPD=0
@set ADDINST=1
@set ADDTINST=1
@set ADDDBG=1
@set ADDCLEAN=0
@set ADDDEBUG=
@set DOPAUSE=pause
@if "%~1x" == "NOPAUSEx" (
@set DOPAUSE=echo No pause requested
@shift
)

@REM set ADDDEBUG=-v9 -d extra -ll

@REM Get to the right FOLDER
@REM call hfg-current

@set TMPPROJ=gdal
@set TMPPRJ=%TMPPROJ%
@set TMPVERS=-1.9.1
@set TMPJENK=win32-%TMPPRJ%
@set TMPMSVC=msvc100

@set TMPBASE=X:
@set TMPBLD=%TMPBASE%\build-%TMPPRJ%
@set TMPSRC=%TMPBASE%\%TMPPROJ%%TMPVERS%
@set TMPGEN=Visual Studio 10
@set TMPINST=%TMPBASE%\3rdParty
@set TMPIFIL=%TMPBASE%\3rdParty\installed.txt
@set TMPCZIP=%TMPPROJ%-cmake.zip

@REM Setup for the BUILD
@set TMPJOB=%JOB_NAME%
@set TMPDST=%WORKSPACE%
@set TMPVER=%BUILD_NUMBER%

@REM Not used
@set TMPIPATH=%TMPINST%\include
@set TMPLPATH=%TMPINST%\lib

@REM Set OPTIONS, if any
@set TMPOPTS=
@REM set TMPOPTS=-DCMAKE_DEBUG_POSTFIX=d
@REM set TMPOPTS=%TMPOPTS% -DOPENAL_LIB_DIR:PATH=%TMPINST%\lib
@REM set TMPOPTS=%TMPOPTS% -DOPENAL_INCLUDE_DIR:PATH=%TMPINST%\include

@if "%TMPJOB%x" == "x" (
@set TMPJOB=%TMPJENK%
)

@if "%TMPDST%x" == "x" (
@set TMPDST=F:\Projects\workspace\%TMPJOB%
)

@if NOT EXIST %TMPDST%\nul (
@md %TMPDST%
@if ERRORLEVEL 1 goto ISERR
)

@REM Set the VERSION LOG file
@if "%TMPVER%x" == "x" goto GETVER
@set TMPLOG=%TMPDST%\bldlog-%TMPVER%.txt
@goto GOTVER
:GETVER
@set TMPVER=0
:RPT
@set /A TMPVER+=1
@set TMPLOG=%TMPDST%\bldlog-%TMPVER%.txt
@if EXIST %TMPLOG% goto RPT
:GOTVER
@set TMPCZIP=%TMPDST%\%TMPPROJ%-cmake-%TMPVER%.zip

@echo Bgn %DATE% %TIME% to %TMPLOG%
@REM Restart LOG
@echo. > %TMPLOG%
@echo Bgn %DATE% %TIME% >> %TMPLOG%
@REM if ERRORLEVEL 1 goto ISERR
@echo NUM 1: ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%

@echo Begin in folder %CD% >> %TMPLOG%

@REM get/update source
@if "%ADDUPD%x" == "1x" (
@call upd%TMPPRJ% NOPAUSE  >> %TMPLOG% 2>&1
@echo Done %TMPPRJ% source update >> %TMPLOG%
)

@REM Check we are in the right place, and can FIND the SOURCE
@if NOT EXIST %TMPSRC%\nul goto NOSRC

@REM if NOT EXIST %TMPBLD%\nul (
@REM md %TMPBLD%
@REM )
@REM if NOT EXIST %TMPBLD%\nul goto NODIR
@REM cd %TMPBLD%
@REM if ERRORLEVEL 1 goto ISERR
@echo Building in folder %CD% >> %TMPLOG%

@echo Deal with the CMakeLists.txt files >> %TMPLOG%
@if EXIST build-cmake.bat (
@echo Copy new cmake list files >> %TMPLOG%
@call build-cmake >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ISERR
) else (
@echo build-cmake.bat NOT found >> %TMPLOG%
)

@REM Check for primary CMakeLists.txt file
@if NOT EXIST %TMPSRC%\CMakeLists.txt goto NOSRC2

@if EXIST setup-jpeg12.bat (
@call setup-jpeg12 >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto JPEG12ERR
)

@REM All looks ok to proceed with BUILD
@REM echo Establish MSVC + SDK environment >> %TMPLOG%
@REM call set-msvc-sdk >> %TMPLOG%
@REM echo NUM 2: ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%

@echo Set more ENVIRONMENT, if any >> %TMPLOG%
@REM set OPENAL_LIB_DIR=%TMPINST%\lib
@REM echo Set ENVIRONMENT OPENAL_LIB_DIR=%OPENAL_LIB_DIR% >> %TMPLOG%
@REM set OPENAL_INCLUDE_DIR=%TMPINST%\include
@REM echo Set ENVIRONMENT OPENAL_INCLUDE_DIR=%OPENAL_INCLUDE_DIR% >> %TMPLOG%

@echo Do cmake configure and generation >> %TMPLOG%
@set TMPOPTS=%TMPOPTS% -DCMAKE_INSTALL_PREFIX:PATH=%TMPINST%

@echo Doing 'cmake %TMPSRC% %TMPOPTS%'  >> %TMPLOG%
cmake %TMPSRC% %TMPOPTS%  >> %TMPLOG% 2>&1
@echo Done 'cmake %TMPSRC% %TMPOPTS%'  ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@if ERRORLEVEL 1 goto NOCM

@if NOT "%ADDDBG%x" == "1x" (
@echo NOTE: Debug build is DISABLED! >> %TMPLOG%
@goto DONE
)

@echo Do cmake Debug build >> %TMPLOG%

@echo Doing: 'cmake --build . --config Debug' >> %TMPLOG%
cmake --build . --config Debug >> %TMPLOG%
@echo Done: 'cmake --build . --config Debug' ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@if ERRORLEVEL 1 goto NOBLDDBG

:DONE

@echo Do cmake Release build >> %TMPLOG%

@echo Doing: 'cmake --build . --config Release' >> %TMPLOG%
cmake --build . --config Release >> %TMPLOG% 2>&1
@echo Done: 'cmake --build . --config Release' ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@if ERRORLEVEL 1 goto NOBLDREL
:DONEREL

@if "%DOTINST%x" == "0x" goto DNTINST

@echo Install and generate the Debug ZIP in a temporary folder >> %TMPLOG%
@if EXIST build-zip.bat (
@echo Doing 'call build-zip Debug' to generate %TMPPROJ% zip >> %TMPLOG%
call build-zip Debug >> %TMPLOG%
@if ERRORLEVEL 1 goto ZIPERR
) else (
@echo Note build-zip.bat does NOT exist in folder %CD%, so NO Debug ZIP created! >> %TMPLOG%
)

@echo Install and generate the Release ZIP in a temporary folder >> %TMPLOG%
@if EXIST build-zip.bat (
@echo Doing 'call build-zip Release' to generate %TMPPROJ% zip >> %TMPLOG%
call build-zip Release >> %TMPLOG%
@if ERRORLEVEL 1 goto ZIPERR
) else (
@echo Note build-zip.bat does NOT exist in folder %CD%, so NO Release ZIP created! >> %TMPLOG%
)

:DNTINST

@if "%ADDINST%x" == "1x" (

@echo Continue with INSTALL? Ctrl+C to abort... other keys continue...
@%DOPAUSE%

@echo Doing: 'cmake --build . --config Debug --target INSTALL'
@echo Doing: 'cmake --build . --config Debug --target INSTALL' >> %TMPLOG%
cmake --build . --config Debug --target INSTALL >> %TMPLOG%
@echo Done: 'cmake --build . --config Debug --target INSTALL' ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@if ERRORLEVEL 1 goto NOINST

@echo. >> %TMPIFIL%
@echo = %TMPPROJ% Debug install %DATE% %TIME% >> %TMPIFIL%
@if EXIST install_manifest.txt (
type install_manifest.txt >> %TMPIFIL%
copy install_manifest.txt install-%TMPPROJ%-dbg.txt >nul
)

@echo Doing: 'cmake --build . --config Release --target INSTALL'
@echo Doing: 'cmake --build . --config Release --target INSTALL' >> %TMPLOG%
cmake --build . --config Release --target INSTALL >> %TMPLOG%  2>&1
@echo Done: 'cmake --build . --config Release --target INSTALL' ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@if ERRORLEVEL 1 goto NOINST

@echo. >> %TMPIFIL%
@echo = %TMPPROJ% Release install %DATE% %TIME% >> %TMPIFIL%
@echo Installed to %TMPINST% >> %TMPIFIL%
@if EXIST install_manifest.txt (
type install_manifest.txt >> %TMPIFIL%
copy install_manifest.txt install-%TMPPROJ%-rel.txt >nul
)
)

@echo Looks successful - keep the CMakeLists.txt, in '%TMPCZIP%'
@echo Looks successful - keep the CMakeLists.txt, in '%TMPCZIP%' >> %TMPLOG%
@if EXIST %TMPCZIP% del %TMPCZIP% >nul
@echo Doing: 'call zip8 -a -p -r -o %TMPCZIP% %TMPSRC%\CMakeLists.txt' >> %TMPLOG%
@call zip8 -a -p -r -o %TMPCZIP% %TMPSRC%\CMakeLists.txt >nul 2>&1
@if NOT EXIST %TMPCZIP% (
@echo WARNING: Appears ZIP of %TMPCZIP% failed, but considered non-critical! >> %TMPLOG%
)

@if NOT "%ADDINST%x" == "1x" (
@echo NOTE: Install to %TMPINST% is DISABLED >> %TMPLOG%
)
@call elapsed %TMPBGN% >> %TMPLOG%
@echo End %DATE% %TIME% ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@call elapsed %TMPBGN%
@echo End %DATE% %TIME% Success
@goto END

:JPEG12ERR
@echo Trouble with Jpeg12 creation?
@goto ISERR

:ZIPERR
@echo ERROR: build ZIP failed! returning ERRORLEVEL=%ERRORLEVEL%
@goto ISERR

:NOINST
@echo ERROR: CMake install error! ERRORLEVEL=%ERRORLEVEL%! >> %TMPLOG%
@goto ISERR

:NOBLDDBG
@echo ERROR: Debug build error! ERRORLEVEL=%ERRORLEVEL%! >> %TMPLOG%
@goto ISERR


:NOBLDREL
@fa4 "LINK : fatal error LNK1104:" %TMPLOG%
@if ERRORLEVEL 1 (
@echo Repeat release build due this STUPID STUPID STUPID error
@echo Repeat release build due this STUPID LINK error >> %TMPLOG%
cmake --build . --config Release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto NOBLDREL2
@goto DONEREL
)
:NOBLDREL2
@fa4 "mt.exe : general error c101008d:" %TMPLOG%
@if ERRORLEVEL 1 (
@echo Repeat release build due this STUPID STUPID STUPID error
@echo Repeat release build due this STUPID MT error >> %TMPLOG%
cmake --build . --config Release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto NOBLDREL3
@goto DONEREL
)
:NOBLDREL3
@echo ERROR: Release build error! ERRORLEVEL=%ERRORLEVEL%! >> %TMPLOG%
@goto ISERR

:NOCM
@echo ERROR: CMake configure and generation error! ERRORLEVEL=%ERRORLEVEL%! >> %TMPLOG%
@goto ISERR

:NOSRC
@echo ERROR: Could NOT locate SOURCE! %TMPSRC% >> %TMPLOG%
@goto ISERR

:NOSRC2
@echo ERROR: Could NOT locate SOURCE! %TMPSRC%\CMakeLists.txt >> %TMPLOG%
@goto ISERR

:NODIR
@echo ERROR: Could NOT create build directory %TMPBLD%! >> %TMPLOG%
@goto ISERR

:ISERR
@echo ERROREXIT! ERRORLEVEL=%ERRORLEVEL%! >> %TMPLOG%
@echo Error exit, see %TMPLOG%!
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
