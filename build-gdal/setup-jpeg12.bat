@setlocal
@set TMPSRC=F:\FG\18\gdal-1.9.1\frmts\jpeg\libjpeg12
@set TMPMORE=%TMPSRC%\jmorecfg.h.12
@set TMPMORE2=%TMPSRC%\jmorecfg.h
@if NOT EXIST %TMPSRC%\nul goto NOSRC
@if NOT EXIST %TMPMORE% goto NOSRC2
@if EXIST %TMPMORE2% goto ALLDONE
@cd %TMPSRC%
@if ERRORLEVEL 1 goto NOCD
xcopy /Y ..\libjpeg\*.h
@if ERRORLEVEL 1 goto GOTERR
xcopy /Y jmorecfg.h.12 jmorecfg.h
@if ERRORLEVEL 1 goto GOTERR
xcopy /Y ..\libjpeg\*.c
@if ERRORLEVEL 1 goto GOTERR
ren jcapimin.c jcapimin12.c
@if ERRORLEVEL 1 goto GOTERR
ren jcapistd.c jcapistd12.c
@if ERRORLEVEL 1 goto GOTERR
ren jccoefct.c jccoefct12.c
@if ERRORLEVEL 1 goto GOTERR
ren jccolor.c jccolor12.c
@if ERRORLEVEL 1 goto GOTERR
ren jcdctmgr.c jcdctmgr12.c
@if ERRORLEVEL 1 goto GOTERR
ren jchuff.c jchuff12.c
@if ERRORLEVEL 1 goto GOTERR
ren jcinit.c jcinit12.c
@if ERRORLEVEL 1 goto GOTERR
ren jcmainct.c jcmainct12.c
@if ERRORLEVEL 1 goto GOTERR
ren jcmarker.c jcmarker12.c
@if ERRORLEVEL 1 goto GOTERR
ren jcmaster.c jcmaster12.c
@if ERRORLEVEL 1 goto GOTERR
ren jcomapi.c jcomapi12.c
@if ERRORLEVEL 1 goto GOTERR
ren jcparam.c jcparam12.c
@if ERRORLEVEL 1 goto GOTERR
ren jcphuff.c jcphuff12.c
@if ERRORLEVEL 1 goto GOTERR
ren jcprepct.c jcprepct12.c
@if ERRORLEVEL 1 goto GOTERR
ren jcsample.c jcsample12.c
@if ERRORLEVEL 1 goto GOTERR
ren jctrans.c jctrans12.c
@if ERRORLEVEL 1 goto GOTERR
ren jdapimin.c jdapimin12.c
@if ERRORLEVEL 1 goto GOTERR
ren jdapistd.c jdapistd12.c
@if ERRORLEVEL 1 goto GOTERR
ren jdatadst.c jdatadst12.c
@if ERRORLEVEL 1 goto GOTERR
ren jdatasrc.c jdatasrc12.c
@if ERRORLEVEL 1 goto GOTERR
ren jdcoefct.c jdcoefct12.c
@if ERRORLEVEL 1 goto GOTERR
ren jdcolor.c jdcolor12.c
@if ERRORLEVEL 1 goto GOTERR
ren jddctmgr.c jddctmgr12.c
@if ERRORLEVEL 1 goto GOTERR
ren jdhuff.c jdhuff12.c
@if ERRORLEVEL 1 goto GOTERR
ren jdinput.c jdinput12.c
@if ERRORLEVEL 1 goto GOTERR
ren jdmainct.c jdmainct12.c
@if ERRORLEVEL 1 goto GOTERR
ren jdmarker.c jdmarker12.c
@if ERRORLEVEL 1 goto GOTERR
ren jdmaster.c jdmaster12.c
@if ERRORLEVEL 1 goto GOTERR
ren jdmerge.c jdmerge12.c
@if ERRORLEVEL 1 goto GOTERR
ren jdphuff.c jdphuff12.c
@if ERRORLEVEL 1 goto GOTERR
ren jdpostct.c jdpostct12.c
@if ERRORLEVEL 1 goto GOTERR
ren jdsample.c jdsample12.c
@if ERRORLEVEL 1 goto GOTERR
ren jdtrans.c jdtrans12.c
@if ERRORLEVEL 1 goto GOTERR
ren jerror.c jerror12.c
@if ERRORLEVEL 1 goto GOTERR
ren jfdctflt.c jfdctflt12.c
@if ERRORLEVEL 1 goto GOTERR
ren jfdctfst.c jfdctfst12.c
@if ERRORLEVEL 1 goto GOTERR
ren jfdctint.c jfdctint12.c
@if ERRORLEVEL 1 goto GOTERR
ren jidctflt.c jidctflt12.c
@if ERRORLEVEL 1 goto GOTERR
ren jidctfst.c jidctfst12.c
@if ERRORLEVEL 1 goto GOTERR
ren jidctint.c jidctint12.c
@if ERRORLEVEL 1 goto GOTERR
ren jidctred.c jidctred12.c
@if ERRORLEVEL 1 goto GOTERR
ren jmemansi.c jmemansi12.c
@if ERRORLEVEL 1 goto GOTERR
ren jmemmgr.c jmemmgr12.c
@if ERRORLEVEL 1 goto GOTERR
ren jquant1.c jquant112.c
@if ERRORLEVEL 1 goto GOTERR
ren jquant2.c jquant212.c
@if ERRORLEVEL 1 goto GOTERR
ren jutils.c jutils12.c
@if ERRORLEVEL 1 goto GOTERR
@goto END

:NOCD
@echo Error: Can NOT do 'cd %TMPSRC%'! *** FIX ME ***
@goto ISERR

:NOSRC
@echo ERROR: Can NOT locate source %TMPSRC%! *** FIX ME ***
@goto ISERR

:NOSRC2
@echo ERROR: Can NOT locate source %TMPMORE%! *** FIX ME ***
@goto ISERR

:GOTERR
:ISERR
@endlocal
@exit /b 1

:CLEAN
del *.c
del *.h
@goto END

:ALLDONE
@echo Appears libjpeg12 setup already done
:END
@endlocal
@exit /b 0

@REM eof
