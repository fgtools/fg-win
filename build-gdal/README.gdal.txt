file : README.gdal.txt 20140207 - 20130320 - 20121105 - 20120627

====================================================================================
20140206: After a HDD failure, had to RESTART building gdal 1.9.1
Wrote an in2cmake.pl to 'convert' a config.h.in type file to cmake
for CMakeLists.txt and a *config.h.cmake to *config.h
Good start, but still some things TODO ;=(( DONE!
First successful build and install to 3rdParty of gdal 1.9.1
TODO: Run some tests on the binaries, and library...
The DBG and REL ZIPS are in the F:\Projects\workspace/win32-gdal. Maybe 
these should be include as part of this repo? Decision to be made here.

====================================================================================
20140207: Found a cmake fork of gdal
from : https://gitorious.org/cmake-gdal
but last push was circa April 2011! The VERSION is 1.8.0
WOW, and it COMPILED with no ERRORS, only few WARNINGS ;=))
Decision on using this???!!!???
Hmmm...
1: Is multi-level cmake - not bad but hard to 'follow', debug...
2: Has several cmake modules, in cmake folder - that's ok too...
3: Builds a DLL - could accept that...
4: No separation of debug and release builds, but should be an easy fix.
5: INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include", whereas I believe it 
   should be include/gdal... again easy fix
6: Default ${CMAKE_INSTALL_PREFIX} is "C:/Program Files (x86)/GDAL"... another easy fix...

So it seems QUITE useable ;=))
BUT, since I now have successfully built and installed gdal 1.9.1, will let 
this go for now...
NOTED some more interesting cmake tests that need to be examined - TODO
Also like the configure steps that chose what formats, ogr to build. Looks 
very interesting.

====================================================================================

20130320 - Now being LINKED to terragear, but a number of HEADERS
were NOT found. I checked to simpits gdal win build and find many 
moe heades are 'installed' - see headers-sp.txt

Using the perl utility, findfiles, checked the c:\fg\17\gdal-1.9.1 
and while most were found, 3 or 4 were NOT! See hdrs-found.txt.
This seemed the SAME in the gdal-1.9.2 source, not built.

SO, update the CMakeLists.txt to use ALL the found headers.

The second thing is a LINK error -
Link:     Creating library C:/FG/17/build-tg/src/Utils/poly2ogr/Release/poly2ogr.lib 
     and object C:/FG/17/build-tg/src/Utils/poly2ogr/Release/poly2ogr.expgdal.lib(cpl_odbc.obj) : error LNK2019: unresolved external symbol _SQLInstallerError@20 
referenced in function "public: int __thiscall CPLODBCDriverInstaller::InstallDriver(
char const *,char const *,unsigned short)" 
This modules needs linkage to the odbc32.lib

So added, into port\cpl_odbc.cpp -
#ifdef _MSC_VER
#pragma comment ( lib, "odbc32.lib" ) /* force windows ODBC library */
#endif

===================================================================
20120627

The first build was using the supplied makefile.vc nmake.opt, after 
making a few addjustments in nmake.opt, ran, in source, in MSVC 
environment -
 gdal-1.9.1> nmake -f makefile.vc
and gdal19.dll was successfully built ;=))

But this was only the release, and no install tried, although there 
is an 'install' target... took about 10 minutes...

While a lot of MSVC cl warnings have been disabled, there were still 
a number of warnings...

Can I construct a CMakeLists.txt to do the SAME thing?
Hmmm, quite large list of sources... will try...

20121105:
This is a 2nd try in C:\FG\17...
Not sure 'where' the jpeg sources came from but copied them 
from C:\FG\16\gdal-1.9.1???

====================================================================
# eof






