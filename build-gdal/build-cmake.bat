@setlocal

@set TMPSRC=F:\FG\18\gdal-1.9.1
@set TMPFIL=CMakeLists.txt
@set TMPFIL2=cpl_config.h.cmake
@set TMPFIL3=cpl_vsil_win32.cpp
@set TMPFIL4=cpl_odbc.cpp
@set TMPSRC2=%TMPSRC%\port

@if NOT EXIST %TMPSRC%\nul goto NOSRC
@if NOT EXIST %TMPSRC2%\nul goto NOSRC2
@if NOT EXIST %TMPFIL% goto NOFIL
@if NOT EXIST %TMPFIL2% goto NOFIL2
@if NOT EXIST %TMPFIL3% goto NOFIL3
@if NOT EXIST %TMPFIL4% goto NOFIL4

@if NOT EXIST %TMPSRC%\%TMPFIL% goto DOCOPY
@fc4 -v0 -q %TMPFIL% %TMPSRC%\%TMPFIL%
@if ERRORLEVEL 1 goto DOCOPY
@call dirmin %TMPSRC%\%TMPFIL%
@echo Appears no change in %TMPFIL%... nothing done...
@goto DNCOPY
:DOCOPY
@call dirmin %TMPFIL%
@if EXIST %TMPSRC%\%TMPFIL% @call dirmin %TMPSRC%\%TMPFIL%
@echo Copying %TMPFIL% to %TMPSRC%
copy %TMPFIL% %TMPSRC%
@if NOT EXIST %TMPSRC%\%TMPFIL% goto NOCOPY
@call dirmin %TMPSRC%\%TMPFIL%
@echo Done copy of %TMPFIL% to %TMPSRC%
:DNCOPY

@REM Add any other 'created' files
@REM ==============================================
@if NOT EXIST %TMPSRC2%\%TMPFIL2% goto DOCOPY2
@fc4 -v0 -q %TMPFIL2% %TMPSRC2%\%TMPFIL2%
@if ERRORLEVEL 1 goto DOCOPY2
@call dirmin %TMPSRC2%\%TMPFIL2%
@echo Appears no change in %TMPFIL2%... nothing done...
@goto DNCOPY2
:DOCOPY2
@call dirmin %TMPFIL2%
@if EXIST %TMPSRC2%\%TMPFIL2% @call dirmin %TMPSRC2%\%TMPFIL2%
@echo Copying %TMPFIL2% to %TMPSRC2%
copy %TMPFIL2% %TMPSRC2%
@if NOT EXIST %TMPSRC2%\%TMPFIL2% goto NOCOPY2
@call dirmin %TMPSRC2%\%TMPFIL2%
@echo Done copy of %TMPFIL2% to %TMPSRC2%
:DNCOPY2

@REM ==============================================
@if NOT EXIST %TMPSRC2%\%TMPFIL3% goto DOCOPY3
@fc4 -v0 -q %TMPFIL3% %TMPSRC2%\%TMPFIL3%
@if ERRORLEVEL 1 goto DOCOPY3
@call dirmin %TMPSRC2%\%TMPFIL3%
@echo Appears no change in %TMPFIL3%... nothing done...
@goto DNCOPY3
:DOCOPY3
@call dirmin %TMPFIL3%
@if EXIST %TMPSRC2%\%TMPFIL3% @call dirmin %TMPSRC2%\%TMPFIL3%
@echo Copying %TMPFIL3% to %TMPSRC2%
copy %TMPFIL3% %TMPSRC2%
@if NOT EXIST %TMPSRC2%\%TMPFIL3% goto NOCOPY3
@call dirmin %TMPSRC2%\%TMPFIL3%
@echo Done copy of %TMPFIL3% to %TMPSRC2%
:DNCOPY3

@REM ==============================================
@if NOT EXIST %TMPSRC2%\%TMPFIL4% goto DOCOPY4
@fc4 -v0 -q %TMPFIL4% %TMPSRC2%\%TMPFIL4%
@if ERRORLEVEL 1 goto DOCOPY4
@call dirmin %TMPSRC2%\%TMPFIL4%
@echo Appears no change in %TMPFIL4%... nothing done...
@goto DNCOPY4
:DOCOPY4
@call dirmin %TMPFIL4%
@if EXIST %TMPSRC2%\%TMPFIL4% @call dirmin %TMPSRC2%\%TMPFIL4%
@echo Copying %TMPFIL4% to %TMPSRC2%
copy %TMPFIL4% %TMPSRC2%
@if NOT EXIST %TMPSRC2%\%TMPFIL4% goto NOCOPY4
@call dirmin %TMPSRC2%\%TMPFIL4%
@echo Done copy of %TMPFIL4% to %TMPSRC2%
:DNCOPY4

@goto END

:NOCOPY
@echo Copy of %TMPFIL% to %TMPSRC% FAILED!
@goto ISERR

:NOCOPY2
@echo Copy of %TMPFIL2% to %TMPSRC2% FAILED!
@goto ISERR

:NOCOPY3
@echo Copy of %TMPFIL3% to %TMPSRC2% FAILED!
@goto ISERR

:NOCOPY4
@echo Copy of %TMPFIL4% to %TMPSRC2% FAILED!
@goto ISERR

:NOSRC
@echo ERROR: Can NOT locate source [%TMPSRC%]!
@goto ISERR

:NOSRC2
@echo ERROR: Can NOT locate source [%TMPSRC2%]!
@goto ISERR

:NOFIL
@echo ERROR: Can NOT locate file [%TMPFIL%]!
@goto ISERR

:NOFIL2
@echo ERROR: Can NOT locate file [%TMPFIL2%]!
@goto ISERR

:NOFIL3
@echo ERROR: Can NOT locate file [%TMPFIL3%]!
@goto ISERR

:NOFIL4
@echo ERROR: Can NOT locate file [%TMPFIL4%]!
@goto ISERR

:ISERR
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
