@setlocal

@echo SOURCE: http://www.hdfgroup.org/doc_resource/SZIP/
@set TMPDIR=szip-2.1

@REM =============================================
@set TMPZIP=%TMPDIR%.zip
@set TMPSRC=zips\%TMPZIP%
@if NOT EXIST %TMPSRC% goto ERR1
:DOUNZIP
@if EXIST %TMPDIR%\nul goto ERR2

call unzip8 -d %TMPSRC%

@goto END

:ERR1
@if EXIST %DOWNLOADS%\%TMPZIP% (
copy %DOWNLOADS%\%TMPZIP% %TMPSRC%
@if EXIST %TMPSRC% goto DOUNZIP
)
@echo Can NOT locate %TMPSRC%
@goto END

:ERR2
@echo Directory exist %TMPDIR%
@call dirmin %TMPSRC%
@echo Is source from above zip... download later for update...
@goto END


:END
