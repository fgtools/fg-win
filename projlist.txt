Project list updated by showprojs.pl on 2014/12/03 20:55:41
Found 34 'build' directories...
Found 15 suitable source directories...
 1: FLTK           fltk-1.3.2           2012/09/13 14:19:02   21048 builds: build-fltk, build-fltk.x64
 2: libpng         lpng1514             2013/01/24 03:59:04   12085 builds: build-png, build-png.x64
 3: zlib           zlib-1.2.8           2013/04/28 14:57:10    8098 builds: build-zlib, build-zlib.x64
 4: OpenSceneGraph OpenSceneGraph-3.2.0 2013/07/24 15:11:55   50834 builds: build-osg, build-osg.x64
 5: freeglut       freeglut-3.0.0       2014/01/24 09:35:06   18961 builds: build-freeglut, build-freeglut.x64
 6: jpeg           jpeg-9               2014/05/16 12:27:19    5555 builds: build-jpeg, build-jpeg.x64
 7: plib           plib-1.8.5           2014/05/16 12:27:19   15791 builds: build-plib, build-plib.x64
 8: OpenAL         openal-soft          2014/05/16 15:18:35   38015 builds: build-openal, build-openal.x64
 9: gmp            gmp-6.0.0            2014/05/29 14:55:17   14520 builds: build-gmp
10: pthread        pthreads             2014/06/04 09:25:58    6246 builds: build-pthreads, build-pthreads.x64
11: glew           glew-1.10.0          2014/08/11 13:56:20    3071 builds: build-glew
12: Atlasg         atlas-g              2014/08/12 19:22:30   12154 builds: build-atlas-g
13: freetype       freetype-2.5.3       2014/08/14 16:25:51    5907 builds: build-ft, build-ft.x64
14: SimGear        simgear              2014/11/14 14:19:31   15061 builds: build-sg, build-sg.x64
15: FlightGear     flightgear           2014/11/30 20:22:20   15529 builds: build-fg, build-fg.x64

Unmatched 7 build dirs...
 1: boost_1_53_0   build-boost          2014/08/12 19:22:30   12154
 2: boost_1_53_0   build-boost.x64      2014/08/12 19:22:30   12154
 3: ..\fgx         build-fgx            2014/08/12 19:22:30   12154
 4: X:\gdal-1.9.1  build-gdal           2014/08/12 19:22:30   12154
 5: X:\terragear   build-tg             2014/08/12 19:22:30   12154
 6: X:\tiff-4.0.3  build-tiff           2014/08/12 19:22:30   12154
 7: X:\libxml2-2.9.1 build-xml2           2014/08/12 19:22:30   12154
