@setlocal

@set TMPZIP=zips\OpenSceneGraph-3.2.0.zip
@if NOT EXIST %TMPZIP% goto ERR1
@set TMPDIR=OpenSceneGraph-3.2.0
@if EXIST %TMPDIR%\nul goto DONE
:GOTSRC
@call unzip8 -d %TMPZIP%

@if NOT EXIST %TMPDIR%\nul ERR2

@goto END

:ERR2
@echo Folder %TMPDIR% NOT created... a problem with unzip?!?
@goto END

:ERR1
@set TMPSRC=%DOWNLOADS%\OpenSceneGraph-3.2.0.zip
@if NOT EXIST %TMPSRC% goto ERR11
copy %TMPSRC% %TMPZIP%
@if NOT EXIST %TMPZIP% goto ERR111
@goto GOTSRC
:ERR111
@echo Copy FAILED?
:ERR11
@echo Can NOT locate %TMPSRC%
@echo Can NOT locate %TMPZIP%
@goto END

:DONE
@call dirmin %TMPZIP%
@echo This is a ZIP source, so no update available...

@goto END

:END

