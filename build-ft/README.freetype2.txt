README.freetype2.txt - 20140814 - 20140207

Had to make one small change in the provided CMakeLists.txt, to 
SEPARATE Debug and Release build when using MSVC

So the CMakeLists.txt in this build-ft folder is used to 
OVERWRITE the original in ..\freetype-2.5.3 folder.

AND since I had to make that one small change, also 
removed some MSVC warnings...

Further the 'standard' FindFreeType.cmake looks for library names of
  NAMES freetype libfreetype freetype219

What does OSG do? Well it seems it has it OWN FindFreeType.cmake, BUT it uses the SAME names
FIND_LIBRARY(FREETYPE_LIBRARY 
  NAMES freetype libfreetype freetype219

So adjust the CMakeLists.txt to build 'freetype' library. NOT needed! That is the name used
  
# eof
