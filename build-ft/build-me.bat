@setlocal
@set TMPBGN=%TIME%

@set ADDINST=1
@set ADDDBG=1

@set TMPDIR=X:
@set TMPPROJ=freetype
@set TMPPRJ=ft
@set TMPVERS=-2.6.2
@REM set TMPVERS=-2.5.3
@set TMPMSVC=msvc100

@set TMPBASE=%TMPDIR%
@set TMPSRC=%TMPBASE%\%TMPPROJ%%TMPVERS%
@set TMPGEN=Visual Studio 10
@set TMPINST=%TMPBASE%\3rdParty

@REM Setup for the BUILD
@set TMPJOB=%JOB_NAME%
@set TMPDST=%WORKSPACE%
@set TMPVER=%BUILD_NUMBER%

@REM Not used
@set TMPIPATH=%TMPINST%\include
@set TMPLPATH=%TMPINST%\lib

@REM Set OPTIONS, if any
@set TMPOPTS=
@set TMPOPTS=%TMPOPTS% -DCMAKE_INSTALL_PREFIX:PATH=%TMPINST%

@set TMPLOG=bldlog-1.txt
@echo Bgn %DATE% %TIME% to %TMPLOG%
@REM Restart LOG
@echo. > %TMPLOG%
@echo Bgn %DATE% %TIME% >> %TMPLOG%

@echo Begin in folder %CD% >> %TMPLOG%

@REM Check we are in the right place, and can FIND the SOURCE
@if NOT EXIST %TMPSRC%\nul goto NOSRC

@echo Building in folder %CD% >> %TMPLOG%

@echo Deal with the CMakeLists.txt files >> %TMPLOG%
@if EXIST build-cmake.bat (
@echo Potetially copy new cmake list file >> %TMPLOG%
@call build-cmake >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ISERR
) else (
@echo build-cmake.bat NOT found >> %TMPLOG%
)

@REM Check for primary CMakeLists.txt file
@if NOT EXIST %TMPSRC%\CMakeLists.txt goto NOSRC2

@REM All looks ok to proceed with BUILD
@REM echo Establish MSVC + SDK environment >> %TMPLOG%
@REM call set-msvc-sdk >> %TMPLOG%
@REM echo NUM 2: ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%

@echo Set more ENVIRONMENT, if any >> %TMPLOG%
@REM set OPENAL_LIB_DIR=%TMPINST%\lib
@REM echo Set ENVIRONMENT OPENAL_LIB_DIR=%OPENAL_LIB_DIR% >> %TMPLOG%
@REM set OPENAL_INCLUDE_DIR=%TMPINST%\include
@REM echo Set ENVIRONMENT OPENAL_INCLUDE_DIR=%OPENAL_INCLUDE_DIR% >> %TMPLOG%

@echo Do cmake configure and generation >> %TMPLOG%

@echo Doing 'cmake %TMPSRC% %TMPOPTS%'
@echo Doing 'cmake %TMPSRC% %TMPOPTS%'  >> %TMPLOG%
@cmake %TMPSRC% %TMPOPTS%  >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto NOCM
@echo Done 'cmake %TMPSRC% %TMPOPTS%'  >> %TMPLOG%

@if NOT "%ADDDBG%x" == "1x" (
@echo NOTE: Debug build is DISABLED! >> %TMPLOG%
@goto DONEDBG
)
@echo Do cmake Debug build >> %TMPLOG%

@echo Doing: 'cmake --build . --config Debug'
@echo Doing: 'cmake --build . --config Debug' >> %TMPLOG%
@cmake --build . --config Debug >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto NOBLDDBG
@echo Done: 'cmake --build . --config Debug' >> %TMPLOG%

@if "%ADDINST%x" == "1x" (
@echo Doing: 'cmake --build . --config Debug --target INSTALL'
@echo Doing: 'cmake --build . --config Debug --target INSTALL' >> %TMPLOG%
@cmake --build . --config Debug --target INSTALL >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto NOINST
)
@echo Done: 'cmake --build . --config Debug --target INSTALL' >> %TMPLOG%

:DONEDBG

@echo Do cmake Release build >> %TMPLOG%

@echo Doing: 'cmake --build . --config Release'
@echo Doing: 'cmake --build . --config Release' >> %TMPLOG%
@cmake --build . --config Release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto NOBLDREL
:DONEREL
@echo Done: 'cmake --build . --config Release' >> %TMPLOG%

@if "%ADDINST%x" == "1x" (
@echo Doing: 'cmake --build . --config Release --target INSTALL'
@echo Doing: 'cmake --build . --config Release --target INSTALL' >> %TMPLOG%
@cmake --build . --config Release --target INSTALL >> %TMPLOG%  2>&1
@if ERRORLEVEL 1 goto NOINST
)
@echo Done: 'cmake --build . --config Release --target INSTALL' >> %TMPLOG%
@echo.
@echo Looks successful - see %TMPLOG% for details
@echo Looks successful >> %TMPLOG%

@if NOT "%ADDINST%x" == "1x" (
@echo NOTE: Install to %TMPINST% is DISABLED >> %TMPLOG%
)
@call elapsed %TMPBGN% >> %TMPLOG%
@echo End %DATE% %TIME% ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@call elapsed %TMPBGN%
@echo End %DATE% %TIME% Success
@goto END

:ZIPERR
@echo ERROR: build ZIP failed! returning ERRORLEVEL=%ERRORLEVEL%
@goto ISERR

:NOINST
@echo ERROR: CMake install error! ERRORLEVEL=%ERRORLEVEL%! >> %TMPLOG%
@goto ISERR

:NOBLDDBG
@echo ERROR: Debug build error! ERRORLEVEL=%ERRORLEVEL%! >> %TMPLOG%
@goto ISERR

:NOBLDREL
fa4 "LINK : fatal error LNK1104:" %TMPLOG%
@if ERRORLEVEL 1 (
@echo Repeat release build due this STUPID STUPID STUPID error
cmake --build . --config Release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto NOBLDREL2
@goto DONEREL
)
:NOBLDREL2
@echo ERROR: Release build error! ERRORLEVEL=%ERRORLEVEL%! >> %TMPLOG%
@goto ISERR

:NOCM
@echo ERROR: CMake configure and generation error! ERRORLEVEL=%ERRORLEVEL%! >> %TMPLOG%
@goto ISERR

:NOSRC
@echo ERROR: Could NOT locate SOURCE! %TMPSRC% >> %TMPLOG%
@goto ISERR

:NOSRC2
@echo ERROR: Could NOT locate SOURCE! %TMPSRC%\CMakeLists.txt >> %TMPLOG%
@goto ISERR

:ISERR
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
