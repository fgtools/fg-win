@echo Update FG next...
@echo https://sourceforge.net/p/flightgear/flightgear/ci/next/tree/
@setlocal
@REM 20150325 - Update to use SF repo
@set REPO=git://git.code.sf.net/p/flightgear/flightgear
@REM set REPO=git://gitorious.org/fg/flightgear.git
@set TMPDIR=flightgear
@set DOPAUSE=pause
@if "%~1x" == "NOPAUSEx" @set DOPAUSE=echo NO PAUSE requested

@if NOT EXIST %TMPDIR%\. goto CHECKOUT

@echo Doing update
@cd %TMPDIR%
@call git status
@echo *** CONTINUE? ***
@%DOPAUSE%

git pull origin next
@cd ..

@goto END

:CHECKOUT
@echo WARNING: Folder %TMPDIR% does NOT exist!
@echo This is a NEW CLONE
@echo Will do: 'git clone %REPO% %TMPDIR%'
@echo *** CONTINUE? *** Only Ctrl+c to abort... all other keys continue...
@%DOPAUSE%

@echo Doing: 'git clone %REPO% %TMPDIR%'

git clone %REPO% %TMPDIR%

@if NOT EXIST %TMPDIR%\nul goto FAILED
@echo Done fresh checkout...

@goto END

:FAILED
@echo ERROR: git clone FAILED! No folder %TMPDIR% created...
@goto END

:END
@endlocal
