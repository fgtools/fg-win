@setlocal
@set TMPDIR=mapnik
@set TMPREPO=https://github.com/mapnik/mapnik.git
@if EXIST %TMPDIR%\nul goto UPDATE
@echo This is a new clone to %TMPDIR%
@echo Will do 'call git clone %TMPREPO% %TMPDIR%'
@echo *** CONTINUE? *** Only Ctlr+C aborts...

call git clone %TMPREPO% %TMPDIR%

@goto END

:UPDATE
@echo This is an update of %TMPDIR%
@cd %TMPDIR%
@call git status
@echo *** CONTINUE? ***  Only Ctlr+C aborts...
@pause

call git pull

@goto END

:END
