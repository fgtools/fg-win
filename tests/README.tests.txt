README.tests.txt - 20141130

Place for various 'test' modules...

*) freetype folder

Finding 'FreeType'
This was failing in the OpenScecneGraph build, so added a freetype 'test',
with a CMakeLists.txt and a CMakeMOdules/FindFreeType.cmake module.
When I got this working, copied the 'find module to build-osg,
which will then be updated in the OpenSceneGraph-3.2.0 source.


*) gensrc folder

Generating an exe to generate a source

Experiment with using cmake to build an EXE, which is then 'run' to 
generate a source file. Seems to work quite well.


; eof
