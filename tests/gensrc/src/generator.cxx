/*\
 * generator.cxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#include <stdio.h>
#include <string>
#include "generator.hxx"

static const char *module = "generator";

/* generated table */
static char a_table[] = {'a','b'};

static const char *table = "\n/* generated table */\n"
    "char table[] = {'a','b', 0 };\n"
    "/* eof */\n";

// main() OS entry
int main( int argc, char **argv )
{
    int iret = 0;
    std::string file = argv[1];
    if (argc < 2) {
        printf("%s: Error: Need to give output file name!\n", module );
        return 1;
    }
    file += ".cxx";
    FILE *fp = fopen(file.c_str(),"w");
    if (!fp) {
        printf("%s: Error: Failed to create file '%s'!\n", module, file.c_str());
        return 1;
    }
    size_t res, len = strlen(table);
    res = fwrite(table,1,len,fp);
    fclose(fp);
    if (res != len) {
        printf("%s: Error: Failed to write file '%s'!\n", module, file.c_str());
        return 1;
    }
    return iret;
}


// eof = generator.cxx
