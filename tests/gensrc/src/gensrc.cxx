/*\
 * gensrc.cxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#include <stdio.h>
// other includes
#include "gensrc.hxx"

extern char table[];

static const char *module = "gensrc";

// main() OS entry
int main( int argc, char **argv )
{
    int iret = 0;
    int i;
    printf("%s: contents of generated table...\n", module);
    for (i = 0;  ; i++) {
        if (table[i] == 0)
            break;
        printf("%c ", table[i]);
    }
    printf("\n");
    return iret;
}


// eof = gensrc.cxx
