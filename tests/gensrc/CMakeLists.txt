# gensrc project - 20141130
# cmake experimenting with generating a source file
# inspiration from : http://www.cmake.org/Wiki/CMake_FAQ#How_do_I_generate_an_executable.2C_then_use_the_executable_to_generate_a_file.3F
# Here a 'generator' EXE is built, and run to create the 'table.cxx' source
# CMakeLists.txt, generated gencmake.pl, on 2014/11/30 18:08:53
cmake_minimum_required( VERSION 2.8.8 )

# CMakeScripts or use the ones that come by default with CMake.
# set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/CMakeModules ${CMAKE_MODULE_PATH})

project( gensrc )

# The version number.
set( gensrc_MAJOR 3 )
set( gensrc_MINOR 0 )
set( gensrc_POINT 0 )


# Allow developer to select is Dynamic or static library built
set( LIB_TYPE STATIC )  # set default static
option( BUILD_SHARED_LIB "Build Shared Library" OFF )

if(CMAKE_COMPILER_IS_GNUCXX)
    set( WARNING_FLAGS -Wall )
endif(CMAKE_COMPILER_IS_GNUCXX)

if (CMAKE_CXX_COMPILER_ID STREQUAL "Clang") 
   set( WARNING_FLAGS "-Wall -Wno-overloaded-virtual" )
endif() 

if(WIN32 AND MSVC)
    # turn off various warnings - none needed in this compile
    set(WARNING_FLAGS "${WARNING_FLAGS} /wd4996")
    # foreach(warning 4244 4251 4267 4275 4290 4786 4305)
    #     set(WARNING_FLAGS "${WARNING_FLAGS} /wd${warning}")
    # endforeach(warning)
    set( MSVC_FLAGS "-DNOMINMAX -D_USE_MATH_DEFINES -D_CRT_SECURE_NO_WARNINGS -D_SCL_SECURE_NO_WARNINGS -D__CRT_NONSTDC_NO_WARNINGS" )
    # if (${MSVC_VERSION} EQUAL 1600)
    #    set( MSVC_LD_FLAGS "/FORCE:MULTIPLE" )
    # endif (${MSVC_VERSION} EQUAL 1600)
    #set( NOMINMAX 1 )
else()
    # items for unix
endif()

set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${WARNING_FLAGS} ${MSVC_FLAGS} -D_REENTRANT" )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${WARNING_FLAGS} ${MSVC_FLAGS} -D_REENTRANT" )
set( CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${MSVC_LD_FLAGS}" )

# configuration file, if needed
# configure_file( ${CMAKE_SOURCE_DIR}/config.h.cmake ${CMAKE_BINARY_DIR}/config.h )
# add_definitions( -DHAVE_CONFIG_H )
# include_directories( ${CMAKE_BINARY_DIR} )

# to distinguish between debug and release lib
if (MSVC)
    set( CMAKE_DEBUG_POSTFIX "d" )
endif ()

if(BUILD_SHARED_LIB)
   set(LIB_TYPE SHARED)
   message(STATUS "*** Building DLL library ${LIB_TYPE}")
else()
   message(STATUS "*** Option BUILD_SHARED_LIB is OFF ${LIB_TYPE}")
endif()

# src 2 EXECUTABLES from [X:\tests\gensrc\src],
# 1: build the generator
set(name generator)
set(dir src)
set(${name}_SRCS
    ${dir}/generator.cxx
    )
set(${name}_HDRS
    ${dir}/generator.hxx
    )
add_executable( ${name} ${${name}_SRCS} ${${name}_HDRS} )
if (add_LIBS)
    target_link_libraries( ${name} ${add_LIBS} )
endif ()
# NOTE: Can not add a debug postfix, since the GET_TARGET_PROPERTY only gets 'Debug/generator.exe'
###if (MSVC)
###    set_target_properties( ${name} PROPERTIES DEBUG_POSTFIX d )
###endif ()
# deal with install, if any...
#install( TARGETS ${name} DESTINATION bin )

# 2: run the 'generator' to create the 'table.cxx' source
# Note the above problem if a DEBUG_POSTFIX is added
GET_TARGET_PROPERTY(GENERATE_EXE ${name} LOCATION)
ADD_CUSTOM_COMMAND(
   OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/table.cxx
   COMMAND ${GENERATE_EXE} ${CMAKE_CURRENT_BINARY_DIR}/table
   DEPENDS generate
   )

# 3: Create an EXE that links with the generated source
# Seems cmake does not 'look' for this source until the generation is done...
set(name gensrc)
set(dir src)
set(${name}_SRCS
    ${dir}/gensrc.cxx
    ${CMAKE_CURRENT_BINARY_DIR}/table.cxx
    )
set(${name}_HDRS
    ${dir}/gensrc.hxx
    )
add_executable( ${name} ${${name}_SRCS} ${${name}_HDRS} )
if (add_LIBS)
    target_link_libraries( ${name} ${add_LIBS} )
endif ()
if (MSVC)
    set_target_properties( ${name} PROPERTIES DEBUG_POSTFIX d )
endif ()
# deal with install, if any...
#install( TARGETS ${name} DESTINATION bin )

# eof
