
# CMakeLists.txt, generated gencmake.pl, on 2014/11/30 14:38:52

cmake_minimum_required( VERSION 2.6 )

project( test-ft2 )

# CMakeScripts or use the ones that come by default with CMake.
set( CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/CMakeModules ${CMAKE_MODULE_PATH} )
message(STATUS "*** Set CMAKE_MODULE_PATH=${CMAKE_MODULE_PATH}")

# The version number.
set( freetype_MAJOR 3 )
set( freetype_MINOR 0 )
set( freetype_POINT 0 )

# Allow developer to select is Dynamic or static library built
set( LIB_TYPE STATIC )  # set default static
option( BUILD_SHARED_LIB "Build Shared Library" OFF )
option( USE_DBGREL_LIBS "Set ON to link with separate DBG/REL libs in MSVC" ON )

if(CMAKE_COMPILER_IS_GNUCXX)
    set( WARNING_FLAGS -Wall )
endif(CMAKE_COMPILER_IS_GNUCXX)

if (CMAKE_CXX_COMPILER_ID STREQUAL "Clang") 
   set( WARNING_FLAGS "-Wall -Wno-overloaded-virtual" )
endif() 

if(WIN32 AND MSVC)
    # turn off various warnings - none needed in this compile
    set(WARNING_FLAGS "${WARNING_FLAGS} /wd4996")
    # foreach(warning 4244 4251 4267 4275 4290 4786 4305)
    #     set(WARNING_FLAGS "${WARNING_FLAGS} /wd${warning}")
    # endforeach(warning)
    set( MSVC_FLAGS "-DNOMINMAX -D_USE_MATH_DEFINES -D_CRT_SECURE_NO_WARNINGS -D_SCL_SECURE_NO_WARNINGS -D__CRT_NONSTDC_NO_WARNINGS" )
else()
    # items for linux
endif()

set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${WARNING_FLAGS} ${MSVC_FLAGS} -D_REENTRANT" )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${WARNING_FLAGS} ${MSVC_FLAGS} -D_REENTRANT" )
set( CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${MSVC_LD_FLAGS}" )

#  FREETYPE_LIBRARY, the library to link against
#  FREETYPE_FOUND, if false, do not try to link to FREETYPE
#  FREETYPE_INCLUDE_DIRS, where to find headers.
#  This is the concatenation of the paths:
#  FREETYPE_INCLUDE_DIR_ft2build
#  FREETYPE_INCLUDE_DIR_freetype2
# NOTE: Above option USE_DBGREL_LIBS to seek debug/optimized libs
# How to alter Debug (CMAKE_C_FLAGS_DEBUG or CMAKE_CXX_FLAGS_DEBUG) to include symbols???
find_package( FreeType )
if (FREETYPE_FOUND)
    message(STATUS "*** FreeType found inc ${FREETYPE_INCLUDE_DIRS} lib ${FREETYPE_LIBRARY}")
    list(APPEND add_LIBS ${FREETYPE_LIBRARY})
    include_directories( ${FREETYPE_INCLUDE_DIRS} )
    add_definitions( -DUSE_FREETYPE=1 )
else ()
    message(STATUS "*** FreeType NOT FOUND!")
endif ()

# configuration file, if needed
# configure_file( ${CMAKE_SOURCE_DIR}/config.h.cmake ${CMAKE_BINARY_DIR}/config.h )
# add_definitions( -DHAVE_CONFIG_H )
# include_directories( ${CMAKE_BINARY_DIR} )

# to distinguish between debug and release lib
if (MSVC)
    set( CMAKE_DEBUG_POSTFIX "d" )
endif ()

if(BUILD_SHARED_LIB)
   set(LIB_TYPE SHARED)
   message(STATUS "*** Building DLL library ${LIB_TYPE}")
else(BUILD_SHARED_LIB)
   message(STATUS "*** Option BUILD_SHARED_LIB is OFF ${LIB_TYPE}")
endif(BUILD_SHARED_LIB)

# src EXECUTABLE from [X:\tests\freetype\src],
# have 1 C/C++ sources, 1 headers
set(name test-ft2)
set(dir src)
set(${name}_SRCS
    ${dir}/test-ft2.cxx
    )
set(${name}_HDRS
    ${dir}/test-ft2.hxx
    )
add_executable( ${name} ${${name}_SRCS} ${${name}_HDRS} )
if (add_LIBS)
    target_link_libraries( ${name} ${add_LIBS} )
endif ()
if (MSVC)
    set_target_properties( ${name} PROPERTIES DEBUG_POSTFIX d )
endif ()
# deal with install, if any...
#install( TARGETS ${name} DESTINATION bin )

# eof
