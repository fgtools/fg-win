/*\
 * test-ft2.cxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
// other includes
#ifdef USE_FREETYPE
#include <ft2build.h>
#include FT_FREETYPE_H
#include <freetype.h>
#ifdef HAVE_AFM_FONT
#include FT_INTERNAL_STREAM_H
#include FT_INTERNAL_POSTSCRIPT_AUX_H
#endif
#endif // USE_FREETYPE
#include "test-ft2.hxx"

#ifdef _MSC_VER
#define M_IS_DIR _S_IFDIR
#else // !_MSC_VER
#define M_IS_DIR S_IFDIR
#endif

static const char *module = "test-ft2";

#ifdef USE_FREETYPE
FT_Library       library;
#ifdef HAVE_AFM_FONT
static void dump_fontinfo( AFM_FontInfo  fi )
{
    FT_Int  i;


    printf( "This AFM is for %sCID font.\n\n",
            ( fi->IsCIDFont ) ? "" : "non-" );

    printf( "FontBBox: %.2f %.2f %.2f %.2f\n", fi->FontBBox.xMin / 65536.,
                                               fi->FontBBox.yMin / 65536.,
                                               fi->FontBBox.xMax / 65536.,
                                               fi->FontBBox.yMax / 65536. );
    printf( "Ascender: %.2f\n", fi->Ascender / 65536. );
    printf( "Descender: %.2f\n\n", fi->Descender / 65536. );

    if ( fi->NumTrackKern )
      printf( "There are %d sets of track kernings:\n",
              fi->NumTrackKern );
    else
      printf( "There is no track kerning.\n" );

    for ( i = 0; i < fi->NumTrackKern; i++ )
    {
      AFM_TrackKern  tk = fi->TrackKerns + i;


      printf( "\t%2d: %5.2f %5.2f %5.2f %5.2f\n", tk->degree,
                                                  tk->min_ptsize / 65536.,
                                                  tk->min_kern / 65536.,
                                                  tk->max_ptsize / 65536.,
                                                  tk->max_kern / 65536. );
    }

    printf( "\n" );

    if ( fi->NumKernPair )
      printf( "There are %d kerning pairs:\n",
              fi->NumKernPair );
    else
      printf( "There is no kerning pair.\n" );

    for ( i = 0; i < fi->NumKernPair; i++ )
    {
      AFM_KernPair  kp = fi->KernPairs + i;


      printf( "\t%3d + %3d => (%4d, %4d)\n", kp->index1,
                                             kp->index2,
                                             kp->x,
                                             kp->y );
    }

}
#endif

#endif // USE_FREETYPE
#define DT_NONE 0
#define DT_FILE 1
#define DT_DIR  2

static struct stat buf;
int is_file_or_directory ( const char * path )
{
    if (!path)
        return DT_NONE;
	if (stat(path,&buf) == 0)
	{
		if (buf.st_mode & M_IS_DIR)
			return DT_DIR;
		else
			return DT_FILE;
	}
	return DT_NONE;
}

// main() OS entry
int main( int argc, char **argv )
{
    int iret = 0;
#ifdef USE_FREETYPE
    FT_Error    error = FT_Err_Ok;
    FT_Int      amajor,aminor,apatch;
    printf("%s: Found and linked with FreeType library.\n",module);
    error = FT_Init_FreeType( &library );
    if ( error ) {
        printf("%s: FT_Init_FreeType( &library ); FAILED %d\n", module, error );
        return error;
    }
    FT_Library_Version( library, &amajor, &aminor, &apatch );
    printf("%s: FreeType Library Version %d,%d,%d\n", module, amajor, aminor, apatch );
    if (argc > 1) {
        char *file = argv[1];
        if (is_file_or_directory(file) == DT_FILE) {
            // from : http://www.freetype.org/freetype2/docs/tutorial/step1.html
            // file like : "/usr/share/fonts/truetype/arial.ttf"
            // X:\fgdata\Fonts\LED-16.ttf
            FT_Face     face;      /* handle to face object */
            error = FT_New_Face( library,
                       file,
                       0,
                       &face );
            if ( error == FT_Err_Unknown_File_Format ) {
                printf("%s: The font file '%s', could be opened and read,\nbut it appears that its font format is unsupported!\n",
                    module, file );
            } else if ( error ) {
                printf("%s: The font file '%s', could not be opened or read,\nor simply that it is broken!\n",
                    module, file );
            } else {
                printf("%s: The font file '%s' was successfully loaded.\n",
                    module, file );
                // the character size is set to 16pt for a 300�300dpi device:
                error = FT_Set_Char_Size(
                    face,    /* handle to face object           */
                    0,       /* char_width in 1/64th of points  */
                    16*64,   /* char_height in 1/64th of points */
                    300,     /* horizontal device resolution    */
                    300 );   /* vertical device resolution      */
                if (error) {
                    printf("%s: And error %d occurred setting the char size to 16pt for a 300�300dpi device!\n",
                        module, error );
                } else {

                }

    
            }
        } else {
            printf("%s: Can NOT 'stat' file '%s'!\n", module, file );
        }
    }


    FT_Done_FreeType( library );
#else
    printf("%s: FreeType library NOT found!\n",module);
#endif // USE_FREETYPE
    return iret;
}


// eof = test-ft2.cxx
