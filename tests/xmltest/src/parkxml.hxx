/*\
 * parkxml.hxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#ifndef _PARKXML_HXX_
#define _PARKXML_HXX_
#include <map>
#include <string>
#include <vector>

// these are guesses at this time
#define MX_TYPE 32
#define MX_NAME 32
#define MX_CODES 128

#define ppf_index  0x00000001   // index="0"
#define ppf_type   0x00000002   // type="gate"
#define ppf_name   0x00000004   // name="D55"
#define ppf_number 0x00000008   // number="1"
#define ppf_lat    0x00000010   // lat="N37 37.098"
#define ppf_lon    0x00000020   // lon="W122 22.851"
#define ppf_hdg    0x00000040   // heading="250.708"
#define ppf_rad    0x00000080   // radius="40.3211"
#define ppf_pbr    0x00000100   // pushBackRoute="506" 
#define ppf_code   0x00000200   // airlineCodes="JBU,SWA,VRD"
#define ppf_unk    0x00008000   // unknown

#define ppf_min (ppf_lat | ppf_lon | ppf_hdg)

typedef struct tagPARKPOS {
    int flag;
    int index;  // index="0"
    char type[MX_TYPE]; // type="gate"
    char name[MX_NAME]; // name="D55"
    int number; // number="1"
    double lat; // lat="N37 37.098"
    double lon; // lon="W122 22.851"
    double hdg; // heading="250.708"
    double rad; // radius="40.3211"
    int pbr;    // pushBackRoute="506" 
    char code[MX_CODES]; // airlineCodes="JBU,SWA,VRD" />
}PARKPOS, * PPARKPOS;

typedef std::map<std::string,int> mSTGINT;
typedef mSTGINT::iterator iSTGINT;


#endif // #ifndef _PARKXML_HXX_
// eof - parkxml.hxx
