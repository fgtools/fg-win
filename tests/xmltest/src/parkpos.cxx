/*\
 * parkpos.cxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#include <stdio.h>
#include <string.h> // for strdup(), ...
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <math.h>
#include <sstream>
#include <map>
#include <simgear/xml/easyxml.hxx>
#include <simgear/constants.h>
#include <simgear/sg_inlines.h>
#include <simgear/math/SGMath.hxx>
#include <simgear/math/sg_geodesy.hxx>

#ifdef _MSC_VER
#include <Windows.h>
#endif
#include "utils/sprtf.hxx"
#include "utils/utils.hxx"
#include "parkxml.hxx"

static const char *module = "parkpos";

static const char *usr_input = 0;
static const char *xg_output = 0;
static bool add_names = false;
static bool at_head = false;    // true;
static bool add_info = false;
static int verbosity = 0;

#define VERB1 (verbosity >= 1)
#define VERB2 (verbosity >= 2)
#define VERB5 (verbosity >= 5)
#define VERB9 (verbosity >= 9)


#define MEOL std::endl
#define OUTPUT std::cout
#define ERROUT std::cerr

/* =======================================================
    And comparing the KSFO xml with the info in apt.dat.gz
   ======================================================= */
typedef struct tagAPTDATPP {
    int rec;
    double lat,lon,hdg;
    const char *type;
    const char *name;
}APTDATPP, * PAPTDATPP;

APTDATPP sAptDatPP[] = {
    { 15,   37.62912825, -122.38192148, 297.40, "GA", "1" },
    { 15,   37.61376700, -122.38959748, 113.00, "Gate", "A2" },
    { 15,   37.61303808, -122.38548715,  75.33, "Gate", "B24" },
    { 15,   37.61576612, -122.38317879, 222.03, "Gate", "C41" },
    { 15,   37.61427835, -122.38272621,  28.97, "Gate", "C44" },
    { 15,   37.61848370, -122.38387052, 305.34, "Gate", "E62" },
    { 15,   37.61951876, -122.38481589, 121.39, "Gate", "E63" },
    { 15,   37.61960825, -122.38834381,  26.00, "Gate", "F81" },
    { 15,   37.61798593, -122.39031718, 208.00, "Gate", "G94" },
    { 15,   37.61762250, -122.39252890,  28.00, "Gate", "G95" },
    // last always
    { 0,    0,            0,             0,     0,       0    }
};

//typedef std::map<std::string,int> mSTGINT;
//typedef mSTGINT::iterator iSTGINT;
static mSTGINT *pms = 0;    // new mSTGINT;

void add_to_names( const char* name ) 
{
    iSTGINT ii;
    std::string s,s2;
    int cnt;
    bool fnd = false;
    s = name;
    fnd = false;
    for (ii = pms->begin(); ii != pms->end(); ii++) {
        cnt = (*ii).second;
        s2  = (*ii).first;
        if (s == s2) {
            fnd = true;
            cnt++;
            (*ii).second = cnt;
            break;
        }
    }
    if (!fnd) {
       (*pms)[s] = 1;
    }
}


typedef std::vector<PARKPOS> vPKPOS;

typedef struct tagBBOX {
    double min_lat, min_lon, max_lat, max_lon;
}BBOX, * PBBOX;

void set_bbox( PBBOX pbb )
{
    pbb->min_lat = 400;
    pbb->max_lat = -400;
    pbb->min_lon = 400;
    pbb->max_lon = -400;
}

void add_bbox( PBBOX pbb, double lat, double lon )
{
    if (lat > pbb->max_lat)
        pbb->max_lat = lat;
    if (lat < pbb->min_lat)
        pbb->min_lat = lat;
    if (lon > pbb->max_lon)
        pbb->max_lon = lon;
    if (lon < pbb->min_lon)
        pbb->min_lon = lon;
}


#ifndef IS_DIGIT
#define IS_DIGIT(a) ((a >= '0')&&(a <= '9'))
#endif

int sg_geo_direct_wgs_84 ( double lat1, double lon1, double az1, double s, 
                            double *lat2, double *lon2, double *az2 )
{
    SGGeod geod1,geod2;
    geod1.setLatitudeDeg(lat1);
    geod1.setLongitudeDeg(lon1);
    geod1.setElevationM(0.0);
    int res = geo_direct_wgs_84( geod1, az1, s, 
        geod2, az2 );
    *lat2 = geod2.getLatitudeDeg();
    *lon2 = geod2.getLongitudeDeg();
    return res;
}

static const char *box_color = "blue";
static const char *hdg_color = "white";

std::string get_rectangle( double clat, double clon, double hdg, double rad, PBBOX pbb, PPARKPOS ppp )
{
    int res;
    double lat1, lon1, az1;
    double lat2, lon2, az2;
    double hrad = rad / 2.0;
    double rhdg = hdg + 180.0;
    if (rhdg >= 360.0)
        rhdg -= 360.0;
    // get tow ends using heading and reciprocal
    res = sg_geo_direct_wgs_84( clat, clon,  hdg, hrad, &lat1, &lon1, &az1 );
    res = sg_geo_direct_wgs_84( clat, clon, rhdg, hrad, &lat2, &lon2, &az2 );
    // setup heading to 'right' and 'left'
    double hdg1 = hdg + 90.0;
    if (hdg1 >= 360.0)
        hdg1 -= 360.0;
    double hdg2 = hdg - 90.0;
    if (hdg2 < 0.0)
        hdg2 += 360.0;
    double lata, lona, aza;
    double latb, lonb, azb;
    double latc, lonc, azc;
    double latd, lond, azd;
    // generate the four corners
    res = sg_geo_direct_wgs_84( lat1, lon1, hdg1, hrad, &lata, &lona, &aza );
    res = sg_geo_direct_wgs_84( lat1, lon1, hdg2, hrad, &latb, &lonb, &azb );
    res = sg_geo_direct_wgs_84( lat2, lon2, hdg1, hrad, &latd, &lond, &azd );
    res = sg_geo_direct_wgs_84( lat2, lon2, hdg2, hrad, &latc, &lonc, &azc );
    // fill out the boundary
    add_bbox( pbb, lata, lona );
    add_bbox( pbb, latb, lonb );
    add_bbox( pbb, latc, lonc );
    add_bbox( pbb, latd, lond );
    add_bbox( pbb, lat1, lon1 );
    add_bbox( pbb, lat2, lon2 );

    // generate the Xgraph stream
    std::stringstream xg;
    // *******************************************************************
    // seems default precision is quite small - with very BAD results
    xg << std::setprecision(12);
    // *******************************************************************
    if (add_names) {
        if (at_head) {
            xg << "anno " << lon1 << " " << lat1;
        } else {
            xg << "anno " << clon << " " << clat;
        }
        if ((ppp->flag & ppf_name) && ppp->name[0] ) {
            xg << " " << ppp->name;
            if (ppp->flag & ppf_number)
                xg << ppp->number;
        } else {
            xg << " No_Name";
        }
        if (add_info) {
            xg << " r=" << rad << " h=" << hdg;
        }
        xg << MEOL;
    }
    xg << "color " << box_color << MEOL;
    xg << lona << " " << lata << MEOL;
    xg << lonb << " " << latb << MEOL;
    xg << lonc << " " << latc << MEOL;
    xg << lond << " " << latd << MEOL;
    xg << lona << " " << lata << MEOL;
    xg << "NEXT" << MEOL;
    xg << "color " << hdg_color << MEOL;
    xg << lon1 << " " << lat1 << MEOL;
    xg << lon2 << " " << lat2 << MEOL;
    xg << "NEXT" << MEOL;
    return xg.str();
}

// lat="37 37.098"
// ll=0 = lat, ll=1 = lon
bool get_deg_val( const char *cp, double *plat, bool neg, int ll )
{
    char *bgn = (char *)cp;
    int len = 0;
    while(IS_DIGIT(*cp)) {
        len++;
        cp++;
    }
    if (len == 0) {
        SPRTF("%s: %s value '%s', does NOT have size!\n", module, 
            ( ll ? "lon" : "lat"),
            cp );
        return false;
    }
    double deg = atof(bgn);
    len = 0;
    while(*cp && !IS_DIGIT(*cp)) {
        len++;
        cp++;
    }
    if (*cp) {
        double mins = atof(cp);
        deg += (mins / 60.0);
    }
    if (neg)
        deg *= -1.0;
    if (ll) {
        // lon
        if ((deg < -180.0) || (deg > 180.0)) {
            SPRTF("%s: Warning: lon %lf OUT OF RANGE!\n", module, deg);
            return false;
        }
    } else {
        // lat
        if ((deg < -90.0) || (deg > 90.0)) {
            SPRTF("%s: Warning: lat %lf OUT OF RANGE!\n", module, deg);
            return false;
        }
    }
    *plat = deg;
    return true;
}

bool get_lat_val( const char *av, double *plat )
{
    char *cp = GetNxtBuf();
    strcpy(cp,av);
    char c = cp[0];
    bool neg = false;
    if (c == 'N') {
        neg = false;
    } else if (c == 'S') {
        neg = true;
    } else {
        SPRTF("%s: lat value '%s', does NOT start with 'N' or 'S'!\n", module, av );
        return false;
    }
    cp++;
    return get_deg_val(cp,plat,neg,0);
}

bool get_lon_val( const char *av, double *plat )
{
    char *cp = GetNxtBuf();
    strcpy(cp,av);
    char c = cp[0];
    bool neg = false;
    if (c == 'E') {
        neg = false;
    } else if (c == 'W') {
        neg = true;
    } else {
        SPRTF("%s: lon value '%s', does NOT start with 'E' or 'W'!\n", module, av );
        return false;
    }
    cp++;
    return get_deg_val(cp,plat,neg,1);
}


#define BAD_DBL 400.0

void null_stuct(PPARKPOS ppp)
{
    ppp->flag = 0;
    ppp->index = -1;  // index="0"
    ppp->type[0] = 0; // type="gate"
    ppp->name[0] = 0; // name="D55"
    ppp->number = -1; // number="1"
    ppp->lat = BAD_DBL; // lat="N37 37.098"
    ppp->lon = BAD_DBL; // lon="W122 22.851"
    ppp->hdg = BAD_DBL; // heading="250.708"
    ppp->rad = 0.0;     // BAD_DBL; // radius="40.3211"
    ppp->pbr = -1;    // pushBackRoute="506" 
    ppp->code[0] = 0; // airlineCodes="JBU,SWA,VRD" />
}


////////////////////////////////////////////////////////////////////
/// XML Handling
class MyVisitor : public XMLVisitor
{
public:
    MyVisitor() {
        got_gn = false;
        in_pl = false;
        pvpp = new vPKPOS;
    }
    virtual void startXML () {
        OUTPUT << "Start XML" << MEOL;
    }
    virtual void endXML () {
        OUTPUT << "End XML" << MEOL;
    }
    virtual void startElement (const char * name, const XMLAttributes &atts) {
        int i, max = atts.size();
#if 0 // 0000000000000000000000000000000000000000000000000000000000000
        //         0123456789012345
        OUTPUT << "Start element : '" << name << "'";
        if (max) {
            OUTPUT << ", atts " << max;
        }
        OUTPUT << MEOL;
        for (i = 0; i < max; i++) {
            OUTPUT << "  " << atts.getName(i) << '=' << atts.getValue(i) << MEOL;
        }
#endif // 0000000000000000000000000000000000000000000000000000000000000
       if (strcmp(name,"groundnet") == 0) {
           got_gn = true;
       } else if (got_gn) {
           if (strcmp(name,"parkingList") == 0) {
              in_pl = true;
           } else if (in_pl) {
              if (strcmp(name,"Parking") == 0) {
                  null_stuct(&pp);
                  pp.flag = 0;
                  for (i = 0; i < max; i++) {
                      const char *an = atts.getName(i);
                      const char *av = atts.getValue(i);
                      if (strcmp(an,"index") == 0) {
                          // index="0"
                          pp.flag |= ppf_index;
                          pp.index = atoi(av);
                      } else if (strcmp(an,"type") == 0) {
                          // type="gate"
                          if (strlen(av) >= MX_TYPE) {
                              SPRTF("%s: WARNING: Type size %d exceeeds buffer %d! *** FIX ME ***\n", module, (int)strlen(av), MX_TYPE);
                              strncpy(pp.type,av,MX_TYPE);
                              pp.type[MX_TYPE-1] = 0;
                          } else {
                              strcpy(pp.type,av);
                          }
                          pp.flag |= ppf_type;
                      } else if (strcmp(an,"name") == 0) {
                          // name="D55"
                          if (strlen(av) >= MX_NAME) {
                              SPRTF("%s: WARNING: Name size %d exceeeds buffer %d! *** FIX ME ***\n", module, (int)strlen(av), MX_NAME);
                              strncpy(pp.type,av,MX_NAME);
                              pp.name[MX_NAME-1] = 0;
                          } else {
                              strcpy(pp.name,av);
                          }
                          pp.flag |= ppf_name;
                          // add_to_names(av);
                      } else if (strcmp(an,"number") == 0) {
                          // number="1"
                          pp.number = atoi(av);
                          pp.flag |= ppf_number;
                      } else if (strcmp(an,"lat") == 0) {
                          // lat="N37 37.098"
                          if (get_lat_val( av, &pp.lat )) {
                              pp.flag |= ppf_lat;
                          }
                      } else if (strcmp(an,"lon") == 0) {
                          // lon="W122 22.851"
                          if (get_lon_val( av, &pp.lon )) {
                                pp.flag |= ppf_lon;
                          }
                      } else if (strcmp(an,"heading") == 0) {
                          // heading="250.708"
                          pp.flag |= ppf_hdg;
                          pp.hdg = atof(av);
                      } else if (strcmp(an,"radius") == 0) {
                          // radius="40.3211"
                          pp.flag |= ppf_rad;
                          pp.rad = atof(av);
                      } else if (strcmp(an,"pushBackRoute") == 0) {
                          // pushBackRoute="506" 
                          pp.flag |= ppf_pbr;
                          pp.pbr = atoi(av);
                      } else if (strcmp(an,"airlineCodes") == 0) {
                          // airlineCodes="JBU,SWA,VRD"
                          if (strlen(av) >= MX_CODES) {
                              SPRTF("%s: WARNING: Name size %d exceeeds buffer %d! *** FIX ME ***\n", module, (int)strlen(av), MX_CODES);
                              strncpy(pp.code,av,MX_CODES);
                              pp.code[MX_CODES-1] = 0;
                          } else {
                              strcpy(pp.code,av);
                          }
                          pp.flag |= ppf_code;
                      } else {
                          pp.flag |= ppf_unk;
                      }
                  }
                  // done all attributes
                  //////////////////////////////////////////////////////////////////////////////
                  // Only save it if got the MINIMUM information
                  // at least lat, lon, hdg, ...
                  if ((pp.flag & ppf_min) == ppf_min) {
                      pvpp->push_back(pp);
                      if ( pp.flag & ppf_name ) {
                          std::stringstream name;
                          name << pp.name;
                          if ( pp.flag & ppf_number ) {
                              name << pp.number;
                          }
                          add_to_names(name.str().c_str());

                      }
                  } else {
                      SPRTF("%s: WARNING: parkpos does not have minimum info - skipped!\n", module);
                  }

              }
           }
        }
    }
    virtual void endElement (const char * name) {
        //         0123456789012345
        // OUTPUT << "End element   : '" << name << "'" << MEOL;
        if (strcmp(name,"groundnet") == 0) {
            got_gn = false;
        } else if (strcmp(name,"parkingList") == 0) {
            in_pl = false;
        }
    }
    virtual void data (const char * s, int len) {
        std::string xdat = std::string(s,len);
        //lrtrim(xdat);
        trim_in_place(xdat);
        if (xdat.size()) {
           //         0123456789012345
           //OUTPUT << "Character data: '" <<  xdat << "'" << MEOL;
        }
    }
    virtual void pi (const char * target, const char * data) {
        OUTPUT << "Processing instruction " << target << ' ' << data << MEOL;
    }
    virtual void warning (const char * message, int line, int column) {
        OUTPUT << "Warning: " << message << " (" << line << ',' << column << ')' << MEOL;
    }
    virtual void error (const char * message, int line, int column) {
        OUTPUT << "Error: " << message << " (" << line << ',' << column << ')' << MEOL;
    }
    bool got_gn, in_pl;
    PARKPOS pp;
    vPKPOS *pvpp;
};

void show_ppp(PPARKPOS ppp)
{
    SPRTF("--latitude=%lf --longitude=%lf --heading=%lf ", ppp->lat, ppp->lon, ppp->hdg);
    if ((ppp->flag & ppf_name) && ppp->name[0]) {
        SPRTF("--parkpos=%s ", ppp->name);
    }
    SPRTF("\n");
}

void show_park_pos(MyVisitor &visitor, const char *xml_file)
{
    PPARKPOS ppp;
    BBOX bb;
    std::stringstream xg, xg1, xg2;
    char *cp = GetNxtBuf();
    size_t ii, max = visitor.pvpp->size();
    sprintf(cp, "# %s: Have %d parking positions from %s\n", module, (int)max, xml_file);
    SPRTF("%s",cp);
    xg1 << cp;
    set_bbox( &bb );
    for (ii = 0; ii < max; ii++) {
        ppp = &visitor.pvpp->at(ii);
        add_bbox( &bb, ppp->lat, ppp->lon );
        if ((ppp->flag & ppf_rad) && (ppp->rad > 0.0)) {
            xg << get_rectangle( ppp->lat, ppp->lon, ppp->hdg, ppp->rad, &bb, ppp );
        }
        /// xg << ppp->lon << " " << ppp->lat << " # hdg " << ppp->hdg << MEOL;
        show_ppp(ppp);
    }

    // ADD the parkpos from apt.dat.gz
    // static const char *box_color = "blue";
    // static const char *hdg_color = "white";
    box_color = "green";
    hdg_color = "red";
    PAPTDATPP pad = sAptDatPP;
    ii = 0;
    while (pad->name) {
        pad++;
        ii++;
    }
    sprintf(cp,"# %d parkpos records from current apt.dat.gz", (int)ii);
    xg2 << cp << MEOL;
    SPRTF("%s\n",cp);
    pad = sAptDatPP;
    PARKPOS pp;
    null_stuct(&pp);

    while (pad->name) {
        pp.flag = ppf_name | ppf_type;
        strcpy(pp.name,pad->name);
        strcpy(pp.type,"gate");
        pp.lat = pad->lat;
        pp.lon = pad->lon;
        pp.hdg = pad->hdg;
        pp.rad = 30.0;
        xg2 << get_rectangle( pad->lat, pad->lon, pad->hdg, 30.0, &bb, &pp );
        show_ppp(&pp);
        pad++;
    }

    cp = GetNxtBuf();
    sprintf(cp, "# BBOX: %lf,%lf,%lf,%lf\n", bb.min_lon, bb.min_lat, bb.max_lon, bb.max_lat);
    xg1 << cp;
    xg1 << "color gray" << MEOL;
    xg1 << bb.min_lon << " " << bb.min_lat << MEOL;
    xg1 << bb.min_lon << " " << bb.max_lat << MEOL;
    xg1 << bb.max_lon << " " << bb.max_lat << MEOL;
    xg1 << bb.max_lon << " " << bb.min_lat << MEOL;
    xg1 << bb.min_lon << " " << bb.min_lat << MEOL;
    xg1 << "NEXT" << MEOL;


    bool dnxg = false;
    if (xg_output) {
        size_t len, wtn;
        FILE *fp = fopen(xg_output,"w");
        if (fp) {
            len = xg1.str().size();
            wtn = fwrite( xg1.str().c_str(), 1, len, fp );
            if (wtn == len) {
                len = xg.str().size();
                wtn = fwrite( xg.str().c_str(), 1, len, fp );
                if (wtn == len) {
                    len = xg2.str().size();
                    wtn = fwrite( xg2.str().c_str(), 1, len, fp );
                    if (wtn == len) {
                        dnxg = true;
                    }
                }
            }
            fclose(fp);
        }
        if (dnxg) {
            SPRTF("%s: xg written to '%s'\n", module, xg_output);
        } else {
            SPRTF("%s: Write to file '%s' FAILED\n", module, xg_output);
        }
    } 
    if (!dnxg) {
        direct_out_it((char *)xg1.str().c_str());
        direct_out_it((char *)xg.str().c_str());
    }
}

int parse_xml( const char *xml_file )
{
    MyVisitor visitor;
    std::ifstream input(xml_file);

    SPRTF("Reading '%s'\n", xml_file );
    try {
      readXML(input, visitor);
    } catch (const sg_exception& e) {
      ERROUT << "Error: file '" << xml_file << "' " << e.getFormattedMessage() << MEOL;
      return 1;

    } catch (...) {
      ERROUT << "Error reading from " << xml_file << MEOL;
      return 1;
    }
    show_park_pos(visitor,xml_file);
    return 0;
}

int Deal_with_input(const char *input)
{
    if (is_file_or_directory32 ( input ) != DT_FILE ) {
        SPRTF("%s: Unable to 'stat' file %s!\n", module, input );
        return 1;
    }
    size_t len = get_last_file_size32();
    if (len == 0) {
        SPRTF("%s: Input file %s has no SIZE!\n", module, input );
        return 1;
    }

    return parse_xml(input);
}

void fix_log_file(const char *name)
{
    const char *def_log = "temppark.txt";
    char *cp = GetNxtBuf();
    size_t len = GetBufSiz();
#ifdef _MSC_VER
    DWORD dwd = GetModuleFileName( NULL, cp, (DWORD)len );
    DWORD i, last = 0;
    int c;
    for (i = 0; i < dwd; i++) {
        c = cp[i];
        if ((c == '\\')||(c == '/')) {
            last = i + 1;
        }
    }
    if (last)
        cp[last] = 0;

    strcat(cp,def_log);
#else
    // TODO: Find a 'safe' place for this log file, probably HOME???
    *cp = 0;
    char *home = getenv("HOME");
    if (home) {
        strcpy(cp,home);
        strcat(cp,"/");
    }
    strcat(cp,def_log);

#endif
    set_log_file(cp,false);

}
void give_help( char *name )
{
    printf("%s: usage: [options] usr_input\n", module);
    printf("Options:\n");
    printf(" --help  (-h or -?) = This help and exit(2)\n");
    printf(" --xg <file>   (-x)    = Given an xgraph output file, write the parking list.\n");
    printf(" --add         (-a) = Add parkpos names to xg file.\n");
    printf(" --verb[n]     (-v) = Bump or set verbosity to 'n'. Vals 0,1,2,5,9 (def=%d)\n", verbosity);
    printf(" Given an ICAO.groundnet.xml file, enumerate the parking postions\n");
    printf(" found in the file is any.\n");
    // TODO: More help
}

int parse_args( int argc, char **argv )
{
    int i,i2,c;
    char *arg, *sarg;
    for (i = 1; i < argc; i++) {
        arg = argv[i];
        i2 = i + 1;
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-')
                sarg++;
            c = *sarg;
            switch (c) {
            case 'a':
                add_names = true;
                break;
            case 'h':
            case '?':
                give_help(argv[0]);
                return 2;
                break;
            case 'v':
                while (*sarg == 'v') {
                    verbosity++;
                    if (IS_DIGIT(*sarg)) {
                        verbosity = atoi(sarg);
                        break;
                    }
                    sarg++;
                }
                if (VERB1)
                    printf("%s: Set verbosity to %d\n", module, verbosity );
                break;
            case 'x':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    xg_output = strdup(sarg);
                    if (VERB1)
                        printf("%s: Set xg output to '%s'\n", module, xg_output);
                } else {
                    printf("%s: Expected file name to follow '%s'\n", module, arg);
                    return 1;
                }
                break;
            // TODO: Other arguments
            default:
                printf("%s: Unknown argument '%s'. Tyr -? for help...\n", module, arg);
                return 1;
            }
        } else {
            // bear argument
            if (usr_input) {
                printf("%s: Already have input '%s'! What is this '%s'?\n", module, usr_input, arg );
                return 1;
            }
            usr_input = strdup(arg);
            if (VERB1)
                printf("%s: Set user input to '%s'.\n", module, usr_input);
        }
    }
    if (!usr_input) {
        printf("%s: No user input found in command!\n", module);
        return 1;
    }
    return 0;
}

void show_name_stats()
{
    iSTGINT ii;
    int cnt, total = 0;
    int unique = 0;
    std::string s2;
    for (ii = pms->begin(); ii != pms->end(); ii++) {
        cnt = (*ii).second;
        s2  = (*ii).first;
        total += cnt;
        if (cnt == 1)
            unique++;
    }
    SPRTF("%s: %d parkpos name stats... ", module, total);
    if (unique) {
        SPRTF("%d are unique... ", unique);
        if (!VERB5) {
            SPRTF("only shown if -v5+... ");
        }
    }
    SPRTF("\n");
    for (ii = pms->begin(); ii != pms->end(); ii++) {
        cnt = (*ii).second;
        s2  = (*ii).first;
        if ((cnt > 1) || VERB5)
            SPRTF("name '%s' used %d times\n", s2.c_str(), cnt );
    }
    SPRTF("%s: Stats for total of %d parkpos...\n", module, total);
}

// main() OS entry
int main( int argc, char **argv )
{
    int iret = 0;
    iret = parse_args(argc,argv);
    if (iret)
        return iret;

    fix_log_file(argv[0]);
    pms = new mSTGINT;

    iret = Deal_with_input(usr_input);
    if (iret == 0) {
        show_name_stats();
    }
    delete pms;
    return iret;
}


// eof = parkpos.cxx
