/*\
 * xmltest.cxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#ifdef _MSC_VER
#include <Windows.h>
#endif
#include <stdio.h>
#include <string.h> // for strdup(), ...
#include <string>
#include <vector>
#include <locale>   // for std::isspace
#include <algorithm> // find_if() 
#include <functional> // not1(), ptr_fun<...>, ..
#include <iostream>
#include <fstream>
#include <sstream>
#include <simgear/xml/easyxml.hxx>

// other includes
#include "utils/utils.hxx"
#include "utils/sprtf.hxx"
#include "xmltest.hxx"

static const char *module = "xmltest";

// fwd refs
int parse_xml( const char *xml_file );

static const char *usr_input = 0;
static const char *output_path = 0;
static int verbosity = 1;
static int exit_value = 0;
static const char *out_file = 0;
static const char *fg_root_path = 0;
static int xml_read_count = 0;

#define MEOL std::endl

static vSTG xmlpath;
static vSTG inp_path;

#define VERB1 (verbosity >= 1)
#define VERB2 (verbosity >= 2)
#define VERB5 (verbosity >= 5)
#define VERB9 (verbosity >= 9)

//////////////////////////////////////////////////////////
// flags 
static int parsing_flag = 0;
#define flg_sim         0x00000001
#define flg_author      0x00000002
#define flg_status      0x00000004
#define flg_rating      0x00000008
#define flg_rFDM        0x00000010
#define flg_rsystems    0x00000020
#define flg_rcockpit    0x00000040
#define flg_rmodel      0x00000080
#define flg_avers       0x00000100
#define flg_fmodel      0x00000200
#define flg_path        0x00000400
#define flg_aero        0x00000800
#define flg_desc        0x00001000
#define flg_tags        0x00002000
#define flg_tag         0x00004000
#define flg_navdb       0x00008000
#define flg_minrwy      0x00010000
// <limits>
//  <mass-and-balance>
//   <maximum-ramp-mass-lbs>2407</maximum-ramp-mass-lbs>
//   <maximum-takeoff-mass-lbs>2400</maximum-takeoff-mass-lbs>
//   <maximum-landing-mass-lbs>2400</maximum-landing-mass-lbs>
//  </mass-and-balance>
// </limits>
#define flg_limits      0x00020000
#define flg_mass        0x00040000
#define flg_maxramp     0x00080000
#define flg_maxtoff     0x00100000
#define flg_maxland     0x00200000

#define flg_tutorials   0X00400000

typedef struct tagFLGITEMS {
    std::string authors;
    std::string status;
    std::string rFDMr;
    std::string rsystems;
    std::string rcockpit;
    std::string rmodel;
    std::string avers;
    std::string fmodel;
    std::string desc;
    std::string aero;
    std::string tags;
    std::string minrwy;
    std::string maxramp;
    std::string maxtoff;
    std::string maxland;
    vSTG acfiles;
}FLGITEMS, *PFLGITEMS;

typedef struct tagFLG2TXT {
    int flag;
    const char *text;
}FLG2TXT, *PFLG2TXT;

static FLG2TXT flg2txt[] = {
    { flg_sim, "sim" }, // 0x00000001
    { flg_author, "author" },   //  0x00000002
    { flg_status, "status" },   // 0x00000004
    { flg_rating, "rating" },   // 0x00000008
    { flg_rFDM, "FDM" }, //         0x00000010
    { flg_rsystems, "systems" }, //     0x00000020
    { flg_rcockpit, "cockpit" }, //     0x00000040
    { flg_rmodel, "model" }, //       0x00000080
    { flg_avers, "aircraft-version" },   //       0x00000100
    { flg_fmodel, "flight-model" }, //      0x00000200
    { flg_path, "path" },   // 0x00000400
    { flg_aero, "aero" },   // 0x00000400
    { flg_desc, "description" }, // 0x00001000
    { flg_tags, "tags" },
    { flg_tag,  "tag" },
    { flg_navdb, "navdb" },
    { flg_minrwy, "min-runway-length-ft" },
    { flg_limits, "limits" },
    { flg_mass, "mass-and-balance" },
    { flg_maxramp, "maximum-ramp-mass-lbs" },
    { flg_maxtoff, "maximum-takeoff-mass-lbs" },
    { flg_maxland, "maximum-landing-mass-lbs" },
    { flg_tutorials, "tutorials" },

    //////////////////////////////////////
    // last entry
    { 0, 0 }
};

static int simauthor = (flg_sim | flg_author);
static int simstatus = (flg_sim | flg_status);
static int simrFDM  = (flg_sim | flg_rating | flg_rFDM);
static int simrsystems  = (flg_sim | flg_rating | flg_rsystems);
static int simrcockpit  = (flg_sim | flg_rating | flg_rcockpit);
static int simrmodel  = (flg_sim | flg_rating | flg_rmodel);
static int simavers  = (flg_sim | flg_avers);
static int simfmod  = (flg_sim | flg_fmodel);
static int simaero  = (flg_sim | flg_aero);
static int simdesc  = (flg_sim | flg_desc);
static int simtags  = (flg_sim | flg_tags | flg_tag);
static int simminrwy = (flg_sim | flg_navdb | flg_minrwy);

//#ifndef STRICT_MODEL_PATH
//static int simmodpath = flg_path;
//#else   // STRICT_MODEL_PATH
//static int simmodpath = (flg_sim | flg_path);
static int simmodpath = (flg_sim | flg_rmodel | flg_path);
//#endif // STRICT_MODEL_PATH y/n

static int limmassramp = (flg_limits | flg_mass | flg_maxramp);
static int limmasstoff = (flg_limits | flg_mass | flg_maxtoff);
static int limmassland = (flg_limits | flg_mass | flg_maxland);



std::string get_flag_name(int flag)
{
    std::string s;
    PFLG2TXT f2t = flg2txt;
    while (f2t->text) {
        if (flag & f2t->flag) {
            if (s.size())
                s += "/";
            s += f2t->text;
        }
        f2t++;
    }
    if (flag == 0)
        s = "none";
    else if (s.size() == 0)
        s = "unknown";
    return s;
}

void add_parsing_flag( const char *element )
{
    PFLG2TXT f2t = flg2txt;
    int flag;
    while (f2t->text) {
        if (strcmp(f2t->text,element) == 0) {
            flag = f2t->flag;
            parsing_flag |= flag;
            if (VERB9) {
                std::string s1 = get_flag_name(flag); 
                std::string s2 = get_flag_name(parsing_flag); 
                SPRTF("DBG: Added flag '%s' %#X %s, now %s\n", element, flag, s1.c_str(), s2.c_str());
            }
            flag = parsing_flag;
            return;
        }
        f2t++;
    }
}

void remove_parsing_flag( const char *element )
{
    PFLG2TXT f2t = flg2txt;
    int flag;
    while (f2t->text) {
        if (strcmp(f2t->text,element) == 0) {
            flag = f2t->flag;
            if (parsing_flag & flag) {
                parsing_flag &= ~flag;
                if (VERB9) {
                    std::string s1 = get_flag_name(flag); 
                    std::string s2 = get_flag_name(parsing_flag); 
                    SPRTF("DBG: Removed flag '%s' %#X %s, now %s\n", element, flag, s1.c_str(), s2.c_str());
                }
                flag = parsing_flag;
            } else {
                flag = 0;
            }
            return;
        }
        f2t++;
    }
}

std::string get_xml_path()
{
      size_t i, max = xmlpath.size();
      std::string path;
      for (i = 0; i < max; i++) {
          if (path.size())
              path += "/";
          path += xmlpath[i];
      }
      return path;
}

///////////////////////////////////////////////////////////
int find_extension(std::string &file, const char *ext)
{
    if (!ext)
        return 0;
    size_t len = strlen(ext);
    if (!len)
        return 0;
    size_t pos = file.find(ext);
    if ((pos != std::string::npos) && (pos == file.size() - len))
        return 1;
    return 0;
}


#define GOT_FLG(a) ((parsing_flag & a) == a)
#define NOT_FLG(a)  !(parsing_flag & a)

static PFLGITEMS pflgitems = 0;
static std::string last_good_path;
static vSTG good_paths;
static std::string active_path;
static std::string active_file;

bool conditional_add_string(vSTG &vs, std::string &file)
{
    std::string f;
    size_t ii, max = vs.size();
    for (ii = 0; ii < max; ii++) {
        f = vs[ii];
        if (f == file)
            return false;
    }
    vs.push_back(file);
    return true;
}

int get_file_name(std::string value, std::string &ifile)
{
    int fnd = 0;
    std::string fil, p;
    size_t ii, max;
    // first try last good path, but not sure this is a GOOD idea?
    // If the first found path is not the 'best'?!?!?
    if (last_good_path.size()) {
        fil = last_good_path;
        fil += PATH_SEP;
        fil += value;
        ensure_native_sep( fil );
        if (is_file_or_directory32( fil.c_str()) == DT_FILE) {
            fnd = 1;
        }
    }
    // If not found, try the path of the primary input file
    if (!fnd) {
        std::string s;
        max = inp_path.size();
        p = ""; // clear path, and build step by step
        for (ii = 0; ii < max; ii++) {
            s = inp_path[ii];
            if (p.size())
                p += PATH_SEP;
            p += s;
            fil = p;
            fil += PATH_SEP;
            fil += value;
            ensure_native_sep( fil );
            if (is_file_or_directory32( fil.c_str()) == DT_FILE) {
                last_good_path = p; // try this one first next time...
                fnd = 1;
                break;
            }
        }
    }
    if (!fnd && good_paths.size()) {
        max = good_paths.size();
        for (ii = 0; ii < max; ii++) {
            p = good_paths[ii];
            if (p == last_good_path)
                continue;
            fil = p;
            fil += PATH_SEP;
            fil += value;
            ensure_native_sep( fil );
            if (is_file_or_directory32( fil.c_str()) == DT_FILE) {
                fnd = 1;
                break;
            }
        }
    }
    if (!fnd && active_path.size()) {
        fil = active_path;
        fil += PATH_SEP;
        fil += value;
        ensure_native_sep( fil );
        if (is_file_or_directory32( fil.c_str()) == DT_FILE) {
            fnd = 1;
        }
    }
    // very klugee!!! relative to the active path using the fg_root_path [+ Aircraft]
    if (!fnd && fg_root_path && active_path.size()) {
        vSTG vsp = PathSplit(value);
        size_t cnt = 0;
        max = vsp.size();
        for (ii = 0; ii < max; ii++) {
            if (vsp[ii] == "..")
                cnt++;
            else
                break;
        }
        if (cnt) {
            // got relative count
            // build up a relative to fg root path
            vSTG vap = PathSplit(active_path);
            if (vap.size() > cnt) {
                fil = fg_root_path;
                fil += PATH_SEP;
                fil += "Aircraft";
                ii = vap.size() - cnt;
                for ( ; ii < vap.size(); ii++) {
                    fil += PATH_SEP;
                    fil += vap[ii];
                }
                fil += PATH_SEP;
                fil += value;
                ensure_native_sep( fil );
                fix_relative_path( fil );
                if (is_file_or_directory32( fil.c_str()) == DT_FILE) {
                    fnd = 1;
                }
            }
        }
    }
    if (fnd)
        ifile = fil;
    return fnd;
}

void process_xml( std::string fil, int flag = 0 );
void process_xml( std::string fil, int flag )
{
    std::string path = get_xml_path();
    int save = parsing_flag;
    xmlpath.clear();
    exit_value |= parse_xml(fil.c_str());
    parsing_flag = save;
    xmlpath.clear();
    xmlpath = PathSplit(path);
}


//int save_text_per_flag( char *in_value, std::string &mfile, const char *file )
int save_text_per_flag( const char *in_value )
{
    int iret = 0;
    if (!in_value)
        return 0;
    if (!in_value[0])
        return 0;
    std::string value = in_value;
    trim_in_place(value);
    if (value.size() == 0)
        return 0;
#ifdef ADD_DEBUG_STOP
    debug_test_stg(value);
#endif
    if (pflgitems == 0)
        pflgitems = new FLGITEMS;
    int fnd = 0;
    if (parsing_flag & flg_sim) {
        std::string fil, p;
        bool isac = find_extension(value,".ac") ? true : false;
        bool isxml = find_extension(value,".xml") ? true : false;
        if ( isac || isxml ) {
            fnd = get_file_name(value,fil);
            if (fnd) {
                if (isxml) {
                    process_xml(fil);
                    return exit_value;
                }
            } else if (VERB5) {
                SPRTF("WARNING: Path to '%s' NOT FOUND!\n", value.c_str());
            }
        }

        if (GOT_FLG(simauthor)) {
            // save the author
            pflgitems->authors = value;
        }
        if (GOT_FLG(simstatus)) {
            pflgitems->status = value;
        }
        if (GOT_FLG(simrFDM)) {
            pflgitems->rFDMr = value;
        }
        if (GOT_FLG(simrsystems)) {
            pflgitems->rsystems = value;
        }
        if (GOT_FLG(simrcockpit)) {
            pflgitems->rcockpit = value;
        }
        if (GOT_FLG(simrmodel)) {
            pflgitems->rmodel = value;
        }
        if (GOT_FLG(simavers)) {
            pflgitems->avers = value;
        }
        if (GOT_FLG(simfmod)) {
            pflgitems->fmodel = value;
        }
        if (GOT_FLG(simaero)) {
            pflgitems->aero = value;
        }
        if (GOT_FLG(simdesc)) {
            if (NOT_FLG(flg_tutorials)) {
                pflgitems->desc = value;
            }
        }
        // added 20150111
        if (GOT_FLG(simtags)) {
            if (pflgitems->tags.size())
                pflgitems->tags += "|";
            pflgitems->tags += value;
        }
        if (GOT_FLG(simminrwy)) {
            pflgitems->minrwy = value;
        }
        // =============================
        if (GOT_FLG(simmodpath)) {
            if (isac && fnd && fil.size()) {
                conditional_add_string(pflgitems->acfiles,fil);
            }
        }
    }
    if (GOT_FLG(limmassramp)) {
        pflgitems->maxramp = value;
    }
    if (GOT_FLG(limmasstoff)) {
        pflgitems->maxtoff = value;
    }
    if (GOT_FLG(limmassland)) {
        pflgitems->maxland = value;
    }

    return iret;
}



////////////////////////////////////////////////////////////////////
/// XML Handling
///////////////////////////////////////////////////////////////////
class MyVisitor : public XMLVisitor
{
public:
  virtual void startXML () {
      if (VERB9) {
          SPRTF("Start XML\n");
      }
  }
  virtual void endXML () {
      if (VERB9) {
          SPRTF("End XML\n");
      }
  }
  virtual void startElement (const char * name, const XMLAttributes &atts) {
      std::stringstream ss;
      int i, max = atts.size();
      add_parsing_flag(name);
      //         0123456789012345
      ss << "Start element : '" << name << "'";
      if (max) {
          ss << ", attrs " << max;
      }
      ss << MEOL;
      for (i = 0; i < max; i++) {
          ss << "  " << atts.getName(i) << '=' << atts.getValue(i) << MEOL;
      }
      xmlpath.push_back(name);
      ss << "Path: " << get_xml_path() << MEOL;
      ss << "Flag: " << get_flag_name(parsing_flag) << MEOL;
      if (VERB9) {
          direct_out_it((char *)ss.str().c_str());
      }
      for (i = 0; i < max; i++) {
          std::string nam = atts.getName(i);
          std::string val = atts.getValue(i);
          ss << "  " << nam << '=' << val << MEOL;
          if (nam == "include") {
              bool isxml = find_extension(val,".xml") ? true : false;
              if ( isxml ) {
                  std::string fil;
                  int fnd = get_file_name(val,fil);
                  if (fnd) {
                      process_xml(fil,1);
                  } else if (VERB5) {
                      SPRTF("WARNING: include '%s' NOT FOUND!\n", val.c_str());
                 }
             }
          }
      }
  }

  virtual void endElement (const char * name) {
      remove_parsing_flag(name);
      std::stringstream ss;
      //     0123456789012345
      ss << "End element   : '" << name << "'" << MEOL;
      if (xmlpath.size()) {
          std::string s = xmlpath.back();
          if (strcmp((char *)name,s.c_str()) == 0) {
              xmlpath.pop_back();
          } else {
              SPRTF("WARNING: END element '%s' is NOT last on stack '%s'\n", name, s.c_str());
          }
      } else {
          SPRTF("WARNING: END element '%s' is NOT on stack!\n", name);
      }
      ss << "Path: " << get_xml_path() << MEOL;
      ss << "Flag: " << get_flag_name(parsing_flag) << MEOL;
      if (VERB9) {
          direct_out_it((char *)ss.str().c_str());
      }
  }
  // +++++++++++++++++++++++++++++++++++++++++++++++++++
  // TAKE CARE: const char * s IS NOT NULL TERMINATED
  // Must use std::string(s,len) to extract the string
  // +++++++++++++++++++++++++++++++++++++++++++++++++++
  virtual void data (const char * s, int len) {
      std::stringstream ss;
      std::string xdat = std::string(s,len);
      //lrtrim(xdat);
      trim_in_place(xdat);
      if (xdat.size()) {
          //     0123456789012345
          ss << "Character data: '" <<  xdat << "'" << MEOL;
          if (VERB9) {
              direct_out_it((char *)ss.str().c_str());
          }
          save_text_per_flag(xdat.c_str());
      }
  }

  virtual void pi (const char * target, const char * data) {
      std::stringstream ss;
      ss << "Processing instruction " << target << ' ' << data << MEOL;
      direct_out_it((char *)ss.str().c_str());
  }
  virtual void warning (const char * message, int line, int column) {
      std::stringstream ss;
      ss << "Warning: " << message << " (" << line << ',' << column << ')' << MEOL;
      direct_out_it((char *)ss.str().c_str());
  }
  virtual void error (const char * message, int line, int column) {
      std::stringstream ss;
      ss << "Error: " << message << " (" << line << ',' << column << ')' << MEOL;
      direct_out_it((char *)ss.str().c_str());
  }
};

static vSTG xml_done_files;

int parse_xml( const char *xml_file )
{
    std::string file = xml_file;
    if (!conditional_add_string(xml_done_files,file)) {
        return 0; // file already processed
    }
    active_path = get_path_only(file);
    active_file = get_file_only(file);

    MyVisitor visitor;
    std::ifstream input(xml_file);
    if (input.bad()) {
        SPRTF("%s: FAILED Reading %s\n", module, xml_file);
        return 1;
    }
    if (xml_read_count == 0) {
        if (VERB1)
            SPRTF("%s: Reading %s\n", module, xml_file);
    } else {
        if (VERB9)
            SPRTF("%s: %d: Reading %s\n", module, xml_read_count + 1, xml_file);
    }
    xml_read_count++;
    std::string path = get_path_only(file);
    conditional_add_string(good_paths,path);
    try {
        readXML(input, visitor);
    } catch (const sg_exception& e) {
        std::stringstream ss;
        ss << "Error: file '" << xml_file << "' " << e.getFormattedMessage() << MEOL;
        direct_out_it((char *)ss.str().c_str());
        return 1;
    } catch (...) {
        std::stringstream ss;
        ss << "Error reading from " << xml_file << MEOL;
        direct_out_it((char *)ss.str().c_str());
        return 1;
    }
    return 0;
}
//////////////////////////////////////////////////////////////////////

char *get_full_path( char *file )
{
    char *cp = GetNxtBuf();
    strcpy(cp,file);
#ifdef _MSC_VER
    DWORD siz = (DWORD)GetBufSiz();
    DWORD res = GetFullPathName(file,
        siz,
        cp,
        NULL );
#else
    // TODO: Get full path in UNIX
    char *res = realpath(file,cp);
    if (!res)
        strcpy(cp,file);
#endif
    return cp;
}


void give_help( char *name )
{
    std::string en = name;
    en = get_file_only(en);
    printf("%s: usage: [options] xml_file\n", en.c_str());
    printf("Options:\n");
    printf(" --help  (-h or -?) = This help and exit(2)\n");
    printf(" --verb[nn]    (-v) = Bump or set verbosity. (def=%d)\n", verbosity);
    printf(" --log <file>  (-l) = Set the log file for output.  Use 'none' to disable.\n (def=%s)\n", get_log_file());
    printf(" --out <file>  (-o) = Write results to out file. (def=%s)\n",
        out_file ? out_file : "none");
    printf(" --root <path> (-r) = Set the 'root' path.\n");
    printf("\n");
    printf(" Will parse the input as a FlightGear 'xxx-set' xml file, and extract information.\n");
    printf(" While there is a good attempt to handle a relative file name, it is certainly better\n");
    printf(" to use a fully qualified input file name.\n");
    printf("\n");
    printf(" NOTE: This may ONLY work for FlightGear Aircraft 'xxx-set.xml' files that are\n");
    printf("      in the fgdata folder since there are some very FG specific relative\n");
    printf("      paths applied to some.\n");
    printf("\n");
    printf(" But in general terms it could be taken as a reasonable example of how to\n");
    printf(" extract information from any xml file using the services of SimGear easyxml library.\n");
    printf(" Use -v9 to see ALL the gory parsing details.\n");
    printf("\n");
}

#define ISDIGIT(a) (( a >= '0' ) && ( a <= '9' ))

int parse_args( int argc, char **argv )
{
    int i,i2,c;
    char *arg, *sarg;
    for (i = 1; i < argc; i++) {
        arg = argv[i];
        i2 = i + 1;
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-')
                sarg++;
            c = *sarg;
            switch (c) {
            case 'h':
            case '?':
                give_help(argv[0]);
                return 2;
                break;
            case 'l':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    set_log_file(sarg,false);
                } else {
                    SPRTF("%s: Expected log file name to follow '%s'! Aborting...\n", module, arg);
                    return 1;
                }
                break;
            case 'o':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    out_file = strdup(sarg);
                } else {
                    SPRTF("%s: Expected output file name to follow '%s'! Aborting...\n", module, arg);
                    return 1;
                }
                break;
            case 'r':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    fg_root_path = strdup(sarg);
                    if (is_file_or_directory32(fg_root_path) != 2) {
                        SPRTF("%s: Unable to 'stat' fg root path '%s'! Check name. Aborting...\n", module,
                            fg_root_path);
                        return 1;
                    }
                } else {
                    SPRTF("%s: Expected root path to follow '%s'! Aborting...\n", module, arg);
                    return 1;
                }
                break;
            case 'v':
                sarg++;
                verbosity++;
                while (*sarg && !ISDIGIT(*sarg)) {
                    if (*sarg == 'v')
                        verbosity++;
                    sarg++;
                }
                if (ISDIGIT(*sarg))
                    verbosity = atoi(sarg);
                if (VERB1)
                    printf("Set verbosity to %d\n", verbosity);
                break;

            default:
                printf("%s: Unknown argument '%s'. Tyr -? for help...\n", module, arg);
                return 1;
            }
        } else {
            // bear argument
            if (usr_input) {
                printf("%s: Already have input '%s'! What is this '%s'?\n", module, usr_input, arg );
                return 1;
            }
            usr_input = strdup(get_full_path(arg));
        }
    }
    if (!usr_input) {
        printf("%s: No user input found in command!\n", module);
        return 1;
    }
    if (fg_root_path == 0) {
        arg = getenv("FG_ROOT");
        if (arg)
            fg_root_path = strdup(arg);
    }
    if (fg_root_path) {
        if (is_file_or_directory32(fg_root_path) == 2) {
            std::string s = fg_root_path;
            conditional_add_string(good_paths,s);
        }
    }


    return 0;
}

void fix_log_file(const char *name)
{
    const char *def_log = "tempxml.txt";
    char *cp = GetNxtBuf();
    size_t len = GetBufSiz();
    *cp = 0;
#ifdef _MSC_VER
    DWORD dwd = GetModuleFileName( NULL, cp, (DWORD)len );
    DWORD i, last = 0;
    int c;
    for (i = 0; i < dwd; i++) {
        c = cp[i];
        if ((c == '\\')||(c == '/')) {
            last = i + 1;
        }
    }
    if (last)
        cp[last] = 0;
    if (*cp)
        output_path = strdup(cp);
    strcat(cp,def_log);
#else
    // TODO: Find a 'safe' place for this log file, probably HOME???
    char *home = getenv("HOME");
    if (home) {
        strcpy(cp,home);
        strcat(cp,"/");
    }
    output_path = strdup(cp);
    strcat(cp,def_log);
#endif
    set_log_file(cp,false);

}

static const char *skip_folder[] = {
    "adf",
    "alt",
    "alt-2",
    "altimeter",
    "asi300",
    "Avionics",
    "Cabine",
    "cdu",
    "clock",
    "Cockpit",
    "computing-gun-sights",
    "Crew",
    "Effects",
    "effects",
    "Effects-Submodels",
    "EFIS",
    "egt",
    "Engine",
    "Engines",
    "Exhausts",
    "Exterior",
    "External-objects",
    "flaps",
    "FlightDeck",
    "garmin196",
    "GasHandle",
    "Geometry",
    "GMeter",
    "Guns",
    "Handles",
    "Human",
    "Humans",
    "Illuminators",
    "Immat",
    "Instrumentation",
    "instruments",
    "Instruments",
    "Instruments-3d",
    "Instruments3d",
    "interior",
    "Interior",
    "ki206",
    "Kns80",
    "kr87-adf",
    "kt70",
    "kt76a",
    "kx165",
    "ky196",
    "landinggear",
    "LandingLight",
    "Landinglights",
    "LedStrip",
    "LeftPanel",
    "LeftSwitchPanel",
    "light",
    "Light",
    "Lights",
    "lights",
    "Loads",
    "magneto-switch",
    "MainPanel",
    "Manual",
    "Missiles",
    "mk-viii",
    "NASA",
    "Operations",
    "Panel",
    "panels",
    "Panels",
    "Parachutes",
    "pedals",
    "Pilot",
    "Pogos",
    "primus-1000",
    "Propeller",
    "propeller",
    "propellers",
    "Propellers",
    "Pushback",
    "quadrant",
    "Radar",
    "Remb",
    "Rescue",
    "RightPanel",
    "RightSwitchPanel",
    "Rotors",
    "rwr",
    "Seat",
    "Sprayer",
    "Stick",
    "Stickers",
    "Stores",
    "Submodels",
    "Switches",
    "Systems",
    "Tach",
    "tach",
    "tc",
    "Throttle",
    "trim",
    "vor",
    "vsi-6",
    "Vzor",
    "weapon",
    "weapons",
    "Weapons",
    "wing",
    "yoke",
    "zkv500",
    0
    };

static const char *skip_models[] = {
    "null.ac",
    "marker.ac",
    "shadow.ac",
    "tracer.ac",
    0
};

bool in_skip_list(const char *dir)
{
    int i;
    for (i = 0; ; i++) {
        if (skip_folder[i] == 0)
            break;
        if (strcmp(dir,skip_folder[i]) == 0)
            return true;
    }
    return false;
}

bool in_skip_models(const char *mod)
{
    int i;
    for (i = 0; ; i++) {
        if (skip_models[i] == 0)
            break;
        if (strcmp(mod,skip_models[i]) == 0)
            return true;
    }
    return false;
}

bool file_in_skip( std::string & file )
{
    std::string p, path = get_path_only(file);
    if (path.size()) {
        size_t ii, max;
        vSTG vsp = PathSplit(path);
        max = vsp.size();
        for (ii = 0; ii < max; ii++) {
            p = vsp[ii];
            if (in_skip_list(p.c_str()))
                return true;
        }
    }
    path = get_file_only(file);
    if (path.size()) {
        if (in_skip_models(path.c_str()))
            return true;
    }
    return false;
}

//////////////////////////////////////////////////////////////////////////
// When MULTIPLE model ac files found, try to choose the ONE
// based on several small name changes usually between the 
// Aircraft/<model> folder, and the Aircraft/<model>/.../<...>.ac files
// 1: The first choice is if the <model> folder === the <model>.ac - obvious choice
// 2: Some time there are 'case' changes so do stricmp(<model>,<acmodel>) == 0
// 3: If an INI aero = <name> matches the <model>.ac
// ...
// see notes below
// ...
// n: Dangerous: Elimination of unlikely sub-folders, and specific .ac files
//////////////////////////////////////////////////////////////////////////
std::string choose_best_model_file(vSTG &ac)
{
    std::string p,s,f,name,path;
    size_t ii, max = ac.size();
    size_t i2, max2;
    std::string acr = "Aircraft";
    vSTG modfolds;
    for (ii = 0; ii < max; ii++) {
        f = ac[ii];
        name = get_file_only(f);
        path = get_path_only(f);
        if (name.size() && path.size()) {
            vSTG vsp = PathSplit(path);
            vSTG vsf = FileSplit(name);
            s = vsf[0];
            max2 = vsp.size();
            for (i2 = 0; i2 < max2; i2++) {
                p = vsp[i2];
                if (p == acr) {
                    i2++;   // get directory after 'Aircraft'
                    if (i2 < max2) {
                        p = vsp[i2];    // this is the <model> folder
                        conditional_add_string(modfolds,p);
                        if (p == s) {
                            return f;
                        }
                    }
                    break;
                }
            }
        }
    }

    max2 = modfolds.size();
    if (max2) {
        for (ii = 0; ii < max; ii++) {
            f = ac[ii];
            name = get_file_only(f);
            if (name.size()) {
                vSTG vsf = FileSplit(name);
                s = vsf[0]; // get just the file name sans extent
                for (i2 = 0; i2 < max2; i2++) {
                    p = modfolds[i2];
                    // no case compare
                    if (stricmp(p.c_str(),s.c_str()) == 0) {
                        return f;
                    }
                }
            }
        }
    }

    if (pflgitems && pflgitems->aero.size()) {
        // does the ac file match the 'aero' name
        // like aero = 777-200
        // model-file1 = X:\fgdata\Aircraft\777\Models\777-200.ac
        for (ii = 0; ii < max; ii++) {
            f = ac[ii];
            name = get_file_only(f);
            if (name.size()) {
                vSTG vsf = FileSplit(name);
                s = vsf[0];
                if (s == pflgitems->aero) {
                    return f;
                }
            }
        }
    }

    // TODO: could try even harder... some observations
    // ================================================

    // model-file1 = D:\FG\fgaddon\Aircraft\Aerostar-700\Models\aerostar700.ac
    // model-file1 = D:\FG\fgaddon\Aircraft\ASK21-MI\Models\ask21mi.ac
    // model-file1 = D:\FG\fgaddon\Aircraft\B-17\Models\b17.ac
    // could strip non-alphanumeric from the <model> folder, ASK21-MT
    // and do a no case compare with 'ask21mi'

    // model-file1 = D:\FG\fgaddon\Aircraft\747-200\Models\boeing747-200.ac
    // maybe if the <model> folder can be found in the <name>.ac

    // model-file1 = D:\FG\fgaddon\Aircraft\A-26-Invader\Models\a26.ac
    // model-file1 = D:\FG\fgaddon\Aircraft\A24-Viking\Models\a24.ac
    // model-file1 = D:\FG\fgaddon\Aircraft\A340-600\Models\A340.ac
    // model-file2 = D:\FG\fgaddon\Aircraft\B-1B\Models\b1b.ac
    // model-file1 = D:\FG\fgaddon\Aircraft\Allegro-2000\Models\allegro.ac
    // getting difficult
    // maybe if the <name>.ac can be found in the <model> folder

    // model-file1 = D:\FG\fgaddon\Aircraft\a4\Models\a4-blue.ac
    // also difficult - if the <model> folder is found in only 1 of the <name>.ac

    // DANGEROUS: Eliminate <name>.ac files that contains one of the above 'skip' folders
    // and IFF left ONLY ONE, return it.
    vSTG nlist;
    int cnt = 0;
    for (ii = 0; ii < max; ii++) {
        f = ac[ii];
        path = get_path_only(f);
        if (path.size()) {
            vSTG vsp = PathSplit(path);
            max2 = vsp.size();
            for (i2 = 0; i2 < max2; i2++) {
                p = vsp[i2];
                if (in_skip_list(p.c_str()))
                    break;
            }
            if (i2 == max2) {
                name = get_file_only(f);
                if (!in_skip_models(name.c_str())) {
                    nlist.push_back(f);
                    cnt++;
                    if (cnt == 2)
                        break;  // no need to continue
                }
            }
        }
    }
    if (nlist.size() == 1) {
        f = nlist[0];
        return f;
    }

    s = "";
    return s;
}

void show_items_found()
{
    if (!pflgitems)
        return;
    size_t ii, max = pflgitems->acfiles.size();
    std::stringstream ss;
    std::string s;
#if 0 // 00000000000000000000000000000000000000000000000
    if (VERB5) {
        max = all_ac_files.size();
        SPRTF("\n");
        SPRTF("%s: ac files seen %d...\n", module, (int)max );
        if (max == all_ac_xml.size()) {
            for (ii = 0; ii < max; ii++) {
                s = all_ac_xml[ii];
                s = get_path_only(s);
                s += PATH_SEP;
                s += all_ac_files[ii];
                if (is_file_or_directory(s.c_str()) == 1) {
                    // found the file, so
                    SPRTF("%s\n", s.c_str());
                } else {
                    s = all_ac_files[ii];
                    SPRTF("%s in ", s.c_str());
                    SPRTF("%s\n", all_ac_xml[ii].c_str()); 
                }
            }
        } else {
            // Drat can only show this file
            for (ii = 0; ii < max; ii++) {
                s = all_ac_files[ii];
                SPRTF("%3d: %s\n", (int)(ii + 1), s.c_str());
            }
        }
        max = loaded_files.size();
        SPRTF("\n");
        SPRTF("%s: Loaded %d xml files...\n", module, (int)max );
        for (ii = 0; ii < max; ii++) {
            s = loaded_files[ii];
            SPRTF("%s\n", s.c_str());
        }
    }
#endif // 0000000000000000000000000000000000000
    int itemcnt = 0;
    max = pflgitems->acfiles.size();
    ss << "# " << module << ": Processed file '" << usr_input << MEOL;
    //if (VERB5) {
    //    ss << "# Items found in scan of " << scanned_count << " xml file(s), " << nice_num(GetNxtBuf(),uint64_to_stg(bytes_processed));
    //    ss << " bytes, in " << get_seconds_stg(get_seconds() - bgn_secs) << MEOL;
    //}
    ss << "[model]" << MEOL;

    if (pflgitems->aero.size()) {
        ss << "aero            = " << pflgitems->aero << MEOL;
        itemcnt++;
    }
    if (pflgitems->desc.size()) {
        ss << "description     = " << pflgitems->desc << MEOL;
        itemcnt++;
    }
    if (pflgitems->authors.size()) {
        ss << "authors         = " << pflgitems->authors << MEOL;
        itemcnt++;
    }
    if (pflgitems->status.size()) {
        ss << "status          = " << pflgitems->status << MEOL;
        itemcnt++;
    }
    if (pflgitems->rFDMr.size()) {
        ss << "rating_FDM      = " << pflgitems->rFDMr << MEOL;
        itemcnt++;
    }
    if (pflgitems->rsystems.size()) {
        ss << "rating_systems  = " << pflgitems->rsystems << MEOL;
        itemcnt++;
    }
    if (pflgitems->rcockpit.size()) {
        ss << "rating_cockpit  = " << pflgitems->rcockpit << MEOL;
        itemcnt++;
    }
    if (pflgitems->rmodel.size()) {
        ss << "rating_model    = " << pflgitems->rmodel << MEOL;
        itemcnt++;
    }
    if (pflgitems->avers.size()) {
        ss << "aircraft-version= " << pflgitems->avers << MEOL;
        itemcnt++;
    }
    if (pflgitems->fmodel.size()) {
        ss << "flight-model    = " << pflgitems->fmodel << MEOL;
        itemcnt++;
    }
    if (pflgitems->tags.size()) {
        ss << "tags            = " << pflgitems->tags << MEOL;
        itemcnt++;
    }
    if (pflgitems->minrwy.size()) {
        ss << "min-runway-ft   = " << pflgitems->minrwy << MEOL;
        itemcnt++;
    }
    if (pflgitems->maxramp.size()) {
        ss << "max-ramp-lbs    = " << pflgitems->maxramp << MEOL;
        itemcnt++;
    }
    if (pflgitems->maxtoff.size()) {
        ss << "max-takeoff-lbs = " << pflgitems->maxtoff << MEOL;
        itemcnt++;
    }
    if (pflgitems->maxland.size()) {
        ss << "max-landing-lbs = " << pflgitems->maxland << MEOL;
        itemcnt++;
    }

    if (max) {
        itemcnt++;
        if (max == 1) {
            ss << "model-file      = " << pflgitems->acfiles[0] << MEOL;
        } else {
            s = choose_best_model_file(pflgitems->acfiles);
            if (s.size()) {
                ss << "model-file      = " << s << MEOL;
            }
            if ((s.size() == 0) || VERB2) {
                if (VERB9) {
                    // SHOW EM ALL
                    ss << "# Got " << max << " 'model' files..." << MEOL;
                    for (ii = 0; ii < max; ii++) {
                        ss << "model-file" << (ii + 1) << " = " << pflgitems->acfiles[ii] << MEOL;
                    }
                } else {
                    int okcnt = 0;
                    for (ii = 0; ii < max; ii++) {
                        if (!file_in_skip(pflgitems->acfiles[ii])) {
                            okcnt++;
                            ss << "model-file" << okcnt << " = " << pflgitems->acfiles[ii] << MEOL;
                        }
                    }
                    if (okcnt == (int)max)
                        ss << "# Got " << max << " 'model' files." << MEOL;
                    else
                        ss << "# Got " << max << " 'model' files, but shown " << okcnt << ". -v9 to see all..." << MEOL;
                }
            }
        }
    }
    if (itemcnt) {
        if (VERB1) {
            if (VERB2)
                SPRTF("\n");
            direct_out_it((char *)ss.str().c_str());
        }
        if (out_file) {
            FILE *fp = fopen(out_file,"w");
            if (fp) {
                size_t res, len = ss.str().size();
                res = fwrite(ss.str().c_str(),1,len,fp);
                fclose(fp);
                if (res == len) {
                    SPRTF("%s: Results witten to file '%s'\n", module, out_file);
                } else {
                    SPRTF("WARNING: Write to file '%s' failed! req %d, wrote %d\n", out_file, (int)len, (int)res);
                }
            } else {
                SPRTF("WARNING: Unable to Write to file '%s'!\n", out_file);
            }
        }
    } else {
        SPRTF("%s: No known item found in '%s'\n", module, usr_input );
    }

    // clean up...
    pflgitems->acfiles.clear();
    delete pflgitems;
    pflgitems = 0;
}

void set_inp_path( const char *path )
{
    std::string s = path;
    inp_path = PathSplit(get_path_only(s));
}

// main() OS entry
int main( int argc, char **argv )
{
    int iret = 0;
    fix_log_file(argv[0]);
    iret = parse_args(argc,argv);
    if (iret)
        return iret;
    
    set_inp_path( usr_input );
    exit_value = parse_xml( usr_input );

    //if (exit_value == 0)
        show_items_found();

    printf("%s: Log output written to '%s'\n", module, get_log_file());

    return exit_value;
}


// eof = xmltest.cxx
