/*\
 * utils.cxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string>
#include "utils.hxx"

static const char *module = "utils";

// implementation
#if 0 // 000000000000000000000000000000000000000000000000000000000000000000
// trim from start
static inline std::string &ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        return s;
}

// trim from end
static inline std::string &rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
        return s;
}

// trim from both ends
static inline std::string &lrtrim(std::string &s) {
        return ltrim(rtrim(s));
}
#endif // 000000000000000000000000000000000000000000000000000000000000000000

// modifies input string, returns input
std::string& trim_left_in_place(std::string& str) {
    size_t i = 0;
    while(i < str.size() && isspace(str[i])) { ++i; };
    return str.erase(0, i);
}

#ifdef USE_STD_ISSPACE
std::string& trim_right_in_place(std::string& str) {
    size_t i = str.size();
    while(i > 0 && isspace(str[i - 1])) { --i; };
    return str.erase(i, str.size());
}
#else // !USE_STD_ISSPACE
std::string& trim_right_in_place(std::string& str) {
    int i = (int)str.size();
    unsigned char c;
    while((i > 0) && (c = str[i - 1]) && isspace(c)) { --i; }
    return str.erase(i, str.size());
}
#endif // USE_STD_ISSPACE y/n

std::string& trim_in_place(std::string& str) {
    return trim_left_in_place(trim_right_in_place(str));
}

static struct stat buf;
DiskType is_file_or_directory32 ( const char * path )
{
    if (!path)
        return DT_NONE;
	if (stat(path,&buf) == 0)
	{
		if (buf.st_mode & M_IS_DIR)
			return DT_DIR;
		else
			return DT_FILE;
	}
	return DT_NONE;
}

size_t get_last_file_size32() { return buf.st_size; }

void ensure_win_sep( std::string &path )
{
    std::string::size_type pos;
    std::string s = "/";
    std::string n = "\\";
    while( (pos = path.find(s)) != std::string::npos ) {
        path.replace( pos, 1, n );
    }
}

void ensure_unix_sep( std::string &path )
{
    std::string::size_type pos;
    std::string n = "/";
    std::string s = "\\";
    while( (pos = path.find(s)) != std::string::npos ) {
        path.replace( pos, 1, n );
    }
}

void ensure_native_sep( std::string &path )
{
#ifdef WIN32
    ensure_win_sep(path);
#else
    ensure_unix_sep(path);
#endif
}

vSTG PathSplit( std::string &path ) 
{
    std::string tmp = path;
    std::string s = PATH_SEP;
    vSTG result;
    size_t pos;
    bool done = false;

    result.clear();
    ensure_native_sep(tmp);
    while ( !done ) {
        pos = tmp.find(s);
        if (pos != std::string::npos) {
            result.push_back( tmp.substr(0, pos) );
            tmp = tmp.substr( pos + 1 );
        } else {
            if ( !tmp.empty() )
                result.push_back( tmp );
            done = true;
        }
    }
    return result;
}

vSTG FileSplit( std::string &file ) 
{
    size_t pos = file.rfind(".");
    vSTG vs;
    if (pos != std::string::npos) {
        std::string s = file.substr(0,pos);
        vs.push_back(s);
        s = file.substr(pos);
        vs.push_back(s);
    } else {
        vs.push_back(file);
    }
    return vs;
}

void fix_relative_path( std::string &path )
{
    vSTG vs = PathSplit(path);
    size_t ii, max = vs.size();
    std::string npath, tmp;
    vSTG n;
    for (ii = 0; ii < max; ii++) {
        tmp = vs[ii];
        if (tmp == ".")
            continue;
        if (tmp == "..") {
            if (n.size()) {
                n.pop_back();
                continue;
            }
            return; // nothing to POP - give up on the exercise
        }
        n.push_back(tmp);
    }
    ii = n.size();
    if (ii && (ii != max)) {
        max = ii;
        for (ii = 0; ii < max; ii++) {
            tmp = n[ii];
            if (npath.size())
                npath += PATH_SEP;
            npath += tmp;
        }
        path = npath;
    }
}

////////////////////////////////////////////////////////////////
// unix fix 
// when given like /media/path/file.xml
// the PathSplit correctly add a 'blank' to the vector, so added a 
// small fix to make sure a path separator gets add at the beginning
//
std::string get_path_only( std::string &file )
{
    std::string path;
    vSTG vs = PathSplit(file);
    size_t ii, max = vs.size();
    if (max)
        max--;
    for (ii = 0; ii < max; ii++) {
        if (path.size() || ii)
            path += PATH_SEP;
        path += vs[ii];
    }
    return path;
}

std::string get_file_only( std::string &path )
{
    std::string file;
    vSTG vs = PathSplit(path);
    size_t max = vs.size();
    if (max) {
        file = vs[max - 1];
    }
    return file;
}


// eof = utils.cxx
