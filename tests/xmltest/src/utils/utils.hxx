/*\
 * utils.hxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#ifndef _UTILS_HXX_
#define _UTILS_HXX_
#include <string>
#include <vector>

extern std::string& trim_left_in_place(std::string& str);
extern std::string& trim_right_in_place(std::string& str);
extern std::string& trim_in_place(std::string& str);

#ifdef _MSC_VER
#define M_IS_DIR _S_IFDIR
#else // !_MSC_VER
#define M_IS_DIR S_IFDIR
#endif
#ifdef WIN32
#define PATH_SEP "\\"
#else
#define PATH_SEP "/"
#endif

enum DiskType {
    DT_NONE = 0,
    DT_FILE,
    DT_DIR
};

extern DiskType is_file_or_directory32 ( const char * path );
extern size_t get_last_file_size32();

typedef std::vector<std::string> vSTG;

extern vSTG PathSplit( std::string &path );
extern vSTG FileSplit( std::string &file );
extern void fix_relative_path( std::string &path );

extern std::string get_path_only( std::string &file );
extern std::string get_file_only( std::string &path );

extern void ensure_native_sep( std::string &path );

#endif // #ifndef _UTILS_HXX_
// eof - utils.hxx
