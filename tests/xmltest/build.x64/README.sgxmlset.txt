README.sgxmlset.txt - 20150116

Program notes

===
*: Fail to find 'relative' files
xmltest: Reading D:\FG\fgaddon\Aircraft\YF-23\YF-23-set.xml
WARNING: include '../../Instruments/dme.xml' NOT FOUND!
WARNING: include '../../Instruments/clock.xml' NOT FOUND!
Why?
Ok, with some messy jiggery-poky code can now find most of these 'relative'
if given a FG_ROOT where they will exist in $FG_ROOT/Aircraft/Instruments

But can NOT find things like
24: D:\FG\fgaddon\Aircraft\Mig-29\Mig-29-set.xml
WARNING: Path to 'Aircraft/Mig-29/Models/Effects/tiptrail.xml' NOT FOUND!
WARNING: Path to 'Aircraft/Mig-29/Models/Effects/contrail.xml' NOT FOUND!
but that is because they DO NOT EXIST.

Feel confident enough to put these under say VERB5...

===
*: Fail to get correct 'description'

X:\fgdata\Aircraft\c172p\c172p-set.xml
X:\fgdata\Aircraft\b1900d\b1900d-set.xml
X:\fgdata\Aircraft\CitationX\CitationX-set.xml
D:\FG\fgaddon\Aircraft\747-400\747-400-set.xml
X:\fgdata\Aircraft\777\777-300-set.xml
D:\FG\fgaddon\Aircraft\bf109\bf109g-set.xml
...
Why?

Added a 'tutorials' flag, and do NOT keep 'text' if this flag is present...

Seems to have FIX IT ;=))

===
*: Try harder to eliminate ac model files

Add a list of sub-folder which are unlikely to contain the model

D:\FG\fgaddon\Aircraft\A-26-Invader\a26-set.xml
Now reduced to just ONE... -v9 will show them all

Very BAD case 
D:\FG\fgaddon\Aircraft\A-10\A-10-set.xml
but reduced the output from 54 to show only 43...

D:\FG\fgaddon\Aircraft\747-200\747-200-set.xml
Reduced from 23 to ONE

D:\FG\fgaddon\Aircraft\A24-Viking\a24-set.xml
From 15 to ONE...

D:\FG\fgaddon\Aircraft\Zlin-50lx\z50lx-set.xml
From 42 to ONE...

D:\FG\fgaddon\Aircraft\Yak-18T\yak18t-set.xml
From 53 to ONE...

Of course, -v9 will show them ALL...

===
*: Crash on D:\FG\fgaddon\Aircraft\707\707-set.xml

Hmmm, again TROUBLE with isspace(str[i - 1]) when the character has the high bit set!
Changed it too -
    unsigned char c;
    while((i > 0) && (c = str[i - 1]) && isspace(c)) { --i; }
and seems to get over the problem...

===
*: Repeated ac files
Now use conditional_add_string(vSTG &vs, std::string file)
when adding .ac files.

===
*: Preferrred using FG_ROOT if given when seaching for a file.
Change to first searching the path of the primary user input set file.


; eof
