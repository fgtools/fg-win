@setlocal
@REM set TMPEXE=Release\xmltest.exe
@set TMPEXE=Debug\xmltestd.exe
@if NOT EXIST %TMPEXE% goto NOEXE

@set TMPCMD=
:RPT
@if "%~1x" == "x" goto GOTCMD
@set TMPCMD=%TMPCMD% %1
@shift
@goto RPT
:GOTCMD

%TMPEXE% %TMPCMD%

@goto END
:NOEXE
@echo Can NOT locate EXE %TMPEXE%
:END

@REM eof
