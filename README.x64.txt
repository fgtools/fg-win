README.x64.txt - 20140814

Building FlightGear for WIN64. 

See README.txt for WIN32 builds, and more general information. 

This ONLY deals with the WIN64 builds (64-bits)

Each of these build-me.bat files will check if your MSVC installation supports
64-bit building. If not it will revert to a 32-bit build. The build-me.bat MAY need 
adjustment depending on where your MSVC version has installed.

Each will build the appropriate libraries, and install them, with headers into 
the 3rdParty.x64 folder.

Note on source updates: Where the sources are repository based you can at any time 
update that source to the latest, and use the relevant build-me.bat to build in the 
updates. Normally the cmake generator correctly handles the recompiling of 'newer'
files.

But there are times whne this fails. Then it is necessary to first run cmake-clean.bat 
to blow away ALL the built files and folder, and start again. This should be the first 
thing done if you get 'strange' errors in the build.

============
*: plib 

NOTE: Must build the 32-bit version in build-plib first to put the hand crafted
CMakeLists.txt into the plib-1.8.5 source folder.  Or manually copy it.

Then cd build-plib.x64 and run build-me.bat.

============
*: zlib

cd build-zlib.x64 and run build-me.bat. 

============
*: png

cd build-png.x64 and run build-me.bat.

============
*: freeglut

cd build-freeglut.x64 and run build-me.bat.

============
*: freetype

TODO: build and instll 64-bit versions of freetype

============
*: pthreads

NOTE: Must build the 32-bit version in build-pthreads first to put the hand crafted
CMakeLists.txt into the pthreads source folder. Or manually copy it.

cd build-pthreads.x64 and run build-me.bat.

============
*: jpeg

NOTE: Must build the 32-bit version in build-jpeg first to put the hand crafted
CMakeLists.txt into the jpeg-9 source folder. Or manually copy it.

cd build-jpeg.x64 and run build-me.bat.

============
*: OpenSceneGraph

NOTE: Sometimes need to MANUALLY adjust some *.vcxproj files. There is a note in the build-me.bat 
about this, and need to uncomment the jump over running cmake generation, else it will re-write
the manually corrected *.vcxproj files. It seems ;optimized.lib and ;debug.lib get add to the 
library dependencies. This seems like a cmake bug, or else the local cmake files have a BUG.

Dependencies:
ESSENTIAL: zlib, png, pthreads, freetype, OpenGL [1]
OPTIONAL: jpeg, curl, sdl, tiff

cd build-osg.x64 and run build-me.bat.
============
*: boost

SOURCE: Run updboost.bat to create X:\boost_1_53_0
BUILD: Establish the MSVC 64-bit command prompt, and CD build-boost.x64
Will install ALL the headers and libraries in
X:\install\msvc100-64\boost

============
*: simgear

Dependencies: 
REQUIRED: Boost, OpenAL, OpenSceneGraph (min 3.2), ZLIB, Threads, OpenGL [1]
OPTIONAL: RTI (https://sourceforge.net/p/openrti/OpenRTI/ci/master/tree/), and EXPAT and only if shared

cd build-sg.x64 and run build-me.bat

============
*: flightgear

TODO: Build the 64-bit version of flightgear

============


Geoff R. McLane
email: reports _at_ geoffair _dot_ info
20140814

[1] OpenGL is normally already installed in Windows

# eof
