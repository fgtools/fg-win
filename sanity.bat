@setlocal
@set TMPDIRS=build-zlib build-png build-osg build-openal build-sg build-plib build-freeglut build-fg
@set TMPBAD=0
@set TMPMISSED=
@set TMPCNT=0
@for %%i in (%TMPDIRS%) do @(call :CHKIT %%i)
@if "%TMPBAD%x" == "1x" goto SHOWBAD
@echo Appears sanity check passed - found %TMPCNT% build directories...
@goto END

:SHOWBAD
@echo Found %TMPCNT% directories, BUT
@echo seem MISSING directory/directories [%TMPMISSED%]
@echo Can NOT run build-all, sadly...
@goto ISERR

:CHKIT
@if "%~1x" == "x" goto :EOF
@if NOT EXIST %1\build-me.bat goto ISBAD
@set /A TMPCNT+=1
@goto :EOF

:ISBAD
@set TMPBAD=1
@set TMPMISSED=%TMPMISSED% %1
@goto :EOF


:ISERR
@echo.
@endlocal
@echo *** GOT AN ERROR *** FIX ME ***
exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
