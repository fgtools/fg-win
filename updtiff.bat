@setlocal
@set TMPDIR=tiff-4.0.3
@echo Is source from zip... download later for update...
@echo ftp://ftp.remotesensing.org/pub/libtiff
@set TMPZIP=zips\%TMPDIR%.zip
@if NOT EXIST %TMPZIP% goto ERR1
:GOTSRC
@if EXIST %TMPDIR%\nul goto ERR2

call unzip8 -d %TMPZIP%

@if NOT EXIST %TMPDIR%\nul goto NODIR

@goto END

:NODIR
@echo Error: %TMPDIR% NOT created!
@goto END

:ERR1
@set TMPSRC=%DOWNLOADS%\%TMPDIR%.zip
@if NOT EXIST %TMPSRC% goto ERR11
copy %TMPSRC% %TMPZIP%
@if NOT EXIST %TMPZIP% goto ERR111
@goto GOTSRC
:ERR111
@echo Copy FAILED?
:ERR11
@echo Can NOT locate %TMPSRC%
@echo Can NOT locate %TMPZIP%
@goto END

:ERR2
@echo Directory exist %TMPDIR%
@call dirmin %TMPZIP%
@goto END

:END
