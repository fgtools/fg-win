@setlocal
@echo Minor modification to CMakeLists.txt
@set TMPFIL=CMakeLists.txt
@set TMPSRC=X:\freetype-2.5.3

@if NOT EXIST %TMPSRC%\nul goto NOSRC
@if NOT EXIST %TMPFIL% goto NOFIL

@if NOT EXIST %TMPSRC%\%TMPFIL% goto DOCOPY
@fc4 -v0 -q %TMPFIL% %TMPSRC%\%TMPFIL%
@if ERRORLEVEL 1 goto DOCOPY
@call dirmin %TMPSRC%\%TMPFIL%
@echo Appears no change in %TMPFIL%... nothing done...
@goto DNCOPY
:DOCOPY
@call dirmin %TMPFIL%
@if EXIST %TMPSRC%\%TMPFIL% @call dirmin %TMPSRC%\%TMPFIL%
@echo Copying %TMPFIL% to %TMPSRC%
copy %TMPFIL% %TMPSRC%
@if NOT EXIST %TMPSRC%\%TMPFIL% goto NOCOPY
@call dirmin %TMPSRC%\%TMPFIL%
@echo Done copy of %TMPFIL% to %TMPSRC%
:DNCOPY

@REM Add any other 'created' files

@goto END

:NOCOPY
@echo Copy of %TMPFIL% to %TMPSRC% FAILED!
@goto ISERR

:NOSRC
@echo ERROR: Can NOT locate source [%TMPSRC%]!
@goto ISERR

:NOFIL
@echo ERROR: Can NOT locate file [%TMPFIL%]!
@goto ISERR

:ISERR
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
