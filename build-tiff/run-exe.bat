@setlocal
@if "%~1x" == "x" goto HELP
@set TMPEXE=Release\%1
@if EXIST %TMPEXE% goto GOTEXE
@set TMPEXE=Release\%1.exe
@if EXIST %TMPEXE% goto GOTEXE
@echo Unable to locate %1 EXE in Release folder...
@goto END
:GOTEXE
@shift
@set TMPCMD=
:RPT
@if "%~1x" == "x" goto GOTCMD
@set TMPCMD=%TMPCMD% %1
@shift
@goto RPT
:GOTCMD

@set TMP3RD=X:\3rdParty\bin
@if NOT EXIST %TMP3RD%\nul goto NO3RD

@set PATH=%TMP3RD%;%PATH%

%TMPEXE% %TMPCMD%


@goto END

:NO3RD
@echo Can NOT locate %TMP3RD%! *** FIX ME ***
@echo This is where the extra RUNTIME DLL live...
@goto END



:HELP
@echo HELP: Give the name of the EXE and any commands...
:END
