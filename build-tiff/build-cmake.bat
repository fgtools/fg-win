@setlocal
@REM 20140417 - adjust to use X: drive system...
@set TMPSRC=CMakeLists.txt
@if NOT EXIST %TMPSRC% goto ERR1
@call dirmin %TMPSRC%
@set TMPDIR=X:\tiff-4.0.3
@if NOT EXIST %TMPDIR%\nul goto ERR2
@set TMPDST=%TMPDIR%\%TMPSRC%
@if NOT EXIST %TMPDST% goto DOCOPY
@call dirmin %TMPDST%
@fc4 -q -v0 %TMPSRC% %TMPDST% >nul
@if ERRORLEVEL 2 goto NOFC4
@if ERRORLEVEL 1 goto DOCOPY
@echo Files are the SAME... Nothing done...
@goto END

:NOFC4
@echo Can NOT run fc4! so doing copy...
:DOCOPY
copy %TMPSRC% %TMPDST%
@if NOT EXIST %TMPDST% goto ERR3
@call dirmin %TMPDST%
@echo Done file update...
@goto END

:ERR1
@echo Source %TMPSRC% does NOT exist!
@goto ISERR

:ERR2
@echo Destination %TMPDIR% does NOT exist!
@goto ISERR

:ERR3
@echo Copy of %TMPSRC% to %TMPDST% FAILED!
@goto ISERR

:ISERR
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof

