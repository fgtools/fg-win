README.tiff.txt - 20140116

See OPTIONS in CMakeLists.txt

TODO: To try the DLL build
option( BUILD_SHARED_LIB "Set ON to build Shared Library"              OFF )


With the static library, all test and tools built fine, but are NOT installed, so OFF by default
option( BUILD_TEST_EXES  "Set ON to build the 'test' executables"      OFF )

option( BUILD_TOOLS_EXES "Set ON to build the 'tools' executables"     OFF )


This option added due to the OSG tiff plugin build was unable to find and 
link with the jpeg.lib, so gave unresolved externals. This could be fixed 
by modifying the OSG cmake files, but reluctant to do this...
option( ADD_JPEG_SUPPORT "Set ON to find jpeg library and add support" OFF )



Had to make ONE small changes to the source

1: tiff-4.0.3\test\rewrite_tag.c
In this C files, the declaration of a variable
     int failure = 0;

had to be moved in front of the code -
      (void) argc;
for the MS cl compiler.

Tried adding... but this FAILED
2: tiff-4.0.3\libtiff\tif_jpeg.c
#ifdef JPEG_SUPPORT

#ifdef _MSC_VER

#pragma comment(lib, "jpeg.lib") // link with JPEG library

#endif // _MSC_VER

This did NOT help in the OSG compile/link of the tiff plugin
where it can NOT find the jpeg.lib... so REMOVED this change...

3: Try removing JPEG_SUPPORT in the CMakeLists.txt
Added an option ADD_JPEG_SUPPORT which is OFF by default
and this allowed OSG tiff plugin DLL to be linked - PHEW


# eof
