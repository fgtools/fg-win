@setlocal

@set TMPPRJ=glew
@set TMPSRC=..\glew-1.11.0
@if NOT EXIST %TMPSRC%\nul goto NOSRC
@set TMPLOG=bldlog-1.txt
@set BLDDIR=%CD%
@set TMPROOT=X:
@set SET_BAT=%ProgramFiles(x86)%\Microsoft Visual Studio 10.0\VC\vcvarsall.bat
@if NOT EXIST "%SET_BAT%" goto NOBAT
@set TMPOPTS=-G "Visual Studio 10 Win64"

@call chkmsvc %TMPPRJ%

@echo Doing build output to %TMPLOG%
@echo Doing build output to %TMPLOG% > %TMPLOG%

@if /I "%PROCESSOR_ARCHITECTURE%" EQU "AMD64" (
@set TMPINST=%TMPROOT%\3rdParty.x64
) ELSE (
 @if /I "%PROCESSOR_ARCHITECTURE%" EQU "x86_64" (
@set TMPINST=%TMPROOT%\3rdParty.x64
 ) ELSE (
@echo ERROR: Appears 64-bit is NOT available! Aborting...
@goto ISERR
 )
)

@echo Doing: 'call "%SET_BAT%" %PROCESSOR_ARCHITECTURE%'
@echo Doing: 'call "%SET_BAT%" %PROCESSOR_ARCHITECTURE%' >> %TMPLOG%
@call "%SET_BAT%" %PROCESSOR_ARCHITECTURE% >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR0

@cd %BLDDIR%

:DNARCH

@REM call setupqt64

@set TMPOPTS=%TMPOPTS% -DCMAKE_INSTALL_PREFIX=%TMPINST%

@if NOT EXIST X:\nul goto NOXDIR
@REM if NOT EXIST %BOOST_ROOT%\nul goto NOBOOST

@if EXIST build-cmake.bat (
@call build-cmake >>%TMPLOG% 2>&1
)
@if NOT EXIST %TMPSRC%\CMakeLists.txt goto NOCM

@echo Running in %CD%
@echo Running in %CD% >>%TMPLOG%

@echo Doing: 'cmake %TMPSRC% %TMPOPTS%' output to %TMPLOG%
@echo Doing: 'cmake %TMPSRC% %TMPOPTS%' >>%TMPLOG%
@cmake %TMPSRC% %TMPOPTS% >>%TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR1

@echo Doing: 'cmake --build . --config Debug' output to %TMPLOG%
@echo Doing: 'cmake --build . --config Debug' >>%TMPLOG%
@cmake --build . --config Debug >>%TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR2

@echo Doing: 'cmake --build . --config Release' output to %TMPLOG%
@echo Doing: 'cmake --build . --config Release' >>%TMPLOG%
cmake --build . --config Release >>%TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR3

@echo Appears a successful build...

@REM echo No install at this time...
@REM goto END

@echo.
@echo Continue with install to %TMPINST%? Only Ctrl+C aborts...
@echo.
@pause

@echo Doing: 'cmake --build . --config Debug --target INSTALL' output to %TMPLOG%
@echo Doing: 'cmake --build . --config Debug --target INSTALL' >>%TMPLOG%
cmake --build . --config Debug --target INSTALL >>%TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR4

@echo Doing: 'cmake --build . --config Release --target INSTALL' output to %TMPLOG%
@echo Doing: 'cmake --build . --config Release --target INSTALL' >>%TMPLOG%
cmake --build . --config Release --target INSTALL >>%TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR5

@fa4 " -- " %TMPLOG%
@echo.
@echo Appears a successful build and install... see %TMPLOG% for details
@echo.
@goto END

:NOBAT
@echo Can NOT locate "%SET_BAT%"! *** FIX ME ***
@goto ISERR

:NOBOOST
@echo Can NOT locate Boost in %BOOST_ROOT%!
@goto ISERR

:NOZDIR
@echo Error: Z: drive NOT setup!
@goto ISERR

:NOXDIR
@echo Error: X: drive NOT setup!
@goto ISERR

:NOSRC
@echo Can NOT locate source %TMPSRC%! *** FIX ME ***
@goto ISERR

:NOCM
@echo Can NOT locate source %TMPSRC%\CMakeLists.txt! *** FIX ME ***
@goto ISERR

:ERR4
:ERR5
@echo See %TMPLOG% for error
@goto ISERR

:ERR0
@echo Appears "%SET_BAT%" FAILED!
@goto ISERR

:Err1
@echo cmake config, gen error
@goto ISERR

:Err2
@echo debug build error
@goto ISERR

:Err3
@echo release build error
@goto ISERR

:ISERR
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
