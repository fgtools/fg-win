@setlocal
@set DOPAUSE=pause
@if "%~1x" == "NOPAUSEx" (
@set DOPAUSE=echo No Pause requested
@echo No pause requested
)

@REM Update to SF fgdata
@echo Update FGDATA next...
@echo https://sourceforge.net/p/flightgear/fgdata/ci/next/tree/

@set REPO=git://git.code.sf.net/p/flightgear/fgdata
@set TMPBGN=%TIME%
@set TMPDIR=fgdata

@if NOT EXIST %TMPDIR%\nul goto CHECKOUT

@cd %TMPDIR%
@echo This is an update in %CD%
@echo Doing: 'call git status'... moment...
@call git status
@echo Check the above status. Continue with update? will do 'git pull'
@echi *** CONTINUE? *** Only Ctrl+C aborts. All other keys continue...
@%DOPAUSE%

@call git pull
@cd ..

@goto END

:CHECKOUT
@echo WARNING: Folder %TMPDIR% does NOT exist!
@echo This is a NEW CLONE
@echo Note: last clone took nearly 15 mins - abt 1.3 GB download, expands 2.9 GB of data
@echo This is the output shown, but for sure the numbers will only increase
@echo.
@echo Will do: 'git clone %REPO% %TMPDIR2%'
@echo.
@echo BE WARNED: The clone can break before completing,
@echo and it is necessary to completely empty the 'fgdata' directory each time
@echo before the next attempt...
@echo.
@echo *** CONTINUE? *** Only Ctrl+c to abort... all other keys continue...
@echo.
@%DOPAUSE%
@echo.
@echo *** ARE YOU SURE? *** Only Ctrl+C aborts...
@echo.
@%DOPAUSE%
@echo.
@echo Doing: 'call git clone %REPO% %TMPDIR2%'

call git clone %REPO% %TMPDIR%

@if NOT EXIST %TMPDIR%\nul goto FAILED
@echo Done fresh checkout...

@goto END

:FAILED
@echo ERROR: git clone FAILED! No folder %TMPDIR% created...
@goto ISERR

:FAILED2
@echo ERROR: make directory FAILED! No folder %TMPDIR1% created...
@goto ISERR

:ISERR
@endlocal
@exit /b 1

:END
@call elapsed %TMPBGN%
@endlocal
@exit /b 0

@REM eof
