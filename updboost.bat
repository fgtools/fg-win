@setlocal
@echo Zip source - check http://www.boost.org/users/download/ for LATER versions
@echo Note: simpits uses boost 1.52...
@set TMPDIR=boost_1_53_0
@set TMPZIP=zips\%TMPDIR%.zip
@if NOT EXIST %TMPZIP% goto NOSRC

:TRY2
@if EXIST %TMPDIR%\nul goto NOWAY

call unzip8 -d %TMPZIP%
@if NOT EXIST %TMPDIR%\nul goto FAILED

@echo Source has been place in %TMPDIR%

@goto END

:FAILED
@echo Unzip of %TMPZIP% appears to have FAILED - CHECK ME!
@goto END


:NOSRC
@set TMPSRC=%DOWNLOADS%\%TMPDIR%.zip
@if NOT EXIST %TMPSRC% goto NOSRC2
copy %TMPSRC% %TMPZIP%
@if EXIST %TMPZIP% goto TRY2

:NOSRC2
@echo Error: Can NOT find source %TMPSRC% - FIX ME!
@echo Error: Can NOT find source %TMPZIP% - FIX ME!
@goto END

:NOWAY
@echo Dir %TMPDIR% already exists...
@call dirmin %TMPZIP%
@echo This is a zipped source, so no update available
@goto END

:END
