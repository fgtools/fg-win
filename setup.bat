@set RUNDIR=%~dp0
@goto DOSETUP

:Substring
::Substring(retVal,string,startIndex,length)
:: extracts the substring from string starting at startIndex for the specified length 
@SET string=%2%
@SET startIndex=%3%
@SET length=%4%
 
@if "%4" == "0" goto :noLength
@CALL SET _substring=%%string:~%startIndex%,%length%%%
@goto :substringResult
:noLength
@CALL SET _substring=%%string:~%startIndex%%%
:substringResult
@set "%~1=%_substring%"
@GOTO :EOF
 
:StrLength
::StrLength(retVal,string)
::returns the length of the string specified in %2 and stores it in %1
@set #=%2%
@set length=0
:stringLengthLoop
@if defined # (set #=%#:~1%&set /A length += 1&goto stringLengthLoop)
::echo the string is %length% characters long!
@set "%~1=%length%"
@GOTO :EOF

:DOSETUP

:: get the length of the batch path
@call:StrLength length %RUNDIR%
@echo length %length%

:: extract minus the final '\'
@set /a length-=1
@echo New length %length%
@call:Substring newdir,%RUNDIR%,0,%length%

@subst x: /D
subst x: %newdir%
@echo Set X: to be %newdir%, and adding X:\scripts to the PATH
@echo goto that place...
X:
@set PATH=X:\scripts;%PATH%
@call st flightgear build x: drive
