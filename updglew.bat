@setlocal
@set TMPDIR=glew-1.11.0
@set TMPZIP=%TMPDIR%.zip
@set TMPSRC=zips\%TMPZIP%
@REM set TMPDIR=glew-1.10.0
@REM set TMPDIR=glew-1.9.0

@if NOT EXIST %TMPSRC% goto NOZIP
:GOTZIP
@if EXIST %TMPDIR%\nul goto ALLDONE

call unzip8 -d %TMPSRC%

@if NOT EXIST %TMPDIR%\nul goto FAILED

@echo All done... source is in %TMPDIR%

@goto END

:FAILED
@echo ERROR: Unzip of %TMPZIP% FAILED!
!goto ISERR

:NOZIP
@if NOT EXIST %DOWNLOADS%\%TMPZIP% goto NOZIP2
copy %DOWNLOADS%\%TMPZIP% %TMPSRC%
@if EXIST %TMPSRC% goto GOTZIP
:NOZIP2
@echo ERROR: Unable to locate SOURCE zip! %TMPZIP% FIX ME!!!
@goto ISERR

:ISERR
@endlocal
@exit /b 1

:ALLDONE
@echo Folder %TMPDIR% already exists... nothing to do...
@echo Delete this folder to unzip again from %TMPZIP%
@goto END

:END
@endlocal
@exit /b 0

@REM eof
