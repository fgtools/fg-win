@setlocal
@echo https://gitlab.com/fgtools/cmake-test
@set TMPDIR=cmake-test
@set TMPREPO=git@gitlab.com:fgtools/cmake-test.git
@set DOPAUSE=pause
@if "%~1x" == "NOPAUSEx" @set DOPAUSE=echo NO PAUSE requested

@if EXIST %TMPDIR%\nul goto UPDATE

@echo Are you SURE continue to do CLONE of %TMPDIR% source?
@echo Will do: call git clone %TMPREPO% %TMPDIR%

@%DOPAUSE%

call git clone %TMPREPO% %TMPDIR%
@echo Done: call git clone %TMPREPO% %TMPDIR%
@if NOT EXIST %TMPDIR%\. goto COFAILED

@REM cd %TMPDIR%
@REM call git branch
@REM cd ..

@echo Done new clone to %TMPDIR%...
@goto END

:UPDATE
@echo This is an UPDATE of %TMPDIR%
@cd %TMPDIR%
@call git status
@echo *** CONTINUE? ***
@%DOPAUSE%

@echo In %CD% doing: 'git pull'
@git pull
@echo Done: 'git pull'
@cd ..
@goto END

:COFAILED
@echo ERROR: %TMPDIR% not created - clone FAILED
@endlocal
@exit /b 1

:END
@endlocal

@REM eof
