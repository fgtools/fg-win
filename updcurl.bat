@setlocal
@set TMPDIR=curl-7.39.0
@set TMPZIP=zips\%TMPDIR%.zip
@if NOT EXIST %TMPZIP% goto NOSRC
:GOTSRC
@REM call unzip8 -vb %TMPZIP%
@if EXIST %TMPDIR%\nul goto ALLDONE

call unzip8 -d %TMPZIP%
@if NOT EXIST %TMPDIR%\nul goto FAILED

@echo All done... source is in %TMPDIR%

@goto END

:FAILED
@echo ERROR: Unzip of %TMPZIP% FAILED!
!goto ISERR

:NOSRC
@if EXIST %DOWNLOADS%/%TMPDIR%.zip (
@copy %DOWNLOADS%\%TMPDIR%.zip zips\.
)
@if EXIST %TMPZIP% goto GOTSRC
:NOZIP
@echo ERROR: Unable to locate SOURCE zip! %TMPZIP%
@goto ISERR

:ISERR
@endlocal
@exit /b 1

:ALLDONE
@echo Folder %TMPDIR% already exists... nothing to do...
@call dirmin %TMPZIP%
@echo Delete this folder to unzip again from %TMPZIP%
@goto END

:END
@endlocal
@exit /b 0

@REM eof
