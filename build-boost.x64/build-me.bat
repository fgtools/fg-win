@setlocal
@echo Building all boost libraries 64-bit...
@REM bjam --toolset=msvc address-model=64 --build-type=complete --build-dir=c:\build install 
@set LOGFIL=X:\build-boost.x64\bldlog-1.txt
@set BLDLOG= ^>^> %LOGFIL% 2^>1
@set HAVELOG=1

@set VS_PATH=C:\Program Files (x86)\Microsoft Visual Studio 10.0
@set VC_BAT=%VS_PATH%\VC\vcvarsall.bat
@if NOT EXIST "%VS_PATH%" goto NOVS
@if NOT EXIST "%VC_BAT%" goto NOBAT
@set BUILD_BITS=%PROCESSOR_ARCHITECTURE%
@IF /i %BUILD_BITS% EQU x86_amd64 (
    @set "RDPARTY_ARCH=x64"
    @set "RDPARTY_DIR=software.x64"
) ELSE (
    @IF /i %BUILD_BITS% EQU amd64 (
        @set "RDPARTY_ARCH=x64"
        @set "RDPARTY_DIR=software.x64"
    ) ELSE (
        @echo Appears system is NOT 'x86_amd64', nor 'amd64'
        @echo Can NOT build the 64-bit version! Aborting
        @exit /b 1
    )
)

@ECHO Setting environment - CALL "%VC_BAT%" %BUILD_BITS%
@CALL "%VC_BAT%" %BUILD_BITS%
@if ERRORLEVEL 1 goto NOSETUP

@set TMPINST=X:\install
@if NOT EXIST %TMPINST% (
@md %TMPINST%
@if ERRORLEVEL 1 goto NOIDIR
)
@set TMPBLD=X:\build-boost.x64
@set TMPINST=X:\install\msvc100-64
@if NOT EXIST %TMPINST% (
@md %TMPINST%
@if ERRORLEVEL 1 goto NOIDIR
)
@set TMPINST=X:\install\msvc100-64\boost

@set TMPSRC=boost_1_53_0
cd ..
@if NOT EXIST %TMPSRC%\nul (
    @if EXIST updboost.bat (
        call updboost
        @if NOT EXIST %TMPSRC%\nul (
            @echo FAILED to create %TMPSRC%
            @goto ISERR
        )
    )
)
@cd %TMPSRC%
@echo Got boost source in %CD%
@IF %HAVELOG% EQU 1 (
    @echo Got boost source in %CD% > %LOGFIL%
)

@set B2OPTS=install
@set B2OPTS=%B2OPTS% --toolset=msvc
@set B2OPTS=%B2OPTS% --address-model=64
@set B2OPTS=%B2OPTS% --build-type=complete
@set B2OPTS=%B2OPTS% --build-dir=%TMPBLD%
@set B2OPTS=%B2OPTS% --prefix="%TMPINST%"

@if NOT EXIST %TMPINST%\nul (
    @echo Building boost libraries
    @ECHO Doing: 'call .\bootstrap' %BLDLOG%
    @IF %HAVELOG% EQU 1 (
        @ECHO Doing: 'call .\bootstrap' to %LOGFIL%
    )
    call .\bootstrap %BLDLOG%
    @ECHO Doing: '.\b2 %B2OPTS%' %BLDLOG%
    @IF %HAVELOG% EQU 1 (
        @ECHO Doing: '.\b2 %B2OPTS%' to %LOGFIL%
    )
    .\b2 %B2OPTS% %BLDLOG%
) else (
    @echo Install directory %TMPINST% already exists
    @echo Assume boost build and install done
    @echo Delete %TMPINST% to redo...
)
@goto END

:NOIDIR
@echo Failed to create %TMPINST%
@goto ISERR

:NOSETUP
@echo MSVC environmnet setup FAILED
@goto ISERR

:ISERR
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM EOF
