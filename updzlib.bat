@setlocal
@REM http://zlib.net/
@REM download: zlib127.zip 
@REM
@set TMPZIP=zlib128.zip
@set TMPSRC=zips\%TMPZIP%
@set TMPDIR=zlib-1.2.8

@if EXIST %TMPDIR%\nul (
@echo No update - this is a ZIP source %TMPSRC%
@if EXIST %TMPSRC% @call dirmin %TMPSRC%
) else (
@if NOT EXIST %TMPSRC% goto ERR1
:TRY2
call unzip8 -d %TMPSRC%
@if NOT EXIST %TMPDIR%\nul goto ERR2
@echo Have unzipped source into %TMPDIR%
)
@endlocal
@exit /b 0

:ERR1
@set TMPSRC2=%DOWNLOADS%\%TMPZIP%
@if NOT EXIST %TMPSRC2% goto ERR11
copy %TMPSRC2% %TMPSRC%
@if EXIST %TMPSRC% goto TRY2
:ERR11
@echo ERROR: Zip source %TMPSRC% is NOT available!
@echo ERROR: And source %TMPSRC2% is NOT available!
@goto ISERR

:ERR2
@echo ERROR: Unzip source %TMPSRC% FAILED to create %TMPDIR%!
@goto ISERR

:ISERR
@endlocal
@exit /b 1
